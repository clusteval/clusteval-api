# API of ClustEval

[![Documentation Status](https://readthedocs.org/projects/clusteval/badge/?version=latest)](http://clusteval.readthedocs.io/en/latest/?badge=latest)

Project contains Java sources of
- interfaces
- abstract classes
- concrete exception classes
- some more basic concrete classes
