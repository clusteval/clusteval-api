/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.serverclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.Options;
import org.eclipse.aether.resolution.ArtifactResolutionException;

import ch.qos.logback.classic.Level;
import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.dataset.format.DataSetParseException;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.UnknownDataSetGeneratorException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.ISerializableGoldStandard;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.data.randomizer.DataRandomizeException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.UnknownDynamicComponentException;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ISerializableStandaloneProgram;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.UnknownRunTypeException;
import de.clusteval.run.runnable.IterationRunnableStatus;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ISerializableRunResult;
import de.clusteval.run.runresult.UnknownRunResultException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DeletionException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * An interface for the backend server. This interface contains all command a
 * server can take from a client, e.g. starting, stopping, resuming of runs or
 * shutting down the server.<br>
 * 
 * <dl>
 * <dt>8.0</dt>
 * <dd>Using compbio utils instead of Wiutils</dd>
 * <dt>&#60; 8.1</dt>
 * <dd>...</dd>
 * </dl>
 */
@ClassVersion(version = "8.0")
public interface IBackendServer extends Remote {

	public String getClustEvalServerVersion(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of all runs contained in the
	 *         repository of this server.
	 * @throws RemoteException
	 * @throws RepositoryObjectSerializationException
	 */
	public Collection<ISerializableRun> getRuns(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	/**
	 * @return A collection with the names of all run result directories
	 *         contained in the repository of this server. Those run result
	 *         directories can be resumed, if they were terminated before.
	 * @throws RemoteException
	 */
	public Collection<String> getRunResumes(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection with the names of all programs contained in the
	 *         repository of this server.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRProgramException
	 * @throws RepositoryObjectSerializationException
	 * @throws UnknownRunResultFormatException
	 * @throws UnknownDataSetFormatException
	 */
	public Collection<ISerializableProgram> getPrograms(final String clientId)
			throws RemoteException, UnknownRProgramException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException,
			RepositoryObjectSerializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException;

	public Collection<String> getProgramAliases(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @param generatorName
	 *            The simple name of the class of the dataset generator.
	 * @return A wrapper objects keeping all the options of the specified
	 *         dataset generator.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataSetGeneratorException
	 */
	public Options getOptionsForDataSetGenerator(final String clientId,
			final String generatorName)
			throws RemoteException, UnknownDataSetGeneratorException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @param generatorName
	 *            The simple name of the class of the dataset generator to use
	 *            to generate the new dataset.
	 * @param args
	 *            The arguments to pass on to the dataset generator.
	 * @return True, if the dataset (and goldstandard) has been generated
	 *         successfully.
	 * @throws RemoteException
	 */
	public boolean generateDataSet(final String clientId,
			final String generatorName, final String[] args)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @param randomizerName
	 *            The simple name of the class of the data randomizer to use to
	 *            randomize the new dataset.
	 * @param args
	 *            The arguments to pass on to the data randomizer.
	 * @return True, if the data config has been randomized successfully.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataRandomizerException
	 * @throws DataRandomizeException
	 */
	public boolean randomizeDataConfig(final String clientId,
			final String randomizerName, final String[] args)
			throws RemoteException, UnknownDataRandomizerException,
			DynamicComponentInitializationException, DataRandomizeException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of all datasets contained in the
	 *         repository of this server.
	 * @throws RemoteException
	 *             the remote exception
	 * @throws RepositoryObjectSerializationException
	 */
	public Collection<ISerializableDataSet> getDataSets(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public Collection<ISerializableProgramConfig> getProgramConfigurations(
			final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public Collection<ISerializableDataConfig> getDataConfigurations(
			final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public Map<String, Object> getStatsOfDataConfiguration(
			final String clientId, final String name, final String version)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	public Collection<ISerializableDataSetConfig> getDataSetConfigurations(
			final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	/**
	 * @return
	 * @throws RemoteException
	 * @throws RepositoryObjectSerializationException
	 */
	public Collection<ISerializableGoldStandard> getGoldStandards(
			final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public Collection<ISerializableGoldStandardConfig> getGoldStandardConfigurations(
			final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public <R extends IRepositoryObjectDynamicComponent> Collection<ISerializableWrapperDynamicComponent<R>> getDynamicComponents(
			final String clientId, final Class<R> clazz) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	public void deleteDynamicComponent(final String clientId,
			final Class<? extends IRepositoryObjectDynamicComponent> clazz,
			final String name, final String version) throws RemoteException,
			OperationNotPermittedException, UnknownDynamicComponentException,
			UnknownClientException, DeletionException;

	public String getAbsoluteRepositoryPath(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	public Collection<ISerializableRunResult<? extends IRunResult>> getRunResult(
			final String clientId, final String uniqueRunIdentifier)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	/**
	 * @param uniqueRunIdentifier
	 *            The unique run identifier of a run result stored in the
	 *            corresponding directory of the repository.
	 * @return The run results for the given unique run identifier.
	 * @throws RemoteException
	 */
	public Map<Pair<String, String>, Map<String, Double>> getRunResults(
			final String clientId, final String uniqueRunIdentifier)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection with the names of the parameter optimization run
	 *         results.
	 * @throws RemoteException
	 */
	public Collection<String> getParameterOptimizationRunResultIdentifiers(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of the clustering run results.
	 * @throws RemoteException
	 */
	public Collection<String> getClusteringRunResultIdentifiers(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of the data analysis run results.
	 * @throws RemoteException
	 */
	public Collection<String> getDataAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of the run analysis run results.
	 * @throws RemoteException
	 */
	public Collection<String> getRunAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of the run-data analysis run results.
	 * @throws RemoteException
	 */
	public Collection<String> getRunDataAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * 
	 * @param programConfigName
	 *            The name of a program configuration for which to get its
	 *            parameters.
	 * @return A mapping from parameter name to another map, containing a
	 *         mapping from parameter property (such as def, minValue, maxValue)
	 *         to a set value.
	 * @throws RemoteException
	 * @throws ObjectNotRegisteredException
	 * @throws ObjectVersionNotRegisteredException
	 */
	public Map<String, Map<String, String>> getParametersForProgramConfiguration(
			final String clientId, final String programConfigName)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	/**
	 * 
	 * @return A collection with the names of all runs and run results that are
	 *         currently enqueued but not yet running.
	 * @throws RemoteException
	 */
	public Collection<String> getQueue(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection containing names of all finished runs and run
	 *         resumes.
	 */
	public Collection<String> getFinishedRuns(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection containing names of all running runs and run
	 *         resumes.
	 */
	public Collection<String> getRunningRuns(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection containing names of all terminated runs and run
	 *         resumes.
	 */
	public Collection<String> getTerminatedRuns(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * This method tells the framework that a certain client wants to perform
	 * the run with the given name.
	 * 
	 * @param clientId
	 *            The id of the client, that wants to perform the run.
	 * @param runId
	 *            The name of the run that should be performed.
	 * @return The unique identification string of the run; null if it has not
	 *         been scheduled.
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 * @throws ObjectNotRegisteredException
	 * @throws ObjectVersionNotRegisteredException
	 */
	public String performRun(String clientId, String runId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	/**
	 * This method tells the framework that a certain client wants to resume the
	 * run result with the given unique identifier.
	 * 
	 * @param clientId
	 *            The id of the client, that wants to perform the run.
	 * @param uniqueRunIdentifier
	 *            The unique identifier of the run result that should be
	 *            resumed.
	 * @return true, if successful
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	public boolean resumeRun(final String clientId,
			final String uniqueRunIdentifier) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * This method tells the framework that a certain client wants to terminate
	 * the run with the given name.
	 * 
	 * <p>
	 * This operation is only allowed if the client id is the same, as the one
	 * that performed the run before.
	 * 
	 * @param clientId
	 *            The id of the client, that wants to perform the run.
	 * @param runId
	 *            The name of the run that should be terminated.
	 * @return true, if successful
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	public boolean terminateRun(final String clientId, final String runId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * This method tells the framework to shutdown. The framework will terminate
	 * the supervisor thread, which then in turn terminates all other threads he
	 * is oversees.
	 * 
	 * @param clientId
	 *            The id of the client, that wants to shutdown the framework.
	 * @param timeOut
	 *            The timeout how long the server will wait for threads until
	 *            forcing the shutdown.
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	public void shutdown(final String clientId, final long timeOut)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * This is a factory method which returns a unique random client id which is
	 * used to identify clients. Client ids are needed for the clients to
	 * communicate with the server and give certain commands.
	 * 
	 * @return The next free client id.
	 * @throws RemoteException
	 */
	public String getClientId() throws RemoteException;

	public IClientPermissions getClientPermissions(final String clientId)
			throws RemoteException, UnknownClientException,
			UnknownClientException;

	/**
	 * This method returns the status and percentage of any run performed by the
	 * client with the given id.
	 * 
	 * @param clientId
	 *            The client id for which this method returns the status of its
	 *            runs.
	 * @return The status and percentage of all runs of this client.
	 * @throws RemoteException
	 */
	public Map<String, Pair<RUN_STATUS, Float>> getRunStatusForClientId(
			String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * This method returns the estimated remaining runtime of any run performed
	 * by the client with the given id in seconds.
	 * 
	 * @param clientId
	 *            The client id for which this method returns the status of its
	 *            runs.
	 * @return The estimated remaining runtime of all runs of this client in
	 *         seconds.
	 * @throws RemoteException
	 */
	public Map<String, Long> getEstimatedRemainingRuntimeForClientId(
			String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @param clientId
	 * @return
	 * @throws RemoteException
	 */
	public Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getOptimizationRunStatusForClientId(
			String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * Returns a map containing active threads and the corresponding
	 * runs/iterations/starttime that they perform
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public Map<String, IterationRunnableStatus> getActiveThreads(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * Returns a list of queued iteration threads
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public List<Triple<String, String, Long>> getIterationRunnableQueue(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * This method allows to set the log level of this server.
	 * <p>
	 * Possible values are
	 * <ul>
	 * <li><b>0</b>: ALL</li>
	 * <li><b>1</b>: TRACE</li>
	 * <li><b>2</b>: DEBUG</li>
	 * <li><b>3</b>: INFO</li>
	 * <li><b>4</b>: WARN</li>
	 * <li><b>5</b>: ERROR</li>
	 * <li><b>6</b>: OFF</li>
	 * </ul>
	 * See {@link Level} for explanations of the log levels.
	 * 
	 * @param logLevel
	 *            The new log level of this server as an integer value.
	 * @throws RemoteException
	 */
	public void setLogLevel(final String clientId, Level logLevel)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @param randomizerName
	 *            The simple name of the class of the data randomizer.
	 * @return A wrapper objects keeping all the options of the specified data
	 *         randomizer.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataRandomizerException
	 */
	public Options getOptionsForDataRandomizer(final String clientId,
			final String randomizerName)
			throws RemoteException, UnknownDataRandomizerException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * Updates the maximal number of parallel iteration threads.
	 * 
	 * @param threadNumber
	 * @throws RemoteException
	 */
	public void setThreadNumber(final String clientId, final int threadNumber)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	public int getThreadNumber(final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	public boolean isRelativeDataSetFormat(final String clientId,
			final ISerializableDataSetFormat format)
			throws RemoteException, DeserializationException,
			OperationNotPermittedException, UnknownClientException;

	public void reparseAllRunResultsIntoDatabase(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	public void reparseRunResultIntoDatabase(final String clientId,
			String runResultId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	public void refreshDatabaseMaterializedViews(final String clientId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	public void setDebugNoNewClusterings(final String clientId, boolean b)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * Returns the last exception messages from parsing and loading the
	 * repository objects.
	 * 
	 * @param clientId
	 * @return
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 */
	public Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> getParsingExceptionMessages(
			final String clientId,
			final Class<? extends IRepositoryObject>... clazz)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * Returns all the exception messages that have been thrown during execution
	 * of the given run.
	 * 
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 * @throws RemoteException
	 */
	public List<String> getRunExceptionMessages(final String clientId,
			final String uniqueRunId, final boolean getWarningExceptions)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * Clears all the exceptions (also including handled warning exceptions) of
	 * the specified run.
	 * 
	 * @param clientId
	 * @param uniqueRunId
	 * @throws RemoteException
	 */
	public void clearRunExceptions(final String clientId,
			final String uniqueRunId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	//
	// public boolean addBackendServerSlave(final String host, final int port)
	// throws RemoteException;
	//
	// public IClustering delegateIteration(final String clientId, final String
	// repositoryAbsPath,
	// final String dataConfigId, final String programConfigId, final
	// ParameterSet paramSet)
	// throws RemoteException;

	public boolean uploadDataset(final String clientId,
			final ISerializableDataSet<IDataSet> dataSet,
			final byte[] bytesDatasetFile,
			final List<byte[]> bytesAdditionalDatasetFiles)
			throws DeserializationException, RemoteException,
			UnknownClientException, OperationNotPermittedException, IOException,
			RegisterException, DataSetParseException;

	public boolean uploadGoldStandard(final String clientId,
			final ISerializableGoldStandard dataConfig, final byte[] bytes)
			throws DeserializationException, RemoteException,
			UnknownClientException, OperationNotPermittedException, IOException,
			RegisterException;

	public boolean uploadDataSetConfig(final String clientId,
			final ISerializableDataSetConfig dataSetConfig)
			throws RemoteException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException;

	public boolean uploadDataConfig(final String clientId,
			final ISerializableDataConfig dataConfig)
			throws RemoteException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException;

	public boolean uploadGoldStandardConfig(final String clientId,
			final ISerializableGoldStandardConfig gsConfig)
			throws RemoteException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException;

	public boolean uploadRun(final String clientId,
			final ISerializableRun<IRun> run)
			throws RemoteException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException;

	public boolean uploadStandaloneProgram(String clientId,
			ISerializableStandaloneProgram program, byte[] bytes)
			throws UnknownClientException, OperationNotPermittedException,
			RemoteException, IOException, DeserializationException,
			RegisterException;

	public boolean uploadProgramConfig(final String clientId,
			final ISerializableProgramConfig programConfig)
			throws RemoteException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException;

	ISerializableDataConfig getDataConfiguration(final String clientId,
			String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableDataSetConfig getDataSetConfiguration(final String clientId,
			String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableGoldStandardConfig getGoldStandardConfiguration(
			final String clientId, String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableDataSet getDataSet(final String clientId, String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableGoldStandard getGoldStandard(final String clientId,
			String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableProgramConfig getProgramConfiguration(final String clientId,
			String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableRun getRun(final String clientId, String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableProgram getProgram(final String clientId, String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	public boolean deleteDataset(final String clientId,
			final ISerializableDataSet dataset) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	public boolean deleteDataConfiguration(final String clientId,
			final ISerializableDataConfig dataConfig) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	public boolean deleteDataSetConfiguration(final String clientId,
			final ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	public boolean deleteGoldStandardConfiguration(final String clientId,
			final ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	public boolean deleteGoldStandard(final String clientId,
			final ISerializableGoldStandard goldStandard)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	public boolean deleteProgram(final String clientId,
			final ISerializableProgram program) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	public boolean deleteProgramConfiguration(final String clientId,
			final ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	public boolean deleteRun(final String clientId, final ISerializableRun run)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	public boolean deleteRunResult(final String clientId, final String name)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException;

	public Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> getErrorObjects(
			final String clientId, Class<? extends IRepositoryObject> clazz)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	String getNextVersionForRepositoryObject(final String clientId,
			final Class<? extends IRepositoryObject> clazz, final String name,
			final String currentVersion) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	void forgetRunStatus(String clientId, String uniqueRunId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	RUN_TYPE getRunTypeOfRunResult(String clientId, String uniqueRunId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, UnknownRunTypeException,
			UnknownRunResultException;

	ISerializableClustering getClusteringOfParameterOptimizationIteration(
			final String clientId, final String resultId,
			final String programConfig, final String dataConfig,
			final long iteration) throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException;

	DataMatrix getPCAOfDataConfig(final String clientId, final String resultId,
			final String programConfig, final String dataConfig,
			final int numberPCs) throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException,
			IOException;

	/**
	 * @param clientId
	 * @param resultId
	 * @param programConfig
	 * @param dataConfig
	 * @return
	 * @throws UnknownRunResultException
	 * @throws RepositoryObjectSerializationException
	 * @throws RemoteException
	 */
	ISerializableClustering getClusteringOfClusteringRun(String clientId,
			String resultId, String programConfig, String dataConfig)
			throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException;

	<T extends IRepositoryObject> ISerializableWrapperDynamicComponent uploadDynamicComponent(
			final String clientId, Class<T> clazz, final String jarFileName,
			byte[] jarAsBytes) throws OperationNotPermittedException,
			IOException, UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectParseException,
			RegisterException;

	boolean createNewDatasetWithAttributesFromExisting(final String clientId,
			final ISerializableDataSet<IDataSet> dataSet,
			final ISerializableDataSet<IDataSet> existingDataSet)
			throws DeserializationException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DataSetParseException;

	String checkForServerUpdate(String clientId,
			boolean includeSnapshotVersions) throws RemoteException;

	String checkForClientUpdate(final String clientId,
			final String clientVersion, boolean includeSnapshotVersions)
			throws RemoteException;

	Map<String, Map<String, String>> checkForComponentUpdates(
			final String clientId, boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException;

	<T extends IRepositoryObject> String getReleaseTitle(final String clientId,
			Class<T> clazz, final String name, final String version)
			throws ArtifactResolutionException, RemoteException;

	<T extends IRepositoryObject> String getReleaseNotes(final String clientId,
			Class<T> clazz, final String name, final String version)
			throws ArtifactResolutionException, RemoteException;

	void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, URISyntaxException, InterruptedException,
			RemoteException;

	long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings)
			throws RemoteException, DeserializationException;

	/**
	 * @param programConfig
	 *            The program configuration for which to calculate the expected
	 *            runtime when applied.
	 * @param numberSamplesInDataset
	 *            The size of the data set.
	 * @param numberClusterings
	 *            The number of clusterings to calculate.
	 * @param numberCores
	 *            The number of cores that are available to cluster in parallel.
	 * @return The expected number of required miliseconds to cluster a data set
	 *         that contains the specified number of samples, with the specified
	 *         program configuration, the specified number of times.
	 */
	long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings, int numberCores)
			throws RemoteException, DeserializationException;
}
