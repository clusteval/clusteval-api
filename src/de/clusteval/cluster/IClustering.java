/**
 * 
 */
package de.clusteval.cluster;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface IClustering extends Iterable<ICluster>, IRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#clone()
	 */
	@Override
	IClustering clone();

	/**
	 * @param item
	 *            The item to look for.
	 * @return A map containing all clusters together with fuzzy coefficients,
	 *         in which the given item is contained.
	 */
	Map<ICluster, Float> getClusterForItem(IClusterItem item);

	/**
	 * @return A set with all clusters of this clustering.
	 */
	Set<ICluster> getClusters();

	/**
	 * @return A set with all cluster items contained in this clustering.
	 */
	Set<IClusterItem> getClusterItems();

	/**
	 * 
	 * @param id
	 *            The id of the cluster item.
	 * @return The cluster item with the given id.
	 */
	IClusterItem getClusterItemWithId(String id);

	/**
	 * Add a cluster to this clustering.
	 * 
	 * @param cluster
	 *            The cluster to add.
	 * @return true, if the cluster is added and hasn't been in the clustering
	 *         before.
	 * @throws ClusteringNotInitializedException
	 */
	boolean addCluster(ICluster cluster);

	/**
	 * Remove a cluster item from this clustering by removing the item from
	 * every cluster contained.
	 * 
	 * @param item
	 *            The item to remove
	 * @return True if this item was contained in this clustering.
	 */
	boolean removeClusterItem(IClusterItem item);

	/**
	 * Remove a cluster item from the specified cluster.
	 * 
	 * @param item
	 *            The item to remove
	 * @param cluster
	 *            The cluster to remove the item from.
	 * @return True if this item was contained in this clustering.
	 */
	boolean removeClusterItem(IClusterItem item, ICluster cluster);

	/**
	 * @return The number of items in this clustering. In case of fuzzy
	 *         clusterings this may differ from the fuzzy size.
	 */
	int size();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	Iterator<ICluster> iterator();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * 
	 * @return A string representing this clustering, where clusters are
	 *         separated by semi-colons and elements of clusters are separated
	 *         by commas.
	 */
	String toFormattedString();

	void writeFormattedString(Writer writer) throws IOException;

	/**
	 * This method converts a fuzzy to a hard clustering by assigning each item
	 * to the cluster, with the highest according fuzzy coefficient. If there
	 * are ties, the assigned cluster is randomly selected.
	 * 
	 * @return A hard clustering resulting from converting this fuzzy
	 *         clustering.
	 * @throws ClusteringParseException
	 */
	IClustering toHardClustering();

	float fuzzySize();

	void unloadFromMemory();

	void loadIntoMemory() throws ClusteringParseException;

	boolean isInMemory();

	IClusteringQualitySet assessQuality(final IDataConfig dataConfig,
			final List<IClusteringQualityMeasure> qualityMeasures)
			throws IOException, UnknownDataSetFormatException,
			InvalidDataSetFormatVersionException,
			ClusteringNotInitializedException,
			InvalidGoldStandardFormatException;

	IClusteringQualitySet getQualities();

	Map<String, ICluster> getClusterIdToCluster();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableClustering asSerializable()
			throws RepositoryObjectSerializationException;
}