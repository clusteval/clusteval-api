/**
 * 
 */
package de.clusteval.cluster;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ClusteringNotInitializedException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8352306337434335319L;

	/**
	 * 
	 */
	public ClusteringNotInitializedException() {
		super("The clustering is not initialized (at least one cluster and item with fuzzy coefficient > 0) but an operation required it.");
	}

	/**
	 * @param message
	 */
	public ClusteringNotInitializedException(final String message) {
		super(message);
	}
}
