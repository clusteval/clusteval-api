/**
 * 
 */
package de.clusteval.cluster;

import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.quality.ISerializableClusteringQualitySet;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableClustering
		extends
			ISerializableWrapperRepositoryObject<IClustering> {

	ISerializableClusteringQualitySet getQualities();

	Map<IClusterItem, Map<ICluster, Float>> getItemToCluster();

	Map<String, IClusterItem> getItemIdToItem();

	float getFuzzySize();

	Set<ICluster> getClusters();

	Map<String, ICluster> getClusterIdToCluster();

	IClustering deserializeInternal(IRepository repository) throws DeserializationException;

}