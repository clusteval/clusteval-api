/**
 * 
 */
package de.clusteval.cluster;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusterItem {

	/**
	 * 
	 * @return The id of this cluster item.
	 */
	String getId();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	IClusterItem clone();
}