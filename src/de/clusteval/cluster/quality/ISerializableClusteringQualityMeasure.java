/**
 * 
 */
package de.clusteval.cluster.quality;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableClusteringQualityMeasure
		extends
			ISerializableWrapperDynamicComponent<IClusteringQualityMeasure> {

}