/**
 * 
 */
package de.clusteval.cluster.quality;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusteringQualityMeasureValue {

	String toString();
	
	boolean hasValue();

	/**
	 * This method returns the quality of the clustering.
	 * 
	 * <p>
	 * It should only be invoked, if the corresponding iteration terminated and
	 * thus {@link #value} is != null.
	 * 
	 * @return The quality of the corresponding clustering.
	 */
	double getValue();

	/**
	 * @return A boolean indicating whether the iteration belonging to this
	 *         value terminated.
	 */
	boolean isTerminated();

}