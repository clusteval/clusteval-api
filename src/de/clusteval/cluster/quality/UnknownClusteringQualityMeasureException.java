/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import de.clusteval.framework.repository.UnknownDynamicComponentException;

/**
 * @author Christian Wiwie
 */
public class UnknownClusteringQualityMeasureException extends UnknownDynamicComponentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 433568096995882002L;

	/**
	 * @param name
	 * @param message
	 */
	public UnknownClusteringQualityMeasureException(final String name, String message) {
		super(IClusteringQualityMeasure.class, name, message);
	}

	/**
	 * @param name
	 * @param cause
	 */
	public UnknownClusteringQualityMeasureException(final String name, Throwable cause) {
		super(IClusteringQualityMeasure.class, name, cause);
	}
}
