/**
 * 
 */
package de.clusteval.cluster.quality;

import java.util.HashMap;

import de.clusteval.framework.repository.ISerializableWrapper;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableClusteringQualitySet
		extends
			ISerializableWrapper<IClusteringQualitySet> {

	HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> getQualities();

}