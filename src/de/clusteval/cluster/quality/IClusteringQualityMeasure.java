/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.IOException;

import de.clusteval.cluster.IClustering;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.r.RLibraryInferior;
import de.clusteval.utils.RCalculationException;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusteringQualityMeasure
		extends
			RLibraryInferior,
			IRepositoryObjectDynamicComponent {

	/**
	 * Gets the quality of clustering.
	 * 
	 * @param clustering
	 *            the clustering
	 * @param goldStandard
	 *            The expected goldstandard.
	 * @param dataConfig
	 *            the data config
	 * @return the quality of clustering
	 * @throws InvalidGoldStandardFormatException
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IOException
	 * @throws RNotAvailableException
	 * @throws RCalculationException
	 * @throws InterruptedException
	 */
	IClusteringQualityMeasureValue getQualityOfClustering(
			IClustering clustering, IClustering goldStandard,
			IDataConfig dataConfig) throws InvalidGoldStandardFormatException,
			UnknownDataSetFormatException, IOException,
			InvalidDataSetFormatVersionException, RNotAvailableException,
			RCalculationException, InterruptedException;

	/**
	 * This method has to be implemented in subclasses to indiciate, whether a
	 * quality measure supports validating fuzzy clusterings.
	 * 
	 * @return True, if this measure supports fuzzy clusterings, false
	 *         otherwise.
	 */
	boolean supportsFuzzyClusterings();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IClusteringQualityMeasure clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * This method compares two values of this clustering quality measure and
	 * returns true, if the first one is better than the second one.
	 * 
	 * @param quality1
	 *            The first quality value.
	 * @param quality2
	 *            The second quality value.
	 * @return True, if quality1 is better than quality2
	 */
	boolean isBetterThan(IClusteringQualityMeasureValue quality1,
			IClusteringQualityMeasureValue quality2);

	/**
	 * @return The minimal value of the range of possible values of this quality
	 *         measure.
	 */
	double getMinimum();

	/**
	 * @return The maximal value of the range of possible values of this quality
	 *         measure.
	 */
	double getMaximum();

	/**
	 * Override this method to indicate, whether the quality measure of your
	 * subclass needs a goldstandard to be able to be computed.
	 * 
	 * @return True, if this clustering quality measure requires a goldstandard
	 *         to be able to assess the quality of a clustering.
	 */
	boolean requiresGoldstandard();

	/**
	 * This alias is used whenever this clustering quality measure is visually
	 * represented and a readable name is needed.
	 * 
	 * @return The alias of this clustering quality measure.
	 */
	String getAlias();

	@Override
	ISerializableClusteringQualityMeasure asSerializable()
			throws RepositoryObjectSerializationException;
}