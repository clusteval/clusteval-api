/**
 * 
 */
package de.clusteval.cluster.quality;

import java.util.Map;

import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.IObjectToBeSerialized;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusteringQualitySet
		extends
			Map<IClusteringQualityMeasure, IClusteringQualityMeasureValue>,
			IObjectToBeSerialized {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#clone()
	 */
	IClusteringQualitySet clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IObjectToBeSerialized#asSerializable()
	 */
	@Override
	ISerializableClusteringQualitySet asSerializable()
			throws RepositoryObjectSerializationException;
}