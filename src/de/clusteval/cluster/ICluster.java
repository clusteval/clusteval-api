/**
 * 
 */
package de.clusteval.cluster;

import java.util.Iterator;
import java.util.Map;

/**
 * @author Christian Wiwie
 *
 */
public interface ICluster extends Iterable<IClusterItem> {

	/**
	 * 
	 * @return The id of the cluster.
	 */
	String getId();

	/**
	 * Checks whether this cluster contains a certain item.
	 * 
	 * @param item
	 *            The item to check for.
	 * @return true, if this cluster contains the item, false otherwise.
	 */
	boolean contains(IClusterItem item);

	/**
	 * @return A map with all items contained in this cluster together with
	 *         their fuzzy coefficients.
	 */
	Map<IClusterItem, Float> getFuzzyItems();

	/**
	 * The (fuzzy) size of this cluster is the sum of the fuzzy coefficients of
	 * all items contained in this cluster.
	 * 
	 * <p>
	 * In case that this clustering is a crisp clustering (all fuzzy
	 * coefficients = 1.0), this size is the same as {@link #size()}.
	 * 
	 * @return The fuzzy size of this cluster.
	 */
	float fuzzySize();

	/**
	 * Add a new item to this cluster with a certain fuzzy coefficient.
	 * 
	 * @param item
	 *            The item to add.
	 * @param fuzzy
	 *            The fuzzy coefficient of the new item.
	 * @return true, if successful
	 */
	boolean add(IClusterItem item, float fuzzy);

	/**
	 * Remove an item from this cluster.
	 * 
	 * @param item
	 *            The item to remove.
	 * @return True, if successful
	 */
	boolean remove(IClusterItem item);

	/**
	 * @return The number of items contained in this cluster.
	 * 
	 *         <p>
	 *         In case of fuzzy clusterings this does not necessarily return the
	 *         same as {@link #fuzzySize()}!
	 */
	int size();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	Iterator<IClusterItem> iterator();

	ICluster clone();

}