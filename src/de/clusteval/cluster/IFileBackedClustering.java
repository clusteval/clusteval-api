/**
 * 
 */
package de.clusteval.cluster;

import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public interface IFileBackedClustering extends IClustering {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IClustering#getFormat()
	 */
	IRunResultFormat getFormat();

	/**
	 * @param resultFormat
	 *            the resultFormat to set
	 */
	void setFormat(IRunResultFormat resultFormat);

}