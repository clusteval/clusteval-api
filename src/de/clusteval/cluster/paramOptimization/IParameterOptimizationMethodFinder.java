/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import de.clusteval.utils.IJARFinder;

/**
 * @author Christian Wiwie
 *
 */
public interface IParameterOptimizationMethodFinder extends IJARFinder<IParameterOptimizationMethod> {

}