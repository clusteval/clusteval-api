/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.util.List;
import java.util.Map;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.ISerializableParameterOptimizationRun;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableParameterOptimizationMethod
		extends
			ISerializableWrapperDynamicComponent<IParameterOptimizationMethod> {

	/**
	 * @return the optimizationParameters
	 */
	List<IProgramParameter<?>> getOptimizationParameters();

	/**
	 * @return the parameterMaxValueOverrides
	 */
	Map<IProgramParameter<?>, String> getParameterMaxValueOverrides();

	/**
	 * @return the parameterMinValueOverrides
	 */
	Map<IProgramParameter<?>, String> getParameterMinValueOverrides();

	/**
	 * @return the parameterOptionsOverrides
	 */
	Map<IProgramParameter<?>, String[]> getParameterOptionsOverrides();

	/**
	 * @return the totalIterationCount
	 */
	int getTotalIterationCount();

	void setRun(ISerializableParameterOptimizationRun<? extends IParameterOptimizationRun> run);

}