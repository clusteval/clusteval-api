/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.utils.InternalAttributeException;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 */
public interface IParameterOptimizationMethod
		extends
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#equals(java.lang.
	 * Object )
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#hashCode()
	 */
	int hashCode();

	/**
	 * @param run
	 *            The new run of this method.
	 */
	void setRun(IParameterOptimizationRun run);

	/**
	 * @return This list holds the program parameters that are to be optimized
	 *         by the parameter optimization run.
	 */
	List<IProgramParameter<?>> getOptimizationParameter();

	/**
	 * This method has to be invoked after every iteration and invocation of
	 * {@link #next()}, to pass the qualities of the last clustering to this
	 * parameter optimization method.
	 * 
	 * <p>
	 * If this method is not invoked after {@link #next()}, before
	 * {@link #next()} is invoked again, an {@link IllegalStateException} will
	 * be thrown.
	 * 
	 * @param qualities
	 *            The clustering qualities for the clustering of the last
	 *            iteration.
	 */
	void giveQualityFeedback(long iteration, IClusteringQualitySet qualities);

	/**
	 * @return True, if there are more iterations together with parameter sets
	 *         that have to be evaluated.
	 */
	boolean hasNext();

	/**
	 * This method tells the method that the next parameter set should be
	 * determined and that the invoker wants to start the evaluation of the next
	 * iteration.
	 * 
	 * <p>
	 * This is a convenience method of {@link #next(ParameterSet, long)},
	 * without enforcement of a passed parameter set.
	 * 
	 * <p>
	 * It might happen that this method puts the calling thread to sleep, in
	 * case the next parameter set requires older parameter sets to finish
	 * first.
	 * 
	 * @return The parameter set that is being evaluated in the next iteration.
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws NoParameterSetFoundException
	 *             This exception is thrown, if no parameter set was found that
	 *             was not already evaluated before.
	 * @throws InterruptedException
	 * @throws ParameterSetAlreadyEvaluatedException
	 */
	Pair<Long, ParameterSet> next() throws InternalAttributeException,
			RegisterException, NoParameterSetFoundException,
			InterruptedException, ParameterSetAlreadyEvaluatedException;

	/**
	 * This method tells the method that the next parameter set should be
	 * determined and that the invoker wants to start the evaluation of the next
	 * iteration.
	 * 
	 * <p>
	 * If the force parameter set is given != null, this parameter set is forced
	 * to be evaluated. This scenario is used during resumption of an older run,
	 * where the parameter sets are already fixed and we want to feed them to
	 * this method together with their results exactly as they were performed
	 * last time.
	 * 
	 * <p>
	 * If this method is invoked twice, without
	 * {@link #giveQualityFeedback(IClusteringQualitySet)} being invoked in
	 * between, a {@link IllegalStateException} is thrown.
	 * 
	 * <p>
	 * It might happen that this method puts the calling thread to sleep, in
	 * case the next parameter set requires older parameter sets to finish
	 * first.
	 * 
	 * @param forcedParameterSet
	 *            If this parameter is set != null, this parameter set is forced
	 *            to be evaluated in the next iteration.
	 * @param iterationNumber
	 *            The original number of the iteration when it was previously
	 *            performed.
	 * @return The parameter set that is being evaluated in the next iteration.
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws NoParameterSetFoundException
	 *             This exception is thrown, if no parameter set was found that
	 *             was not already evaluated before.
	 * @throws InterruptedException
	 * @throws ParameterSetAlreadyEvaluatedException
	 */
	Pair<Long, ParameterSet> next(ParameterSet forcedParameterSet,
			long iterationNumber) throws InternalAttributeException,
			RegisterException, NoParameterSetFoundException,
			InterruptedException, ParameterSetAlreadyEvaluatedException;

	/**
	 * @return An array holding the number of iterations that should be
	 *         performed for each optimization parameter. Keep in mind the hints
	 *         in {@link #totalIterationCount}.
	 * @see #totalIterationCount
	 */
	int getIterationPerParameter();

	/**
	 * @return The total iteration count this method should perform.
	 */
	int getTotalIterationCount();

	/**
	 * This method returns the results of this parameter optimization run. Keep
	 * in mind, that this method returns an incomplete object, before the run is
	 * finished.
	 * 
	 * @return An wrapper object holding all the results that are calculated
	 *         throughout execution of the parameter optimization run.
	 */
	IParameterOptimizationResult getResult();

	/**
	 * This method initializes this method by setting the parameter values to be
	 * evaluated during the process ({@link #initParameterValues()}) and by
	 * creating a wrapper object for the {@link #result}.
	 * 
	 * <p>
	 * Furthermore if the run is a resumption of an old run execution, this
	 * method also reperforms all iterations that have been executed during the
	 * last execution of the run.
	 * 
	 * <p>
	 * This method has to be invoked before anything else is done (
	 * {@link #hasNext()} or {@link #next()}).
	 * 
	 * @param absResultPath
	 *            The absolute path pointing to the result file.
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws RunResultParseException
	 * @throws InterruptedException
	 */
	void reset(File absResultPath)
			throws ParameterOptimizationException, InternalAttributeException,
			RegisterException, RunResultParseException, InterruptedException;

	/**
	 * @return The program configuration this method was created for.
	 */
	IProgramConfig getProgramConfig();

	/**
	 * @return The number of iterations that has been performed so far.
	 */
	int getStartedCount();

	/**
	 * 
	 * @return The number of finished iterations, for which we have received
	 *         qualities.
	 */
	int getFinishedCount();

	/**
	 * @return The data configuration this method was created for.
	 */
	IDataConfig getDataConfig();

	IParameterOptimizationMethod clone();

	/**
	 * By default, we take the first parameter as density parameter for plots
	 * 
	 * @return The optimization parameter, that should be used as the axis
	 *         variable to plot the results.
	 */
	IProgramParameter<?> getPlotDensityParameter();

	/**
	 * @param isResume
	 *            A boolean indicating, whether the run is a resumption of a
	 *            previous run execution or a completely new execution.
	 */
	void setResume(boolean isResume);

	/**
	 * This method returns a list with the classes of all dataset formats this
	 * parameter optimization method supports. If the list is empty, all dataset
	 * formats are assumed to be compatible.
	 * 
	 * <p>
	 * Some methods only support certain input formats. If this is the case for
	 * your method, override this method and return the compatible formats.
	 * 
	 * <p>
	 * This compatibility is checked as soon as a parameter optimization run is
	 * parsed in
	 * {@link IParameterOptimizationRun#checkCompatibilityParameterOptimizationMethod}.
	 * 
	 * @return A list with all dataset format classes that are compatible to
	 *         this parameter optimization method.
	 */
	List<Class<? extends IDataSetFormat>> getCompatibleDataSetFormatBaseClasses();

	/**
	 * This method returns a list with all programs this parameter optimization
	 * method supports. If the list is empty, all programs are assumed to be
	 * compatible.
	 * 
	 * <p>
	 * Some methods only support certain programs. If this is the case for your
	 * method, override this method and return the compatible programs.
	 * 
	 * <p>
	 * This compatibility is checked as soon as a parameter optimization run is
	 * parsed in
	 * {@link IParameterOptimizationRun#checkCompatibilityParameterOptimizationMethod}.
	 * 
	 * @return A list with all programs that are compatible to this parameter
	 *         optimization method.
	 */
	List<String> getCompatibleProgramNames();

	/**
	 * This method is used to set the data configuration of this run to another
	 * object. This is needed for example after cloning and conversion of the
	 * old data configuration during the run process.
	 * 
	 * @param dataConfig
	 *            The new data configuration of this run.
	 */
	void setDataConfig(IDataConfig dataConfig);

	/**
	 * This method is used to set the program configuration of this run to
	 * another object. This is needed for example after cloning and conversion
	 * of the old program configuration during the run process.
	 * 
	 * @param programConfig
	 *            The new program configuration of this run.
	 */
	void setProgramConfig(IProgramConfig programConfig);

	int getCurrentCount();

	void initParameterValues()
			throws ParameterOptimizationException, InternalAttributeException;

	ISerializableParameterOptimizationMethod asSerializable()
			throws RepositoryObjectSerializationException;

	Map<IProgramParameter<?>, String[]> getParameterOptionsOverrides();

	Map<IProgramParameter<?>, String> getParameterMaxValueOverrides();

	Map<IProgramParameter<?>, String> getParameterMinValueOverrides();

	IParameterOptimizationRun getRun();
}