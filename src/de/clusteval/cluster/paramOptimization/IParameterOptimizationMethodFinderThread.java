/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import de.clusteval.utils.IFinderThread;

/**
 * @author Christian Wiwie
 *
 */
public interface IParameterOptimizationMethodFinderThread extends IFinderThread<IParameterOptimizationMethod> {

}