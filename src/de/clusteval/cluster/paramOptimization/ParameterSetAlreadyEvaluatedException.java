/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.util.Set;

import de.clusteval.program.ParameterSet;
import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 * 
 */
public class ParameterSetAlreadyEvaluatedException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4017914597786398975L;
	final protected long iterationNumber;
	final protected Set<Long> previousIterationNumber;
	final protected ParameterSet paramSet;

	/**
	 * @param iterationNumber
	 * @param previousIterationNumber
	 * @param paramSet
	 * 
	 */
	public ParameterSetAlreadyEvaluatedException(final long iterationNumber,
			final Set<Long> previousIterationNumber,
			final ParameterSet paramSet) {
		super("");
		this.iterationNumber = iterationNumber;
		this.previousIterationNumber = previousIterationNumber;
		this.paramSet = paramSet;
	}

	public long getIterationNumber() {
		return iterationNumber;
	}

	public Set<Long> getPreviousIterationNumber() {
		return previousIterationNumber;
	}

	public ParameterSet getParameterSet() {
		return paramSet;
	}
}
