/**
 * 
 */
package de.clusteval.utils;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.artifact.versioning.VersionRange;

/**
 * @author Christian Wiwie
 *
 */
public class IncompatibleClustEvalVersionException extends ClustEvalException
		implements
			IExceptionWithDependent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2429266657411828012L;

	protected String dependent;
	protected ComparableVersion dependentVersion;
	protected VersionRange versionRequirement;
	protected Class<?> target;
	protected String targetVersion;

	/**
	 * @param dependent
	 * @param versionRequirement
	 * @param target
	 * @param targetVersion
	 */
	public IncompatibleClustEvalVersionException(final String dependent,
			VersionRange versionRequirement, Class<?> target,
			String targetVersion) {
		super(String.format(
				"Incompatible with this ClustEval server (depends on %s version %s, but is %s)",
				target.getSimpleName(), versionRequirement, targetVersion));
		this.dependent = dependent;
		this.versionRequirement = versionRequirement;
		this.target = target;
		this.targetVersion = targetVersion;
	}

	/**
	 * @return the versionRequirement
	 */
	public VersionRange getTargetVersionRequirement() {
		return versionRequirement;
	}

	/**
	 * @return the target
	 */
	public Class<?> getTarget() {
		return target;
	}

	/**
	 * @return the targetVersion
	 */
	public String getActualTargetVersion() {
		return targetVersion;
	}

	/**
	 * @return the dependent
	 */
	@Override
	public String getDependent() {
		return dependent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.getMessage(true);
	}

	public String getMessage(final boolean prefixWithDependent) {
		if (prefixWithDependent)
			return String.format("%s (%s): %s", dependent, dependentVersion,
					super.getMessage());
		return super.getMessage();
	}

}
