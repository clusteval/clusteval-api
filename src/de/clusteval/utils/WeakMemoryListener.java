/**
 * 
 */
package de.clusteval.utils;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * @author Christian Wiwie
 *
 */
public class WeakMemoryListener extends WeakReference<IInMemoryListener> {

	/**
	 * @param referent
	 * @param q
	 */
	public WeakMemoryListener(IInMemoryListener referent,
			ReferenceQueue<? super IInMemoryListener> q) {
		super(referent, q);
	}

	/**
	 * @param referent
	 */
	public WeakMemoryListener(IInMemoryListener referent) {
		super(referent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.get().hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof WeakMemoryListener))
			return false;
		return this.get().equals(((WeakMemoryListener) obj).get());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.get().toString();
	}
}