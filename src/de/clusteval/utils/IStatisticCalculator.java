/**
 * 
 */
package de.clusteval.utils;

import java.io.File;

import org.rosuda.REngine.REngineException;

import de.clusteval.data.statistics.StatisticCalculateException;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IStatisticCalculator<T extends IStatistic>
		extends
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IStatisticCalculator<T> clone();

	/**
	 * Calculate the result. This method stores the calculated result in the
	 * {@link #lastResult} attribute for later usage, e.g. in
	 * {@link #writeOutputTo(File)}.
	 * 
	 * @return The calculated statistic.
	 * @throws StatisticCalculateException
	 */
	T calculate() throws StatisticCalculateException;

	/**
	 * @param absFolderPath
	 *            The absolute path to the folder where the statistic should be
	 *            written to.
	 * @throws REngineException
	 * @throws RNotAvailableException
	 * @throws InterruptedException
	 */
	void writeOutputTo(File absFolderPath) throws REngineException,
			RNotAvailableException, InterruptedException;

	/**
	 * @return The statistic calculated during the last {@link #calculate()}
	 *         invocation.
	 */
	T getStatistic();

}