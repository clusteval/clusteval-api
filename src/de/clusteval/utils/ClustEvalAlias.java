package de.clusteval.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Christian Wiwie
 * 
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface ClustEvalAlias {

	/**
	 * 
	 * @return A readable alias of the annotated object.
	 */
	String alias();
}
