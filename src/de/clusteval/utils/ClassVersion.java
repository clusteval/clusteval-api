package de.clusteval.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation should be used to annotate classes with their current
 * version. It can be used to declare dependencies between classes.<br>
 * 
 * <ul>
 * <li>The major portion of the interface's version should be incremented iff a
 * method is added or deleted.</li>
 * <li>The minor portion of the interface's version should be incremented iff a
 * method signature is changed.</li>
 * <li>The (optional) incremental portion of the interface's version should be
 * incremented for non-breaking bug-fix changes.</li>
 * </ul>
 * 
 * @author Christian Wiwie
 * 
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ClassVersion {

	/**
	 * @return The version of the annotated class, as a maven formatted version
	 *         string.
	 */
	String version();
}
