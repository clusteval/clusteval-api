/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public class MissingClustEvalVersionException extends ClustEvalException
		implements
			IExceptionWithDependent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8564467236839224416L;
	protected String dependent;

	/**
	 * @param dependent
	 */
	public MissingClustEvalVersionException(final String dependent) {
		super(String.format("Class is missing the '%s' annotation",
				ClassVersionRequirement.class.getSimpleName()));
		this.dependent = dependent;
	}

	/**
	 * @return the dependent
	 */
	@Override
	public String getDependent() {
		return dependent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.getMessage(true);
	}

	public String getMessage(final boolean prefixWithDependent) {
		if (prefixWithDependent)
			return String.format("%s: %s", dependent, super.getMessage());
		return super.getMessage();
	}
}
