package de.clusteval.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.maven.artifact.versioning.VersionRange;

/**
 * @author Christian Wiwie
 * 
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Repeatable(ClassVersionRequirements.class)
public @interface ClassVersionRequirement {

	/**
	 * 
	 * @return The target of this dependency
	 */
	Class<?> target();

	/**
	 * 
	 * @return A specification of a version range which is defined according to
	 *         and will be parsed using
	 *         {@link VersionRange#createFromVersionSpec(String)}.
	 */
	String versionSpecification();
}
