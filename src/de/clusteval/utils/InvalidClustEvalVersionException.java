/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public class InvalidClustEvalVersionException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3792260527461150479L;
	protected String dependent;
	protected Class<?> target;
	protected String targetVersion;

	/**
	 * @param dependent
	 * @param target
	 * @param targetVersion
	 */
	public InvalidClustEvalVersionException(final String dependent,
			Class<?> target, String targetVersion) {
		super(String.format(
				"'%s' has an invalid version specification in a '%s' annotation and cannot be loaded: Requires version '%s' of '%s'",
				dependent, ClassVersionRequirement.class.getSimpleName(),
				targetVersion, target.getSimpleName()));
		this.dependent = dependent;
	}

	/**
	 * @return the dependent
	 */
	public String getDependent() {
		return dependent;
	}

	/**
	 * @return the targetVersion
	 */
	public String getTargetVersion() {
		return targetVersion;
	}

	/**
	 * @return the target
	 */
	public Class<?> getTarget() {
		return target;
	}
}
