/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 3, 2017
 */
public interface IExceptionWithDependent {

	String getDependent();

	String getMessage();

	String getMessage(boolean prefixWithDependent);
}
