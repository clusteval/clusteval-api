package de.clusteval.utils;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 */
public class DynamicComponentInitializationException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7581743389963113554L;
	protected Class<? extends IRepositoryObject> clazz;
	protected String objectName;

	public DynamicComponentInitializationException(Class<? extends IRepositoryObject> clazz, String objectName,
			String message) {
		super(message);
		this.clazz = clazz;
		this.objectName = objectName;
	}

	public DynamicComponentInitializationException(Class<? extends IRepositoryObject> clazz, String objectName,
			final Throwable cause) {
		super(cause);
		this.clazz = clazz;
		this.objectName = objectName;
	}
}
