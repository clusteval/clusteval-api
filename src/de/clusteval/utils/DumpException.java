/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class DumpException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4831496638059890251L;

	/**
	 * @param message
	 */
	public DumpException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DumpException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DumpException(String message, Throwable cause) {
		super(message, cause);
	}

}
