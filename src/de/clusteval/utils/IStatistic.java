/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.r.RLibraryInferior;

/**
 * @author Christian Wiwie
 *
 */
public interface IStatistic
		extends
			RLibraryInferior,
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IStatistic clone();

	/**
	 * The string returned by this method is used to represent this type of
	 * statistic throughout the framework (e.g. in the configuration files)
	 * 
	 * @return A string representing this statistic class.
	 */
	String getIdentifier();

	/**
	 * Parses the values of a statistic from a string and stores them in the
	 * local attributes of this object.
	 * 
	 * @param contents
	 *            The string to parse the values from.
	 * 
	 */
	void parseFromString(String contents);

	String getResultAsString();

	/**
	 * This alias is used whenever this statistic is visually represented and a
	 * readable name is needed.
	 * 
	 * @return The alias of this statistic.
	 */
	String getAlias();

	@Override
	ISerializableStatistic asSerializable()
			throws RepositoryObjectSerializationException;
}