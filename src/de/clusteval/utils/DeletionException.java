/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public class DeletionException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5440381799506215306L;

	/**
	 * @param message
	 */
	public DeletionException(String message) {
		super(message);
	}

}
