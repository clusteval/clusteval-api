/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.net.MalformedURLException;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IJARFinder<T extends IRepositoryObject> extends IFinder<T> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#doOnFileFound(java.io.File)
	 */
	void doOnFileFound(File file) throws RegisterException, RepositoryObjectParseException, MalformedURLException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#findAndRegisterObjects()
	 */
	void findAndRegisterObjects() throws RegisterException, InterruptedException;

}