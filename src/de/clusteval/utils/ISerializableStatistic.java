/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableStatistic<S extends IStatistic> extends ISerializableWrapperDynamicComponent<S> {

}