/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface INamedAttribute<T> extends IRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/**
	 * @return The name of this attribute.
	 */
	String getName();

	/**
	 * @return The value of this attribute.
	 */
	T getValue();

	/**
	 * @param value
	 *            The new value of this attribute.
	 */
	void setValue(T value);

	String toString();

}