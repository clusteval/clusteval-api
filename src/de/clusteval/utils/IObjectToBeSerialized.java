/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IObjectToBeSerialized {

	/**
	 * @return A version of this object that can be serialized and additionally
	 *         makes sure that attributes of instances of this class are
	 *         maintained. This includes at least the name and the version of
	 *         this object. Sub-classes may include serialized copies of more
	 *         additional attributes.
	 * @throws RepositoryObjectSerializationException
	 */
	ISerializableWrapper<?> asSerializable()
			throws RepositoryObjectSerializationException;

	/**
	 * @return A flag indicating whether this class is serializable as is and
	 *         can be included as original object references in instances
	 *         created with {@link #asSerializable()}.
	 */
	boolean isSerializable();
}
