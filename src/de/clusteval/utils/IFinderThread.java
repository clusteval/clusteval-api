/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.IClustevalThread;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IFinderThread<T extends IRepositoryObject> extends IClustevalThread {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	void run();

	IFinder<T> getFinder() throws RegisterException;

}