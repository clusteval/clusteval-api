/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public class InvalidDependencyTargetException extends ClustEvalException
		implements
			IExceptionWithDependent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 424369063538900571L;
	protected String dependent;
	protected Class<?> target;

	/**
	 * @param dependent
	 * @param target
	 */
	public InvalidDependencyTargetException(final String dependent,
			Class<?> target) {
		super(String.format(
				"The target '%s' of a dependency is missing the '%s' annotation",
				target.getSimpleName(), ClassVersion.class.getSimpleName()));
		this.dependent = dependent;
	}

	/**
	 * @return the dependent
	 */
	public String getDependent() {
		return dependent;
	}

	/**
	 * @return the target
	 */
	public Class<?> getTarget() {
		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IExceptionWithDependent#getMessage(boolean)
	 */
	@Override
	public String getMessage(boolean prefixWithDependent) {
		if (prefixWithDependent)
			return String.format("%s: %s", dependent, super.getMessage());
		return super.getMessage();
	}
}
