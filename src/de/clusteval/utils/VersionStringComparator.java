/**
 * 
 */
package de.clusteval.utils;

import java.util.Comparator;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 *
 */
public class VersionStringComparator implements Comparator<String> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(String o1, String o2) {
		return new ComparableVersion(o1).compareTo(new ComparableVersion(o2));
	}

}
