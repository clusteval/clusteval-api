/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.net.MalformedURLException;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IFinder<T extends IRepositoryObject> extends IRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IFinder<T> clone();

	/**
	 * Find files and try to parse them. If the parsing process is successful it
	 * will implicitely register the new object at the repository. if the object
	 * was known before it will updated, if and only if the changeDate of the
	 * found object is newer.
	 * 
	 * @throws RegisterException
	 * @throws InterruptedException
	 */
	void findAndRegisterObjects() throws RegisterException, InterruptedException;

	void doOnFileFound(File file) throws RegisterException, RepositoryObjectParseException, MalformedURLException;

	void notify(RepositoryEvent e);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	boolean foundInLastRun();

}