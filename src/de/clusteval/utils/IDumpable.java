/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public interface IDumpable {

	/**
	 * @throws DumpException
	 */
	void dumpToFile() throws DumpException;
}
