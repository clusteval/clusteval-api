/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public interface INamedIntegerAttribute extends INamedAttribute<Integer> {

}