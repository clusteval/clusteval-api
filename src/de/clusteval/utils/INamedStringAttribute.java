/**
 * 
 */
package de.clusteval.utils;

/**
 * @author Christian Wiwie
 *
 */
public interface INamedStringAttribute extends INamedAttribute<String> {

}