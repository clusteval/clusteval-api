/**
 * 
 */
package de.clusteval.auth;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class UnknownClientException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6281320556362856213L;

	/**
	 * 
	 */
	public UnknownClientException() {
		super("");
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownClientException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UnknownClientException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UnknownClientException(Throwable cause) {
		super(cause);
	}

}
