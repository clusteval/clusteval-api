/**
 * 
 */
package de.clusteval.auth;

/**
 * @author Christian Wiwie
 *
 */
public class BasicClientPermissions implements IClientPermissions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7837617663207848481L;
	protected boolean canShutdown, canStartRun, canStopRun, canUpload, canDelete;

	/**
	 * 
	 */
	public BasicClientPermissions() {
		super();
		this.canStartRun = true;
		this.canStopRun = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.auth.IUserPermissions#canShutdown()
	 */
	@Override
	public boolean canShutdown() {
		return canShutdown;
	}

	/**
	 * @param canShutdown
	 *            the canShutdown to set
	 */
	public void setCanShutdown(boolean canShutdown) {
		this.canShutdown = canShutdown;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.auth.IUserPermissions#canStartRun()
	 */
	@Override
	public boolean canStartRun() {
		return canStartRun;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.auth.IUserPermissions#canStopRun()
	 */
	@Override
	public boolean canStopRun() {
		return canStopRun;
	}

	/**
	 * @param canStartRun
	 *            the canStartRun to set
	 */
	public void setCanStartRun(boolean canStartRun) {
		this.canStartRun = canStartRun;
	}

	/**
	 * @param canStopRun
	 *            the canStopRun to set
	 */
	public void setCanStopRun(boolean canStopRun) {
		this.canStopRun = canStopRun;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.auth.IClientPermissions#canDelete()
	 */
	@Override
	public boolean canDelete() {
		return this.canDelete;
	}

	/**
	 * @param canDelete
	 *            the canDelete to set
	 */
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.auth.IClientPermissions#canUpload()
	 */
	@Override
	public boolean canUpload() {
		return this.canUpload;
	}

	/**
	 * @param canUpload
	 *            the canUpload to set
	 */
	public void setCanUpload(boolean canUpload) {
		this.canUpload = canUpload;
	}
}
