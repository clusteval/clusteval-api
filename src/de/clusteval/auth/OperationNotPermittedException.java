/**
 * 
 */
package de.clusteval.auth;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class OperationNotPermittedException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5176547908616728278L;

	/**
	 * 
	 */
	public OperationNotPermittedException() {
		super("");
	}

	/**
	 * @param message
	 * @param cause
	 */
	public OperationNotPermittedException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public OperationNotPermittedException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public OperationNotPermittedException(Throwable cause) {
		super(cause);
	}

}
