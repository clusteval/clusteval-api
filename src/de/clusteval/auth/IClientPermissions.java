/**
 * 
 */
package de.clusteval.auth;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 *
 */
public interface IClientPermissions extends Serializable {

	/**
	 * 
	 * @return True, if the client is allowed to shutdown the ClustEval server.
	 */
	boolean canShutdown();

	/**
	 * 
	 * @return True, if the client is allowed to start new runs.
	 */
	boolean canStartRun();

	/**
	 * 
	 * @return True, if the client is alloed to stop own runs.
	 */
	boolean canStopRun();

	/**
	 * 
	 * @return True, if the client is allowed to upload new files / executables
	 *         to the repository.
	 */
	boolean canUpload();

	/**
	 * 
	 * @return True, if the client is allowed to delete files / executables from
	 *         the repository.
	 */
	boolean canDelete();
}
