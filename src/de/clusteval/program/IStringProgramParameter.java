/**
 * 
 */
package de.clusteval.program;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.utils.InternalAttributeException;

/**
 * @author Christian Wiwie
 *
 */
public interface IStringProgramParameter extends IProgramParameter<String> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#clone(program.ProgramParameter)
	 */
	IStringProgramParameter clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMinValueSet()
	 */
	boolean isMinValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMaxValueSet()
	 */
	boolean isMaxValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMinValue()
	 */
	String evaluateMinValue(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMaxValue()
	 */
	String evaluateMaxValue(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateDefaultValue()
	 */
	String evaluateDefaultValue(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ProgramParameter#evaluateOptions(de.clusteval.data
	 * .DataConfig, de.clusteval.program.ProgramConfig)
	 */
	String[] evaluateOptions(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramParameter#isOptionsSet()
	 */
	boolean isOptionsSet();

}