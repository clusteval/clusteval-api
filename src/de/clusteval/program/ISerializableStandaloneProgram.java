/**
 * 
 */
package de.clusteval.program;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableStandaloneProgram extends ISerializableProgram<IStandaloneProgram> {

	/**
	 * @return the context
	 */
	ISerializableWrapperRepositoryObject<IContext> getContext();

}