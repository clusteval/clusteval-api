/**
 * 
 */
package de.clusteval.program;

import java.io.IOException;
import java.util.Map;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 *
 */
public interface IProgram extends IRepositoryObject {

	boolean hasAlias();

	String getAlias();

	IProgram clone();

	/**
	 * Gets the absolute path of the executable.
	 * 
	 * @return the executable
	 */
	String getExecutable();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * This method returns the major name of this program. The major name of the
	 * program is defined as the foldername its executable lies in.
	 * 
	 * @return The major name of this program.
	 */
	String getMajorName();

	/**
	 * This method returns the minor name of this program. The minor name
	 * corresponds to the name of the executable of this program.
	 * 
	 * @return The minor name.
	 */
	String getMinorName();

	/**
	 * This method returns the full name of this program. The full name
	 * corresponds to the concatenated major and minor name separated by a
	 * slash: MAJOR/MINOR
	 * 
	 * @return The full name.
	 */
	String getFullName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/**
	 * This method executes this program on the data defined in the data
	 * configuration.
	 * 
	 * <p>
	 * The complete invocation line is also passed. It is taken from the program
	 * configuration used by the run. All parameter placeholders contained in
	 * this invocation line are already replaced by their actual values.
	 * 
	 * <p>
	 * Additionally all parameter values are passed in the two map parameters.
	 * 
	 * @param dataConfig
	 *            This configuration encapsulates the data, this program should
	 *            be applied to.
	 * @param programConfig
	 *            This parameter contains some additional configuration for this
	 *            program.
	 * @param invocationLine
	 *            This is the complete invocation line, were all parameter
	 *            placeholders are already replaced by their actual values.
	 * @param effectiveParams
	 *            This map contains only the program parameters defined in the
	 *            program configuration together with their actual values.
	 * @param internalParams
	 *            This map contains parameters, that are not program specific,
	 *            but related and necessary for the execution of the program,
	 *            e.g. the path to the output or log files created by the
	 *            program.
	 * @return A Process object which can be used to get the status of or to
	 *         control the execution of this program.
	 * @throws IOException
	 * @throws RNotAvailableException
	 * @throws RLibraryNotLoadedException
	 * @throws REngineException
	 * @throws REXPMismatchException
	 * @throws InterruptedException
	 */
	Process exec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws IOException,
			RNotAvailableException, RLibraryNotLoadedException,
			REngineException, REXPMismatchException, InterruptedException;

	/**
	 * @return The context of this program. A run can only perform this program,
	 *         if it has the same context.
	 * @throws UnknownContextException
	 * @throws DynamicComponentInitializationException
	 */
	IContext getContext() throws UnknownContextException,
			DynamicComponentInitializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableProgram asSerializable()
			throws RepositoryObjectSerializationException;
}