/**
 * 
 */
package de.clusteval.program;

import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableProgram<P extends IProgram>
		extends
			ISerializableWrapperRepositoryObject<P> {

	/**
	 * @return the majorName
	 */
	String getMajorName();

	/**
	 * @return the minorName
	 */
	String getMinorName();

	String getAlias();

	boolean hasAlias();

}