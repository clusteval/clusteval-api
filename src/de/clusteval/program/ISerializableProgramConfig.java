/**
 * 
 */
package de.clusteval.program;

import java.util.List;

import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableProgramConfig<PC extends IProgramConfig>
		extends
			ISerializableWrapperRepositoryObject<PC> {

	/**
	 * @return the program
	 */
	ISerializableProgram<? extends IProgram> getProgram();

	boolean expectsNormalizedDataSet();

	int getMaxExecutionTimeMinutes();

	boolean supportsInternalParameterOptimization();

	@Override
	PC deserialize(IRepository repository) throws DeserializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#getParameterForName(java.
	 * lang.String)
	 */
	IProgramParameter<?> getParameterForName(String name);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getParams()
	 */
	List<IProgramParameter<?>> getParameters();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#getParamWithId(java.lang.
	 * String)
	 */
	IProgramParameter<?> getParamWithId(String id)
			throws UnknownProgramParameterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getCompatibleDataSetFormats()
	 */
	List<ISerializableDataSetFormat> getCompatibleDataSetFormats();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getInvocationFormat(
	 * boolean)
	 */
	String getInvocationFormat(boolean withoutGoldStandard);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#getOptimizableParams()
	 */
	List<IProgramParameter<?>> getOptimizableParameters();

	/**
	 * @return the outputFormat
	 */
	ISerializableWrapperRepositoryObject<IRunResultFormat> getOutputFormat();
}