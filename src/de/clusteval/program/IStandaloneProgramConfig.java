/**
 * 
 */
package de.clusteval.program;

import java.util.Map;

/**
 * @author Christian Wiwie
 *
 */
public interface IStandaloneProgramConfig extends IProgramConfig {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#clone()
	 */
	IStandaloneProgramConfig clone();

	Map<String, String> getEnvVars();

	String getProgramAlias();

	@Override
	IStandaloneProgram getProgram();

}