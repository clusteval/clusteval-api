/**
 * 
 */
package de.clusteval.program;

import java.util.List;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface IProgramConfig
		extends
			IRepositoryObjectWithVersion,
			IDumpableRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IProgramConfig clone();

	/**
	 * @return True, if the encapsulated program requires normalized input data,
	 *         false otherwise.
	 * @see #expectsNormalizedDataSet
	 */
	boolean expectsNormalizedDataSet();

	/**
	 * 
	 * @return The maximal number of minutes a single iteration of this program
	 *         configuration should be executed. An iteration is terminated when
	 *         this time is reached.
	 * 
	 *         A value of -1 corresponds to unlimited execution time.
	 */
	int getMaxExecutionTimeMinutes();

	void setMaxExecutionTimeMinutes(int maxExecutionTimeMinutes);

	/**
	 * Internal Parameter Optimization is an alternative for parameter
	 * optimization, in that the program handles the parameter optimization
	 * itself. In this case, the framework invokes the program only once.
	 * 
	 * @return True, if the encapsulated program supports internal parameter
	 *         optimization, false otherwise.
	 */
	boolean supportsInternalParameterOptimization();

	/**
	 * 
	 * @return The encapsulated program.
	 * @see #program
	 */
	IProgram getProgram();

	/**
	 * @return The name of the program configuration is the name of the file
	 *         without extension.
	 */
	String getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	@Override
	public ISerializableProgramConfig asSerializable()
			throws RepositoryObjectSerializationException;

	public String getProgramAlias();

	/**
	 * 
	 * @return The list of optimizable parameters of the encapsulated program.
	 * @see #optimizableParameters
	 */
	List<IProgramParameter<?>> getOptimizableParameters();

	/**
	 * 
	 * @return The compatible dataset input formats of the encapsulated program.
	 * @see #compatibleDataSetFormats
	 */
	List<IDataSetFormat> getCompatibleDataSetFormats();

	/**
	 * This method returns the invocation line format for non
	 * parameter-optimization runs.
	 * 
	 * @param withoutGoldStandard
	 *            This boolean indicates, whether this method returns the
	 *            invocation format for the case with- or without goldstandard.
	 * 
	 * @return The invocation line format
	 */
	String getInvocationFormat(boolean withoutGoldStandard);

	/**
	 * This method returns the invocation line format for parameter-optimization
	 * runs.
	 * 
	 * @param withoutGoldStandard
	 *            This boolean indicates, whether this method returns the
	 *            invocation format for the case with- or without goldstandard.
	 * 
	 * @return The invocation line format
	 */
	String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard);

	/**
	 * 
	 * @return The output format of the encapsulated program.
	 * @see #outputFormat
	 */
	IRunResultFormat getOutputFormat();

	/**
	 * TODO: merge this and {@link #getParamWithId(String)}
	 * 
	 * @param name
	 *            the name
	 * @return the parameter for name
	 */
	IProgramParameter<?> getParameterForName(String name);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getParams()
	 */
	List<IProgramParameter<?>> getParameters();

	/**
	 * This method returns the program parameter with the given id and throws an
	 * exception, of none such parameter exists.
	 * 
	 * @param id
	 *            The name the parameter should have.
	 * @throws UnknownProgramParameterException
	 * @return The program parameter with the appropriate name
	 */
	IProgramParameter<?> getParamWithId(String id)
			throws UnknownProgramParameterException;
}