/**
 * 
 */
package de.clusteval.program;

import java.io.IOException;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IStandaloneProgram
		extends
			IProgram,
			IRepositoryObjectWithVersion {

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#clone()
	 */
	IStandaloneProgram clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#exec(program.ProgramConfig, java.lang.String,
	 * java.util.Map)
	 */
	Process exec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws IOException;

	IContext getContext();

	/**
	 * @return the version
	 */
	ComparableVersion getVersion();

	@Override
	ISerializableStandaloneProgram asSerializable()
			throws RepositoryObjectSerializationException;
}