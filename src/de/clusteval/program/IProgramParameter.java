/**
 * 
 */
package de.clusteval.program;

import java.io.Serializable;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.utils.InternalAttributeException;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IProgramParameter<T> extends Serializable {

	IProgramParameter<T> clone();

	/**
	 * Returns a copy of this instance with a different minimum.
	 * 
	 * @param minValue
	 *            The new minimal value.
	 */
	IProgramParameter<T> withDifferentMin(String minValue);

	/**
	 * @return The minimal value of this parameter.
	 */
	String getMinValue();

	/**
	 * This method evaluates the string representation of the minimal value
	 * {@link #minValue} to a value corresponding to the dynamic type of this
	 * object, e.g. in case this parameter is a double parameter, it is
	 * evaluated to a double value.
	 * 
	 * <p>
	 * The method requires a data and program configuration, since the string
	 * representation can contain a placeholder of a internal variable which is
	 * replaced by looking it up during runtime. $(meanSimilarity) for example
	 * is evaluated by looking into the data and calculating the mean similarity
	 * of the input.
	 * 
	 * @param dataConfig
	 *            The data configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @param programConfig
	 *            The program configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @return The evaluated value of the {@link #minValue} variable.
	 * @throws InternalAttributeException
	 */
	T evaluateMinValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/**
	 * This method checks, whether the {@link #minValue} variable has been set
	 * to a correct not-null value.
	 * 
	 * @return True, if the variable has been set correctly, false otherwise.
	 */
	boolean isMinValueSet();

	/**
	 * Returns a copy of this instance with a different maximum.
	 * 
	 * @param maxValue
	 *            The new maximal value
	 */
	IProgramParameter<T> withDifferentMax(String maxValue);

	/**
	 * @return The maximal value of this parameter.
	 */
	String getMaxValue();

	/**
	 * This method evaluates the string representation of the maximal value
	 * {@link #maxValue} to a value corresponding to the dynamic type of this
	 * object, e.g. in case this parameter is a double parameter, it is
	 * evaluated to a double value.
	 * 
	 * <p>
	 * The method requires a data and program configuration, since the string
	 * representation can contain a placeholder of a internal variable which is
	 * replaced by looking it up during runtime. $(meanSimilarity) for example
	 * is evaluated by looking into the data and calculating the mean similarity
	 * of the input.
	 * 
	 * @param dataConfig
	 *            The data configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @param programConfig
	 *            The program configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @return The evaluated value of the {@link #maxValue} variable.
	 * @throws InternalAttributeException
	 */
	T evaluateMaxValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/**
	 * This method checks, whether the {@link #maxValue} variable has been set
	 * to a correct not-null value.
	 * 
	 * @return True, if the variable has been set correctly, false otherwise.
	 */
	boolean isMaxValueSet();

	/**
	 * Sets the minimal value.
	 * 
	 * @param options
	 *            The possible values of this parameter.
	 */
	IProgramParameter<T> withDifferentOptions(String[] options);

	/**
	 * @return The possible values of this parameter.
	 */
	String[] getOptions();

	/**
	 * This method evaluates the string representation of the options
	 * {@link #options} to a value corresponding to the dynamic type of this
	 * object, e.g. in case this parameter is a double parameter, it is
	 * evaluated to a double value.
	 * 
	 * <p>
	 * The method requires a data and program configuration, since the string
	 * representation can contain a placeholder of a internal variable which is
	 * replaced by looking it up during runtime. $(meanSimilarity) for example
	 * is evaluated by looking into the data and calculating the mean similarity
	 * of the input.
	 * 
	 * @param dataConfig
	 *            The data configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @param programConfig
	 *            The program configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @return The evaluated value of the {@link #minValue} variable.
	 * @throws InternalAttributeException
	 */
	T[] evaluateOptions(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/**
	 * This method checks, whether the {@link #options} variable has been set to
	 * a correct not-null value.
	 * 
	 * @return True, if the variable has been set correctly, false otherwise.
	 */
	boolean isOptionsSet();

	/**
	 * @return The name of this parameter.
	 */
	String getName();

	// changed 12.07.2012
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object o);

	/**
	 * @return The default value of this parameter.
	 */
	String getDefault();

	/**
	 * @return The description of this parameter.
	 */
	String getDescription();

	/**
	 * This method evaluates the string representation of the default value
	 * {@link #def} to a value corresponding to the dynamic type of this object,
	 * e.g. in case this parameter is a double parameter, it is evaluated to a
	 * double value.
	 * 
	 * <p>
	 * The method requires a data and program configuration, since the string
	 * representation can contain a placeholder of a internal variable which is
	 * replaced by looking it up during runtime. $(meanSimilarity) for example
	 * is evaluated by looking into the data and calculating the mean similarity
	 * of the input.
	 * 
	 * @param dataConfig
	 *            The data configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @param programConfig
	 *            The program configuration which might be needed to evaluate
	 *            certain placeholder variables.
	 * @return The evaluated value of the {@link #def} variable.
	 * @throws InternalAttributeException
	 */
	T evaluateDefaultValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/**
	 * Returns a copy of this instance with a different default.
	 * 
	 * @param def
	 *            The new default value of this parameter.
	 */
	IProgramParameter<T> withDifferentDefault(String def);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

}