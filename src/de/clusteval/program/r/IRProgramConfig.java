/**
 * 
 */
package de.clusteval.program.r;

import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;

/**
 * @author Christian Wiwie
 *
 */
public interface IRProgramConfig extends IProgramConfig {

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramConfig#clone()
	 */
	IRProgramConfig clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#getProgram()
	 */
	IRProgram getProgram();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#asSerializable()
	 */
	@Override
	ISerializableRProgramConfig asSerializable()
			throws RepositoryObjectSerializationException;

}