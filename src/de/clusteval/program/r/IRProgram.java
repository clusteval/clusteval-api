/**
 * 
 */
package de.clusteval.program.r;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.IMyRengine;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 *
 */
public interface IRProgram extends RLibraryInferior, IProgram {

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#clone()
	 */
	IRProgram clone();

	/**
	 * The major name of a RProgram corresponds to the simple name of its class.
	 */
	String getMajorName();

	/**
	 * @return The format of the invocation line of this RProgram.
	 */
	String getInvocationFormat();

	/**
	 * @return A set containing dataset formats, which this r program can take
	 *         as input.
	 */
	Set<IDataSetFormat> getCompatibleDataSetFormats();

	/**
	 * @return The runresult formats, the results of this r program will be
	 *         generated in.
	 */
	IRunResultFormat getRunResultFormat();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#exec(de.clusteval.data.DataConfig,
	 * de.clusteval.program.ProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	Process exec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws REngineException;

	/**
	 * @return The r engine corresponding to this rprogram.
	 */
	IMyRengine getRengine();

	void afterExec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws REXPMismatchException,
			REngineException, IOException, InterruptedException;

	void doExec(IDataConfig dataConfig, IProgramConfig programConfig,
			final String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws RserveException, InterruptedException;

	void beforeExec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, RLibraryNotLoadedException,
			RNotAvailableException, InterruptedException;

	void setRengine(IMyRengine rEngine);

	List<IProgramParameter<?>> createParameters();

	@Override
	ISerializableRProgram asSerializable()
			throws RepositoryObjectSerializationException;

	List<IProgramParameter<?>> getParameters();

	/**
	 * This alias is used whenever this program is visually represented and a
	 * readable name is needed.
	 * 
	 * @return The alias of this program.
	 */
	String getAlias();

	void initData(IDataConfig dataConfig, IProgramConfig programConfig, String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws REngineException, InterruptedException;
}