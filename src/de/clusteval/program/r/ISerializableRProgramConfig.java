/**
 * 
 */
package de.clusteval.program.r;

import java.util.List;

import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableRProgramConfig extends ISerializableProgramConfig<IRProgramConfig> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getProgram()
	 */
	ISerializableRProgram getProgram();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#expectsNormalizedDataSet(
	 * )
	 */
	boolean expectsNormalizedDataSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getMaxExecutionTimeMinutes()
	 */
	int getMaxExecutionTimeMinutes();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * supportsInternalParameterOptimization()
	 */
	boolean supportsInternalParameterOptimization();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#deserialize(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	IRProgramConfig deserialize(IRepository repository) throws DeserializationException;
}