/**
 * 
 */
package de.clusteval.program.r;

import java.util.List;
import java.util.Set;

import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableRProgram extends ISerializableProgram<IRProgram> {

	/**
	 * @return
	 */
	List<IProgramParameter<?>> getParameters();

	Set<ISerializableDataSetFormat> getCompatibleDataSetFormats();

	String getInvocationFormat();

	ISerializableRunResultFormat getRunResultFormat();

}