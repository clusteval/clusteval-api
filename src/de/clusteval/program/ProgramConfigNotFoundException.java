/**
 * 
 */
package de.clusteval.program;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ProgramConfigNotFoundException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5105549970318382452L;

	protected ComparableVersion version;

	public ProgramConfigNotFoundException(final String objectName) {
		this(objectName, null);
	}

	/**
	 * 
	 */
	public ProgramConfigNotFoundException(final String objectName, final ComparableVersion version) {
		super(version == null
				? String.format("The program configuration '%s' could not be found.", objectName)
				: String.format("Version 'v%s' of program configuration '%s' could not be found.", version,
						objectName));
		this.version = version;

	}
}
