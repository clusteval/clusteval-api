/**
 * 
 */
package de.clusteval.program;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.utils.InternalAttributeException;

/**
 * @author Christian Wiwie
 *
 */
public interface IIntegerProgramParameter extends IProgramParameter<Integer> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#clone(program.ProgramParameter)
	 */
	IIntegerProgramParameter clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMinValueSet()
	 */
	boolean isMinValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMaxValueSet()
	 */
	boolean isMaxValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMinValue()
	 */
	Integer evaluateMinValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMaxValue()
	 */
	Integer evaluateMaxValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateDefaultValue()
	 */
	Integer evaluateDefaultValue(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ProgramParameter#evaluateOptions(de.clusteval.data
	 * .DataConfig, de.clusteval.program.ProgramConfig)
	 */
	Integer[] evaluateOptions(IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramParameter#isOptionsSet()
	 */
	boolean isOptionsSet();

}