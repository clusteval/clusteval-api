/**
 * 
 */
package de.clusteval.data.randomizer;

import org.apache.commons.cli.Options;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IAbstractDataSetProvider;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.r.RLibraryInferior;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataRandomizer
		extends
			IAbstractDataSetProvider,
			RLibraryInferior,
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IDataRandomizer clone();

	/**
	 * @return A wrapper object keeping all options of your dataset generator
	 *         together with the default options of all dataset generators. The
	 *         options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #randomizeDataConfig()} .
	 */
	Options getAllOptions();

	IDataConfig randomize(String[] cliArguments) throws DataRandomizeException;

	/**
	 * This method has to be invoked with command line arguments for this
	 * generator. Valid arguments are defined by the options returned by
	 * {@link #getOptions()}.
	 * 
	 * @param cliArguments
	 * @return The generated {@link DataSet}.
	 * @throws DataRandomizeException
	 *             This exception is thrown, if the passed arguments are not
	 *             valid, or parsing of the written data set/ gold standard or
	 *             config files fails.
	 * @throws DataRandomizeException
	 */
	// TODO: remove onlySimulate attribute
	IDataConfig randomize(String[] cliArguments, boolean onlySimulate)
			throws DataRandomizeException;

	ISerializableDataRandomizer asSerializable()
			throws RepositoryObjectSerializationException;

	Options getCustomOptions();

}