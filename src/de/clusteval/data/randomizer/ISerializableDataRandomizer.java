/**
 * 
 */
package de.clusteval.data.randomizer;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataRandomizer extends ISerializableWrapperDynamicComponent<IDataRandomizer> {

}