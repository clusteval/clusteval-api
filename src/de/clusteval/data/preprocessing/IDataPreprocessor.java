/**
 * 
 */
package de.clusteval.data.preprocessing;

import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataPreprocessor extends IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IDataPreprocessor clone();

	/**
	 * @return A set with simple names of all classes, this preprocessor is
	 *         compatible to.
	 */
	Set<String> getCompatibleDataSetFormats();

	/**
	 * This method is reponsible for preprocessing the passed data and creating
	 * a new dataset object corresponding to the newly created preprocessed
	 * dataset.
	 * 
	 * @param dataSet
	 *            The dataset to be preprocessed.
	 * @return The preprocessed dataset.
	 * @throws InterruptedException
	 */
	IDataSet preprocess(IDataSet dataSet) throws InterruptedException;

	/**
	 * @return A wrapper object keeping all options of your dataset generator
	 *         together with the default options of all dataset generators. The
	 *         options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #preprocess()} .
	 */
	Options getAllOptions();

	void handleOptions(final CommandLine cmd) throws ParseException;

	@Override
	ISerializableWrapperDynamicComponent<IDataPreprocessor> asSerializable()
			throws RepositoryObjectSerializationException;
}