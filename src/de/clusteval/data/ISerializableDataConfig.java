/**
 * 
 */
package de.clusteval.data;

import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataConfig extends ISerializableWrapperRepositoryObject<IDataConfig> {

	/**
	 * @return the dataSetConfig
	 */
	ISerializableDataSetConfig getDatasetConfig();

	/**
	 * @return the standardDataSetConfig
	 */
	ISerializableDataSetConfig getStandardDataSetConfig();

	/**
	 * @return the goldstandardConfig
	 */
	ISerializableGoldStandardConfig getGoldstandardConfig();

	/**
	 * @return
	 */
	boolean hasGoldStandardConfig();

}