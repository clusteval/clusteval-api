/**
 * 
 */
package de.clusteval.data.distance;

import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;

/**
 * @author Christian Wiwie
 */
public interface IDistanceMeasure extends IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IDistanceMeasure clone();

	/**
	 * @param point1
	 *            A point with double valued coordinates.
	 * @param point2
	 *            A point with double valued coordinates.
	 * @return Distance between point1 and point2.
	 * @throws RNotAvailableException
	 * @throws InterruptedException
	 */
	double getDistance(double[] point1, double[] point2)
			throws RNotAvailableException, InterruptedException;

	/**
	 * This method indicates, whether a distance measure supports the bulk
	 * calculation of all pairwise distances of rows of a matrix with rows of a
	 * second matrix. Overwrite it in your subclass and return the appropriate
	 * boolean value. If your subclass supports matrices you also have to
	 * overwrite {@link #getDistances(double[][])} with a correct
	 * implementation.
	 * 
	 * @return True, if this distance measure supports bulk distance calculation
	 *         of matrices.
	 */
	boolean supportsMatrix();

	/**
	 * This method indicates whether the similarity s(x,y)==s(y,x).
	 * 
	 * @return True, if this distance measure is symmetric, false otherwise.
	 */
	boolean isSymmetric();

	/**
	 * This method calculates all pairwise distances between the rows of a
	 * matrix.
	 * 
	 * @param matrix
	 *            A matrix containing samples in each row and features in the
	 *            columns.
	 * @return Matrix containing all pairwise distances of rows of the matrix
	 * @throws RNotAvailableException
	 * @throws InterruptedException
	 */
	SimilarityMatrix getDistances(
			IConversionInputToStandardConfiguration config, double[][] matrix)
			throws RNotAvailableException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	ISerializableWrapperDynamicComponent<IDistanceMeasure> asSerializable()
			throws RepositoryObjectSerializationException;
}