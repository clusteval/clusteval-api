/**
 * 
 */
package de.clusteval.data;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataConfig
		extends
			IRepositoryObjectWithVersion,
			IDumpableRepositoryObject {

	/**
	 * Use this method to check, whether this IDataConfig has a goldstandard
	 * configuration or not. Some clustering quality measures do not require a
	 * goldstandard to evaluate a clustering (see
	 * {@link IClusteringQualityMeasure#requiresGoldstandard()}).
	 * 
	 * @return True, if this data configuration has a goldstandard, false
	 *         otherwise.
	 */
	boolean hasGoldStandardConfig();

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IDataConfig clone();

	/**
	 * 
	 * @return If during the execution of a run the dataset has been converted
	 *         to a different format, this method returns the converted dataset
	 *         configuration. If no conversion has been performed, this method
	 *         returns the original dataset configuration.
	 */
	IDataSetConfig getDatasetConfig();

	/**
	 * @return The goldstandard configuration of this data configuration. Is
	 *         null, if there is no goldstandard configuration.
	 */
	IGoldStandardConfig getGoldstandardConfig();

	/**
	 * The name of a data configuration is the filename of the corresponding
	 * file on the filesystem, without the file extension.
	 * 
	 * @return The name of this data configuration.
	 */
	String getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableDataConfig asSerializable()
			throws RepositoryObjectSerializationException;

	IDataSetConfig getStandardDataSetConfig();
}