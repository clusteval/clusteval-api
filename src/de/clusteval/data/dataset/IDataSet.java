/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.io.IOException;
import java.util.List;

import de.clusteval.context.IContext;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormatParser;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.IInMemoryListener;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSet extends IRepositoryObjectWithVersion {

	/**
	 * This enum describes the visibility of a dataset on the website.
	 * 
	 * @author Christian Wiwie
	 */
	public enum WEBSITE_VISIBILITY {
		/**
		 * This dataset is not visible on the website at all. It is not even
		 * listed in the list of all datasets.
		 */
		HIDE,
		/**
		 * The dataset is listed under all datasets, but not selected by
		 * default.
		 */
		SHOW_OPTIONAL,
		/**
		 * This dataset is listed and also selected to be shown by default on
		 * the website.
		 */
		SHOW_ALWAYS
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObject#setAbsolutePath(java
	 * .io.File)
	 */
	void setAbsolutePath(File absFilePath);

	IDataSet clone();

	/**
	 * Gets the dataset format.
	 * 
	 * @see DataSet#datasetFormat
	 * @return The dataset format
	 */
	IDataSetFormat getDataSetFormat();

	/**
	 * @return The type of this dataset.
	 * @see #datasetType
	 */
	IDataSetType getDataSetType();

	/**
	 * Gets the major name of this dataset. The major name corresponds to the
	 * folder the dataset resides in in the filesystem.
	 * 
	 * @return The major name
	 */
	String getMajorName();

	/**
	 * Gets the minor name of this dataset. The minor name corresponds to the
	 * name of the file of this dataset.
	 * 
	 * @return The minor name
	 */
	String getMinorName();

	/**
	 * Gets the full name of this dataset. The full name consists of the minor
	 * and the major name, separated by a slash: MAJOR/MINOR
	 * 
	 * @return The full name
	 */
	String getFullName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#copyTo(java.io.File,
	 * boolean)
	 */
	boolean copyTo(File copyDestination, boolean overwrite);

	boolean copyTo(File copyDestination, boolean overwrite,
			boolean updateAbsolutePath, boolean ensureVersionsForAllComponents);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#copyToFolder(java.io.File,
	 * boolean)
	 */
	boolean copyToFolder(File copyFolderDestination, boolean overwrite);

	/**
	 * Checks whether this dataset is loaded into the memory.
	 * 
	 * @return true, if is in memory
	 */
	boolean isInMemory();

	/**
	 * Load this dataset into memory. When this method is invoked, it parses the
	 * dataset file on the filesystem using the
	 * {@link IDataSetFormatParser#parse(DataSet)} method corresponding to the
	 * dataset format of this dataset. Then the contents of the dataset is
	 * stored in a member variable. Depending on whether this dataset is
	 * relative or absolute, this member variable varies: For absolute datasets
	 * the data is stored in {@link AbsoluteDataSet#dataMatrix}, for relative
	 * datasets in {@link RelativeDataSet#similarities}
	 * 
	 * @return true, if successful
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	boolean loadIntoMemory(IInMemoryListener listener)
			throws IllegalArgumentException, IOException,
			InvalidDataSetFormatVersionException, UnknownDataSetFormatException;

	/**
	 * Load this dataset into memory. When this method is invoked, it parses the
	 * dataset file on the filesystem using the
	 * {@link IDataSetFormatParser#parse(DataSet)} method corresponding to the
	 * dataset format of this dataset. Then the contents of the dataset is
	 * stored in a member variable. Depending on whether this dataset is
	 * relative or absolute, this member variable varies: For absolute datasets
	 * the data is stored in {@link AbsoluteDataSet#dataMatrix}, for relative
	 * datasets in {@link RelativeDataSet#similarities}
	 * 
	 * @return true, if successful
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	boolean loadIntoMemory(IInMemoryListener listener,
			NUMBER_PRECISION precision)
			throws UnknownDataSetFormatException, IllegalArgumentException,
			IOException, InvalidDataSetFormatVersionException;

	/**
	 * This method writes the contents of this dataset to the file denoted by
	 * the {@link #getAbsolutePath()}.
	 * 
	 * @param withHeader
	 *            Whether to write a header into the dataset file.
	 * 
	 * @return True, if the writing was succesfull
	 */
	boolean writeToFile(boolean withHeader);

	/**
	 * This method does not load the content of the dataset into memory, it just
	 * assumes that it has been loaded before and returns the reference.
	 * 
	 * @return The content of this dataset.
	 */
	Object getDataSetContent();

	/**
	 * This method sets the content of this dataset in memory to a new object.
	 * Contents on file system are not refreshed.
	 * 
	 * @param newContent
	 *            The new content of this dataset.
	 * @return True, if the content of this dataset has been updated to the new
	 *         object.
	 */
	boolean setDataSetContent(Object newContent);

	/**
	 * Unload the contents of this dataset from memory.
	 * 
	 * @return true, if successful
	 */
	boolean unloadFromMemory(IInMemoryListener listener);

	/**
	 * This method converts this dataset to a target format:
	 * <p>
	 * First this dataset is converted to a internal standard format (depending
	 * on the type of the IRun). Then it is converted to the target format.
	 * 
	 * @param context
	 * @param targetFormat
	 *            This is the format, the dataset is expected to be in after the
	 *            conversion process. After the dataset is converted to the
	 *            internal format, it is converted to the target format.
	 * @param configInputToStandard
	 *            This is the configuration that is used during the conversion
	 *            from the original format to the internal standard format.
	 * @param configStandardToInput
	 *            This is the configuration that is used during the conversion
	 *            from the internal standard format to the target format.
	 * @return The dataset in the target format.
	 * @throws FormatConversionException
	 * @throws IOException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws RegisterException
	 * @throws RNotAvailableException
	 * @throws InterruptedException
	 * @throws DataSetConversionException
	 */
	IDataSet preprocessAndConvertTo(IContext context,
			IDataSetFormat targetFormat,
			IConversionInputToStandardConfiguration configInputToStandard,
			ConversionStandardToInputConfiguration configStandardToInput)
			throws FormatConversionException, IOException,
			InvalidDataSetFormatVersionException, RegisterException,
			RNotAvailableException, InterruptedException,
			DataSetConversionException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/**
	 * Move this dataset to a new location.
	 * 
	 * <p>
	 * If the overwrite parameter is true and the target file exists already, it
	 * is overwritten.
	 * 
	 * @param targetFile
	 *            A file object wrapping the absolute path of the destination
	 *            this dataset should be moved to.
	 * @param overwrite
	 *            A boolean indicating, whether to overwrite a possibly existing
	 *            target file.
	 * @return True, if this dataset has been moved successfully.
	 */
	boolean moveTo(File targetFile, boolean overwrite);

	/**
	 * @return The alias of this data set.
	 * @see #alias
	 */
	String getAlias();

	/**
	 * @return This dataset in the internal standard format.
	 * @see #thisInStandardFormat
	 */
	IDataSet getInStandardFormat();

	/**
	 * @return This dataset in its original format.
	 * @see #originalDataSet
	 */
	IDataSet getOriginalDataSet();

	/**
	 * @return Checksum of this dataset
	 * @see #checksum
	 */
	long getChecksum();

	/**
	 * @return The object ids contained in the dataset.
	 */
	List<String> getIds();

	WEBSITE_VISIBILITY getWebsiteVisibility();

	IDataSet convertToStandardDirectly(final IContext context,
			final IConversionInputToStandardConfiguration configInputToStandard)
			throws DataSetConversionException;

	IDataSet convertStandardToDirectly(final IContext context,
			final IDataSetFormat targetFormat,
			final ConversionStandardToInputConfiguration configStandardToInput)
			throws DataSetConversionException, FormatConversionException;

	int getNumberOfSamples();

	@Override
	ISerializableDataSet asSerializable()
			throws RepositoryObjectSerializationException;
}