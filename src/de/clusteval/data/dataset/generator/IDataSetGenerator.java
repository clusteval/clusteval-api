/**
 * 
 */
package de.clusteval.data.dataset.generator;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.clusteval.data.dataset.IAbstractDataSetProvider;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.program.r.RLibraryInferior;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetGenerator
		extends
			IAbstractDataSetProvider,
			RLibraryInferior,
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IRepositoryObject clone();

	/**
	 * @return A wrapper object keeping all options of your dataset generator
	 *         together with the default options of all dataset generators. The
	 *         options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #generateDataSet()} .
	 */
	Options getAllOptions();

	/**
	 * If your dataset generator also creates a goldstandard for the generated
	 * dataset, this method has to return true.
	 * 
	 * <p>
	 * If a goldstandard is to be created, it is going to be stored under the
	 * same {@link #folderName} and with the same {@link #fileName} as the
	 * dataset, but within the goldstandard directory of the repository.
	 * 
	 * @return A boolean indicating, whether your dataset generator also
	 *         generates a corresponding goldstandard for the created dataset.
	 */
	boolean generatesGoldStandard();

	/**
	 * This method has to be invoked with command line arguments for this
	 * generator. Valid arguments are defined by the options returned by
	 * {@link #getOptions()}.
	 * 
	 * @param cliArguments
	 * @return The generated {@link DataSet}.
	 * @throws ParseException
	 *             This exception is thrown, if the passed arguments are not
	 *             valid.
	 * @throws DataSetGenerationException
	 * @throws GoldStandardGenerationException
	 * @throws InterruptedException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws RepositoryObjectDumpException
	 * @throws DynamicComponentInitializationException
	 */
	IDataSet generate(String[] cliArguments) throws ParseException,
			DataSetGenerationException, GoldStandardGenerationException,
			InterruptedException, RepositoryObjectDumpException,
			RegisterException, UnknownDistanceMeasureException,
			DynamicComponentInitializationException;

}