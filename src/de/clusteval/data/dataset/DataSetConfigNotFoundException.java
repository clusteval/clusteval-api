/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 * 
 */
public class DataSetConfigNotFoundException extends DataSetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3626917766278884480L;

	protected ComparableVersion version;

	public DataSetConfigNotFoundException(String objectName) {
		this(objectName, null);
	}

	/**
	 * @param message
	 */
	public DataSetConfigNotFoundException(String objectName, ComparableVersion version) {
		super(version == null
				? String.format("The dataset configuration '%s' could not be found.", objectName)
				: String.format("Version 'v%s' of dataset configuration '%s' could not be found.", version,
						objectName));
		this.version = version;
	}

	/**
	 * @return the version
	 */
	public ComparableVersion getVersion() {
		return version;
	}
}
