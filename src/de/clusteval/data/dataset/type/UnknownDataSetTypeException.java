/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset.type;

import de.clusteval.framework.repository.UnknownDynamicComponentException;

/**
 * @author Christian Wiwie
 */
public class UnknownDataSetTypeException extends UnknownDynamicComponentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -229981758008025814L;

	/**
	 * @param name
	 * @param message
	 */
	public UnknownDataSetTypeException(final String name, String message) {
		super(IDataSetType.class, name, message);
	}

	/**
	 * @param name
	 * @param cause
	 */
	public UnknownDataSetTypeException(final String name, Throwable cause) {
		super(IDataSetType.class, name, cause);
	}
}
