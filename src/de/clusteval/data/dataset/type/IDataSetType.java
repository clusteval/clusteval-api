/**
 * 
 */
package de.clusteval.data.dataset.type;

import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetType extends IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IDataSetType clone();

	/**
	 * This alias is used whenever this dataset type is visually represented and
	 * a readable name is needed.
	 * 
	 * @return The alias of this dataset type.
	 */
	String getAlias();

	ISerializableWrapperDynamicComponent<IDataSetType> asSerializable()
			throws RepositoryObjectSerializationException;
}