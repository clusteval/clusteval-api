/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.util.List;

import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataSet<D extends IDataSet> extends ISerializableWrapperRepositoryObject<D> {

	/**
	 * @return the datasetFormat
	 */
	ISerializableWrapperRepositoryObject<IDataSetFormat> getDatasetFormat();

	/**
	 * @return the datasetType
	 */
	ISerializableWrapperRepositoryObject<IDataSetType> getDatasetType();

	String getAlias();

	long getChecksum();

	WEBSITE_VISIBILITY getWebsiteVisibility();

	boolean isRelative();

	String getFullName();

	String getMajorName();

	String getMinorName();

	String getAbsolutePath();

//	List<File> getAdditionalDataSetFiles();
//
//	List<String> getAdditionalDataSetFileNames();

}