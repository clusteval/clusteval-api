/**
 * 
 */
package de.clusteval.data.dataset.format;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataSetFormat extends ISerializableWrapperDynamicComponent<IDataSetFormat> {

}