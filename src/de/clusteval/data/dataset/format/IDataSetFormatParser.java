/**
 * 
 */
package de.clusteval.data.dataset.format;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetFormatParser {

	/**
	 * @param normalize
	 *            Whether this dataset should be normalized.
	 */
	void setNormalize(boolean normalize);

}