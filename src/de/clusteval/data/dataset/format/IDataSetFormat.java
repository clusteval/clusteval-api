/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.io.File;
import java.io.IOException;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 */
public interface IDataSetFormat extends IRepositoryObjectDynamicComponent {

	/**
	 * @param dataSet
	 *            The dataset to be parsed.
	 * @param precision
	 * @return A wrapper object containing the contents of the dataset
	 * @throws IllegalArgumentException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IOException
	 */
	Object parse(IDataSet dataSet, NUMBER_PRECISION precision)
			throws IllegalArgumentException, IOException,
			InvalidDataSetFormatVersionException;

	/**
	 * @param dataSet
	 *            The dataset to be written to the filesystem.
	 * @param withHeader
	 *            Whether to write the header into the dataset file.
	 * @return True, if the dataset has been written to filesystem successfully.
	 */
	boolean writeToFile(IDataSet dataSet, boolean withHeader);

	/**
	 * Convert the given dataset with this dataset format and the given version
	 * using the passed configuration.
	 * 
	 * <p>
	 * This method validates, that the passed dataset has the correct format and
	 * that the version of the format is supported.
	 * 
	 * @param dataSet
	 *            The dataset to convert to the standard format.
	 * @param config
	 *            The configuration to use to convert the passed dataset.
	 * @return The converted dataset.
	 * @throws DataSetConversionException
	 */
	IDataSet convertToStandardFormat(IDataSet dataSet,
			IConversionInputToStandardConfiguration config)
			throws DataSetConversionException;

	/**
	 * Convert the given dataset to the given dataset format (this format) using
	 * the passed configuration.
	 * 
	 * <p>
	 * The passed dataset format object has to be of this class and is used only
	 * for its version and normalize attributes.
	 * 
	 * <p>
	 * This method validates, that the passed dataset format to convert the
	 * dataset to is correct and that the version of the format is supported.
	 * 
	 * @param dataSet
	 *            The dataset to convert to the standard format.
	 * @param dataSetFormat
	 *            The dataset format to convert the dataset to.
	 * @param config
	 *            The configuration to use to convert the passed dataset.
	 * @return The converted dataset.
	 * @throws DataSetConversionException
	 */
	IDataSet convertToThisFormat(IDataSet dataSet, IDataSetFormat dataSetFormat,
			ConversionConfiguration config) throws DataSetConversionException;

	/**
	 * @param normalized
	 *            Whether this dataset is normalized.
	 */
	void setNormalized(boolean normalized);

	/**
	 * @return Whether this dataset is normalized.
	 */
	boolean getNormalized();

	/**
	 * @return The version number of the dataset format.
	 */
	ComparableVersion getVersion();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object o);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IDataSetFormat clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * This method copies the given dataset to the given target file, assuming
	 * that the format of the dataset is this dataset format.
	 * 
	 * @param dataSet
	 *            The dataset to copy to the target file destination.
	 * @param copyDestination
	 *            The target file to which to copy the given dataset.
	 * @param overwrite
	 *            Whether to overwrite the possibly already existing target
	 *            file.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyDataSetTo(IDataSet dataSet, File copyDestination,
			boolean overwrite);

	/**
	 * This method copies the given dataset to the given target file, assuming
	 * that the format of the dataset is this dataset format.
	 * 
	 * @param dataSet
	 *            The dataset to copy to the target file destination.
	 * @param moveDestination
	 *            The target file to which to copy the given dataset.
	 * @param overwrite
	 *            Whether to overwrite the possibly already existing target
	 *            file.
	 * @return True, if the copy operation was successful.
	 */
	boolean moveDataSetTo(IDataSet dataSet, File moveDestination,
			boolean overwrite);

	/**
	 * This method copies the given dataset into the given target folder,
	 * assuming that the format of the dataset is this dataset format.
	 * 
	 * @param dataSet
	 *            The dataset to copy to the target file destination.
	 * @param copyFolderDestination
	 *            The target folder to which into copy the given dataset.
	 * @param overwrite
	 *            Whether to overwrite the possibly already existing target
	 *            file.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyDataSetToFolder(IDataSet dataSet, File copyFolderDestination,
			boolean overwrite);

	/**
	 * This alias is used whenever this dataset format is visually represented
	 * and a readable name is needed.
	 * 
	 * @return The alias of this dataset format.
	 */
	String getAlias();

	ISerializableDataSetFormat asSerializable()
			throws RepositoryObjectSerializationException;

	/**
	 * @param dataSet
	 * @return
	 */
	int getNumberAdditionalFiles(IDataSet dataSet);
}