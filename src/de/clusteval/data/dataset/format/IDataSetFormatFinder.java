/**
 * 
 */
package de.clusteval.data.dataset.format;

import de.clusteval.utils.IJARFinder;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetFormatFinder extends IJARFinder<IDataSetFormat> {

}