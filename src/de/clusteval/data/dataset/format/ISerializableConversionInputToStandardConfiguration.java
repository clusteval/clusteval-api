/**
 * 
 */
package de.clusteval.data.dataset.format;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableConversionInputToStandardConfiguration {

	/**
	 * @return the config
	 */
	IConversionInputToStandardConfiguration getWrappedComponent();

	/**
	 * @param repository
	 * @return
	 * @throws DeserializationException
	 */
	IConversionInputToStandardConfiguration deserialize(IRepository repository) throws DeserializationException;

}