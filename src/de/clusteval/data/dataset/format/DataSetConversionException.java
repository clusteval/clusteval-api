/**
 * 
 */
package de.clusteval.data.dataset.format;

import de.clusteval.data.dataset.DataSetException;

/**
 * @author Christian Wiwie
 *
 */
public class DataSetConversionException extends DataSetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4933270982029633328L;

	/**
	 * 
	 */
	public DataSetConversionException(final String message) {
		super(message);
	}

	public DataSetConversionException(final Throwable cause) {
		super(cause);
	}
}
