/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.util.List;

import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 *
 */
public interface IConversionInputToStandardConfiguration {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IConversionInputToStandardConfiguration clone();

	NUMBER_PRECISION getSimilarityPrecision();

	/**
	 * @return The distance measure to use during the conversion of absolute to
	 *         relative datasets.
	 */
	IDistanceMeasure getDistanceMeasureAbsoluteToRelative();

	/**
	 * @return The preprocessors to apply to the dataset before it is converted
	 *         to pairwise distances/similarities.
	 */
	List<IDataPreprocessor> getPreprocessorsBeforeDistance();

	/**
	 * @return The preprocessors to apply to the dataset after it is converted
	 *         to pairwise distances/similarities.
	 */
	List<IDataPreprocessor> getPreprocessorsAfterDistance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	ISerializableConversionInputToStandardConfiguration asSerializable()
			throws RepositoryObjectSerializationException;

}