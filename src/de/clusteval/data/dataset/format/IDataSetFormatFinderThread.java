/**
 * 
 */
package de.clusteval.data.dataset.format;

import de.clusteval.utils.IFinderThread;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetFormatFinderThread extends IFinderThread<IDataSetFormat> {

}