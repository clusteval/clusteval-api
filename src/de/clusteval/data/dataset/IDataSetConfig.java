/**
 * 
 */
package de.clusteval.data.dataset;

import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataSetConfig
		extends
			IRepositoryObjectWithVersion,
			IDumpableRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IDataSetConfig clone();

	/**
	 * @return The dataset, this configuration belongs to.
	 */
	IDataSet getDataSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * @return The configuration for conversion from the original input format
	 *         to the standard format.
	 * @see #configInputToStandard
	 */
	IConversionInputToStandardConfiguration getConversionInputToStandardConfiguration();

	/**
	 * @return The configuration for conversion from standard format to the
	 *         input format of the clustering method.
	 * @see #configStandardToInput
	 */
	ConversionStandardToInputConfiguration getConversionStandardToInputConfiguration();

	/**
	 * @param dataset
	 *            The new dataset
	 */
	void setDataSet(IDataSet dataset);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableDataSetConfig asSerializable()
			throws RepositoryObjectSerializationException;
}