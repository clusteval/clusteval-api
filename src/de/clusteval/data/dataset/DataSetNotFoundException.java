/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 * 
 */
public class DataSetNotFoundException extends DataSetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6957759734342865464L;

	protected String datasetName;

	protected ComparableVersion version;

	public DataSetNotFoundException(String datasetName) {
		this(datasetName, null);
	}

	/**
	 * @param message
	 */
	public DataSetNotFoundException(String datasetName, ComparableVersion version) {
		super(version == null
				? String.format("The data set '%s' could not be found.", datasetName)
				: String.format("Version 'v%s' of data set '%s' could not be found.", version, datasetName));
		this.version = version;
		this.datasetName = datasetName;
	}

	/**
	 * @return the datasetName
	 */
	public String getDatasetName() {
		return datasetName;
	}

	/**
	 * @return the version
	 */
	public ComparableVersion getVersion() {
		return version;
	}

}
