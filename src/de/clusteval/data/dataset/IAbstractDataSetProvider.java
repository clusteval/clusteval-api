/**
 * 
 */
package de.clusteval.data.dataset;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface IAbstractDataSetProvider extends IRepositoryObject {

}