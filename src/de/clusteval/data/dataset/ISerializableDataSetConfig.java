/**
 * 
 */
package de.clusteval.data.dataset;

import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.ISerializableConversionInputToStandardConfiguration;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataSetConfig extends ISerializableWrapperRepositoryObject<IDataSetConfig> {

	/**
	 * @return the configInputToStandard
	 */
	ISerializableConversionInputToStandardConfiguration getConfigInputToStandard();

	/**
	 * @return the dataSet
	 */
	ISerializableDataSet getDataSet();

	ConversionStandardToInputConfiguration getConversionStandardToInputConfiguration();

}