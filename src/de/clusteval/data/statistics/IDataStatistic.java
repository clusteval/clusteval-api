/**
 * 
 */
package de.clusteval.data.statistics;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataStatistic extends IStatistic {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IDataStatistic clone();

	/**
	 * @return True, if this data statistic requires a goldstandard to be
	 *         assessed.
	 */
	boolean requiresGoldStandard();

}