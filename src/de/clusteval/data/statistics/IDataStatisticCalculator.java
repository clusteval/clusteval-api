/**
 * 
 */
package de.clusteval.data.statistics;

import de.clusteval.utils.IStatisticCalculator;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IDataStatisticCalculator<T extends IDataStatistic> extends IStatisticCalculator<T> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IDataStatisticCalculator<T> clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.StatisticCalculator#calculate()
	 */
	T calculate() throws StatisticCalculateException;

	T getStatistic();

}