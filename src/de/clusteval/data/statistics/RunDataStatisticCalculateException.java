/**
 * 
 */
package de.clusteval.data.statistics;

/**
 * @author Christian Wiwie
 *
 */
public class RunDataStatisticCalculateException
		extends
			StatisticCalculateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6499428479357021155L;

	/**
	 * @param message
	 */
	public RunDataStatisticCalculateException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RunDataStatisticCalculateException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public RunDataStatisticCalculateException(Throwable cause) {
		super(cause);
	}

}
