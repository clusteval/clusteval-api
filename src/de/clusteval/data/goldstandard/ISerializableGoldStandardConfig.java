/**
 * 
 */
package de.clusteval.data.goldstandard;

import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableGoldStandardConfig extends ISerializableWrapperRepositoryObject<IGoldStandardConfig> {

	/**
	 * @return the goldStandard
	 */
	ISerializableWrapperRepositoryObject<IGoldStandard> getGoldstandard();

}