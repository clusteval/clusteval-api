/**
 * 
 */
package de.clusteval.data.goldstandard.format;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface IGoldStandardFormat extends IRepositoryObject {

}