/**
 * 
 */
package de.clusteval.data.goldstandard;

import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableGoldStandard extends ISerializableWrapperRepositoryObject<IGoldStandard> {

	float fuzzySize();

	String getFullName();

	String getMajorName();

	String getMinorName();

	int size();

}