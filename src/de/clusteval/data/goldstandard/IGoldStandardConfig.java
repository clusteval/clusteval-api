/**
 * 
 */
package de.clusteval.data.goldstandard;

import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IGoldStandardConfig
		extends
			IRepositoryObjectWithVersion,
			IDumpableRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IGoldStandardConfig clone();

	/**
	 * @return The goldstandard this configuration belongs to.
	 * @see #goldStandard
	 */
	IGoldStandard getGoldstandard();

	void setGoldStandard(IGoldStandard goldStandard);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	@Override
	ISerializableGoldStandardConfig asSerializable()
			throws RepositoryObjectSerializationException;
}