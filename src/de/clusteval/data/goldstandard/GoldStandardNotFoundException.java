/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.goldstandard;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 * 
 */
public class GoldStandardNotFoundException extends GoldStandardException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2215573555406810759L;

	protected String objectName;

	protected ComparableVersion version;

	public GoldStandardNotFoundException(final String objectName) {
		this(objectName, null);
	}

	/**
	 * @param message
	 */
	public GoldStandardNotFoundException(final String objectName, final ComparableVersion version) {
		super(version == null
				? String.format("Goldstandard '%s' could not be found.", objectName)
				: String.format("Version 'v%s' of goldstandard '%s' could not be found.", version, objectName));
		this.version = version;
		this.objectName = objectName;
	}

	/**
	 * @return the objectName
	 */
	public String getObjectName() {
		return objectName;
	}
}
