/**
 * 
 */
package de.clusteval.data.goldstandard;

import de.clusteval.cluster.IClustering;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IGoldStandard extends IRepositoryObjectWithVersion {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IGoldStandard clone();

	/**
	 * Checks whether this goldstandard is loaded into the memory.
	 * 
	 * @return true, if is in memory
	 */
	boolean isInMemory();

	/**
	 * Load this goldstandard into memory. When this method is invoked, it
	 * parses the goldstandard file on the filesystem
	 * 
	 * @return true, if successful
	 * @throws InvalidGoldStandardFormatException
	 */
	boolean loadIntoMemory() throws InvalidGoldStandardFormatException;

	/**
	 * Unload the contents of this dataset from memory.
	 * 
	 * @return true, if successful
	 */
	boolean unloadFromMemory();

	/**
	 * Size.
	 * 
	 * @return the int
	 */
	int size();

	/**
	 * Fuzzy size.
	 * 
	 * @return the float
	 */
	float fuzzySize();

	/**
	 * This method returns a reference to the clustering object representing the
	 * contents of the goldstandard file.
	 * 
	 * <p>
	 * If this is not already the case, the contents of the file are parsed by
	 * invoking {@link #loadIntoMemory()}.
	 * 
	 * @return The clustering object representing the goldstandard.
	 * @throws InvalidGoldStandardFormatException
	 */
	IClustering getClustering() throws InvalidGoldStandardFormatException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * Gets the full name of this goldstandard. The full name consists of the
	 * minor and the major name, separated by a slash: MAJOR/MINOR
	 * 
	 * @return The full name
	 */
	String getFullName();

	/**
	 * Gets the major name of this goldstandard. The major name corresponds to
	 * the folder the goldstandard resides in in the filesystem.
	 * 
	 * @return The major name
	 */
	String getMajorName();

	/**
	 * Gets the minor name of this goldstandard. The minor name corresponds to
	 * the name of the file of this goldstandard.
	 * 
	 * @return The minor name
	 */
	String getMinorName();

	@Override
	ISerializableGoldStandard asSerializable()
			throws RepositoryObjectSerializationException;
}