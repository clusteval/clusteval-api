/**
 * 
 */
package de.clusteval.framework.threading;

/**
 * @author Christian Wiwie
 *
 */
public interface IClustevalThread {

	/**
	 * 
	 */
	void waitFor();

	/**
	 * @return The supervisor thread that created this thread.
	 */
	ISupervisorThread getSupervisorThread();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#start()
	 */
	void start();

}