/**
 * 
 */
package de.clusteval.framework.threading;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.run.IRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.runnable.IIterationRunnable;
import de.clusteval.run.runnable.IIterationWrapper;
import de.clusteval.run.runnable.IRunRunnable;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunSchedulerThread extends IClustevalThread {

	/**
	 * @param clientId
	 * @return
	 */
	// TODO
	Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getOptimizationRunStatusForClientId(
			String clientId);

	/**
	 * This method is invoked by
	 * {@link ClustevalBackendServer#performRun(String, String)} and schedules a
	 * run. As soon as ressources are available this run is then performed in an
	 * asynchronous way in its own thread.
	 * 
	 * @param clientId
	 *            The id of the client that wants to schedule this run.
	 * @param runId
	 *            The id of the run that should be scheduled
	 * @return The unique identification string of the run; null if the run was
	 *         not scheduled..
	 * @throws ObjectNotRegisteredException
	 * @throws ObjectVersionNotRegisteredException
	 */
	String schedule(String clientId, String runId)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	/**
	 * This method is invoked by
	 * {@link ClustevalBackendServer#resumeRun(String, String)} and schedules a
	 * resume of a run. As soon as ressources are available this resume is then
	 * performed in an asynchronous way in its own thread.
	 * 
	 * @param clientId
	 *            The id of the client that wants to schedule this run.
	 * @param uniqueRunResultIdentifier
	 *            The unique identifier of the run result that should be
	 *            resumed.
	 * @return True, if successful. That means, a run result with the given id
	 *         must exist and the run has not been scheduled already.
	 */
	boolean scheduleResume(String clientId, String uniqueRunResultIdentifier);

	/**
	 * This method is invoked by
	 * {@link ClustevalBackendServer#terminateRun(String, String)} and
	 * terminates a run that is currently being executed.
	 * 
	 * @param clientId
	 *            The id of the client that wants to terminate this run.
	 * @param runId
	 *            The id of the run that should be terminated
	 * @return True, if successful. That means, a run with the given id must
	 *         exist and it is currently being executed.
	 */
	boolean terminate(String clientId, String runId);

	/**
	 * @return A collection containing names of all enqueued runs and run
	 *         resumes.
	 */
	Queue<String> getQueue(final String clientId);

	/**
	 * @return A collection containing names of all finished runs and run
	 *         resumes.
	 */
	Collection<String> getFinishedRuns(final String clientId);

	/**
	 * @return A collection containing names of all running runs and run
	 *         resumes.
	 */
	Collection<String> getRunningRuns(final String clientId);

	/**
	 * @return A collection containing names of all terminated runs and run
	 *         resumes.
	 */
	Collection<String> getTerminatedRuns(final String clientId);

	/**
	 * 
	 * @return A collection of runs, that have been executed or resumed.
	 */
	Set<IRun<?>> getRuns();

	/**
	 * 
	 * This method is invoked by
	 * {@link ClustevalBackendServer#getRunStatusForClientId(String)} and gets
	 * the status of all runs and run resumes scheduled and executed by the user
	 * with the given id.
	 * 
	 * @param clientId
	 *            The id of the client for which we want to know the status of
	 *            its scheduled and executed runs and run resumes.
	 * @return A map containing the id of the runs and run resumes together with
	 *         their current status and percentage (if currently executing).
	 */
	Map<String, Pair<RUN_STATUS, Float>> getRunStatusForClientId(
			String clientId);

	/**
	 * 
	 * This method is invoked by
	 * {@link ClustevalBackendServer#getEstimatedRemainingRuntimeForClientId(String)}
	 * and gets the estimated remaining runtime of all runs and run resumes
	 * scheduled and executed by the user with the given id.
	 * 
	 * @param clientId
	 *            The id of the client for which we want to know the status of
	 *            its scheduled and executed runs and run resumes.
	 * @return A map containing the id of the runs and run resumes together with
	 *         their estimated remaining runtime (if currently executing).
	 */
	Map<String, Long> getEstimatedRemainingRuntimeForClientId(String clientId);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	void run();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#interrupt()
	 */
	void interrupt();

	/**
	 * This method takes a {@link IRunRunnable} and adds it to the thread pool
	 * of this run scheduler thread. The thread pool then determines, when the
	 * runnable can and will be performed depending on the available ressources.
	 * 
	 * @param runRunnable
	 *            The new runnable to perform.
	 * @return A future object, that allows to retrieve the current status of
	 *         the execution of the runnable.
	 */
	Future<?> registerRunRunnable(IRunRunnable runRunnable);

	Future<?> registerIterationRunnable(IIterationRunnable iterationRunnable);

	void informOnStartedIterationRunnable(Thread t,
			IIterationRunnable runnable);

	void informOnFinishedIterationRunnable(Thread t,
			IIterationRunnable runnable);

	Map<Thread, IIterationRunnable<? extends IIterationWrapper, ?>> getActiveIterationRunnables();

	List<Runnable> getIterationRunnableQueue();

	void updateThreadPoolSize(int numberThreads);

	int getThreadPoolSize();

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	List<String> getRunExceptionMessages(String clientId, String uniqueRunId,
			boolean getWarningExceptions);

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	void clearRunExceptions(String clientId, String uniqueRunId);

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	void forgetRunStatus(String clientId, String uniqueRunId);

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	List<Throwable> getRunExceptions(String clientId, String uniqueRunId);

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	List<String> getRunWarningExceptionMessages(String clientId,
			String uniqueRunId);

	/**
	 * @param clientId
	 * @param uniqueRunId
	 * @return
	 */
	List<Throwable> getRunWarningExceptions(String clientId,
			String uniqueRunId);

	/**
	 * @return the iterationThreadPool
	 */
	Map<IRepository, ThreadPoolExecutor> getIterationThreadPool();

	boolean addBackendServerSlave(IRepository server);

	boolean isBackendServerSlaveRegistered(IRepository server);

	void clearRunWarningExceptions(String clientId, String uniqueRunId);

}