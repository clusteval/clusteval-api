/**
 * 
 */
package de.clusteval.framework.threading;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethodFinderThread;
import de.clusteval.cluster.quality.IClusteringQualityMeasureFinderThread;
import de.clusteval.data.dataset.IDataSetConfigFinderThread;
import de.clusteval.data.dataset.format.IDataSetFormatFinderThread;
import de.clusteval.data.statistics.IDataStatisticFinderThread;
import de.clusteval.run.IRunFinderThread;
import de.clusteval.run.runresult.format.IRunResultFormatFinderThread;
import de.clusteval.run.statistics.IRunStatisticFinderThread;

/**
 * @author Christian Wiwie
 *
 */
public interface ISupervisorThread {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	void run();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#interrupt()
	 */

	// TODO: fixme: not all threads (iteration/runrunnable threads?!) are
	// terminated
	void interrupt();

	/**
	 * @return The thread which finds dataset formats.
	 */
	IDataSetFormatFinderThread getDataSetFormatThread();

	/**
	 * @return The thread which finds dataset configurations.
	 */
	IDataSetConfigFinderThread getDataSetConfigThread();

	/**
	 * @return The thread which finds runresult formats.
	 */
	IRunResultFormatFinderThread getRunResultFormatThread();

	/**
	 * @return The thread which finds runs.
	 */
	IRunFinderThread getRunFinderThread();

	/**
	 * @return The thread which finds data statistics.
	 */
	IDataStatisticFinderThread getDataStatisticFinderThread();

	/**
	 * @return The thread which finds run statistics.
	 */
	IRunStatisticFinderThread getRunStatisticFinderThread();

	/**
	 * @return The thread which finds clustering quality measures.
	 */
	IClusteringQualityMeasureFinderThread getClusteringQualityMeasureFinderThread();

	/**
	 * @return The run scheduler thread.
	 */
	IRunSchedulerThread getRunScheduler();

	/**
	 * @return The thread which finds parameter optimization methods.
	 */
	IParameterOptimizationMethodFinderThread getParameterOptimizationMethodFinderThread();

	/**
	 * @param clazz
	 *            The class for which we want the thread instance
	 * @return The thread instance of the passed class.
	 */
	<T extends IClustevalThread> T getThread(Class<T> clazz);

	boolean isAlive();
}