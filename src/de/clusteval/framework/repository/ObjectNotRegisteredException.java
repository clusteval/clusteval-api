/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ObjectNotRegisteredException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5949816642140389535L;

	protected Class<? extends IRepositoryObject> clazz;
	protected String name;
	protected Throwable cause;

	public ObjectNotRegisteredException(final Class<? extends IRepositoryObject> c, final String name) {
		this(c, name, null);
	}

	/**
	 * 
	 */
	public ObjectNotRegisteredException(final Class<? extends IRepositoryObject> c, final String name,
			final Throwable cause) {
		super(cause == null
				? String.format("Object '%s' of type '%s' is not registered.", name, c.getSimpleName())
				: String.format("Could not register object '%s' of type '%s' because: '%s'", name, c.getSimpleName(),
						cause.getMessage()));
		this.clazz = c;
		this.name = name;
		this.cause = cause;
	}

	/**
	 * @return the clazz
	 */
	public Class<? extends IRepositoryObject> getClazz() {
		return clazz;
	}

	/**
	 * @return the name
	 */
	public String getObjectName() {
		return name;
	}

	/**
	 * @return the cause
	 */
	public Throwable getCause() {
		return cause;
	}
}
