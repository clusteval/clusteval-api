/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableWrapperRepositoryObject<R extends IRepositoryObject>
		extends
			ISerializableWrapper<R> {

	R deserialize(final IRepository repository) throws DeserializationException;

	void setWrappedComponent(R wrappedComponent);

	/**
	 * @return the absPath
	 */
	File getFile();

	String getName();

	String getVersion();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();
}