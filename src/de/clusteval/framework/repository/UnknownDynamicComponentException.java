/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class UnknownDynamicComponentException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3231114637466287956L;

	protected Class<? extends IRepositoryObject> clazz;
	protected String name;

	/**
	 * @param clazz
	 * @param name
	 * @param message
	 */
	public UnknownDynamicComponentException(final Class<? extends IRepositoryObject> clazz, final String name,
			String message) {
		super(message);
		this.clazz = clazz;
		this.name = name;
	}

	/**
	 * @param clazz
	 * @param name
	 * @param cause
	 */
	public UnknownDynamicComponentException(Class<? extends IRepositoryObject> clazz, final String name,
			Throwable cause) {
		super(cause);
		this.clazz = clazz;
		this.name = name;
	}

	/**
	 * @return the clazz
	 */
	public Class<? extends IRepositoryObject> getObjectClass() {
		return clazz;
	}

	/**
	 * @return the name
	 */
	public String getObjectName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.getObjectName() + ": " + super.getMessage();
	}

}