/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 * 
 */
public class RepositoryVersionTooOldException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6567290510254275170L;

	/**
	 * @param absPath
	 */
	public RepositoryVersionTooOldException(final ComparableVersion repositoryVersion,
			final ComparableVersion backendServerVersion) {
		super(String.format(
				"The repository version (%s) is too old for this version of ClustEval and needs to be migrated to version %s.",
				repositoryVersion, backendServerVersion));
	}
}
