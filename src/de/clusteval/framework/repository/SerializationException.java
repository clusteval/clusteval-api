/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class SerializationException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -455550439572917555L;
	protected String name;

	/**
	 * @param name
	 * @param cause
	 */
	public SerializationException(final String name, Throwable cause) {
		super(cause);
		this.name = name;
	}

	/**
	 * @param name
	 * @param message
	 */
	public SerializationException(final String name, final String message) {
		super(message);
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
