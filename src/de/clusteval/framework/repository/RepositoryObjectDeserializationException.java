/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 */
public class RepositoryObjectDeserializationException extends DeserializationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5057033270185881322L;

	protected Class<? extends IRepositoryObject> type;

	/**
	 * @param name
	 * @param cause
	 */
	public RepositoryObjectDeserializationException(final Class<? extends IRepositoryObject> type, final String name,
			Throwable cause) {
		super(name, cause);
		this.type = type;
		this.name = name;
	}

	/**
	 * @param type
	 * @param name
	 * @param message
	 * @param cause
	 */
	public RepositoryObjectDeserializationException(final Class<? extends IRepositoryObject> type, final String name,
			final String message) {
		super(name, message);
		this.type = type;
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public Class<? extends IRepositoryObject> getType() {
		return type;
	}
}
