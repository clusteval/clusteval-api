/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 */
public class ObjectNotFoundException extends ObjectResolutionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1329844103735542669L;

	/**
	 * 
	 */
	public ObjectNotFoundException(final String searchTerm) {
		super(searchTerm);
	}
}
