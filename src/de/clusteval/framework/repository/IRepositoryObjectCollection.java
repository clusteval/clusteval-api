/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepositoryObjectCollection {

	void setInitialized();

	boolean isInitialized();

	String getBasePath();

	boolean register(IRepositoryObject object) throws RegisterException;

	boolean unregister(IRepositoryObject object);

	/**
	 * @return the entityClass
	 */
	Class<? extends IRepositoryObject> getEntityClass();

}