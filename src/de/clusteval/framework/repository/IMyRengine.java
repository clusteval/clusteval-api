/**
 * 
 */
package de.clusteval.framework.repository;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.framework.RLibraryNotLoadedException;

/**
 * @author Christian Wiwie
 *
 */
public interface IMyRengine {

	/**
	 * This method tries to load the library with the given name.
	 * 
	 * <p>
	 * If the library could not be loaded, this method throws a
	 * {@link RLibraryNotLoadedException}.
	 * 
	 * @param name
	 *            The name of the library.
	 * @param requiredByClass
	 *            The name of the class that requires the library.
	 * @return True, if the library was loaded successfully or was loaded
	 *         before.
	 * @throws RLibraryNotLoadedException
	 * @throws InterruptedException
	 */
	boolean loadLibrary(String name, String requiredByClass) throws RLibraryNotLoadedException, InterruptedException;

	/**
	 * This method clears all variables stored in the session corresponding to
	 * this rengine and triggers a garbage collection to free unused memory.
	 * 
	 * @throws RserveException
	 * @throws InterruptedException
	 */
	void clear() throws RserveException, InterruptedException;

	/**
	 * This method allows to assign a two-dimensional double array.
	 * 
	 * @param arg0
	 *            The variable name in R.
	 * @param arg1
	 *            A two-dimensional double array which is assigned to the new
	 *            variable.
	 * @throws REngineException
	 * @throws InterruptedException
	 */
	void assign(String arg0, double[][] arg1) throws REngineException, InterruptedException;

	/**
	 * This method allows to assign a two-dimensional integer array.
	 * 
	 * @param arg0
	 *            The variable name in R.
	 * @param arg1
	 *            A two-dimensional integer array which is assigned to the new
	 *            variable.
	 * @throws REngineException
	 * @throws InterruptedException
	 */
	void assign(String arg0, int[][] arg1) throws REngineException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.rosuda.REngine.Rserve.RConnection#eval(java.lang.String)
	 */
	REXP eval(String cmd) throws RserveException, InterruptedException;

	/**
	 * TODO: use this instead of printStackTrace() This method logs the last
	 * error.
	 * 
	 * @throws InterruptedException
	 */
	void printLastError() throws InterruptedException;

	void assign(String arg0, int[] arg1) throws REngineException, InterruptedException;

	void assign(String arg0, double arg1) throws REngineException, InterruptedException;

	void assign(String arg0, double[] arg1) throws REngineException, InterruptedException;

	String getLastError() throws InterruptedException;

	boolean interrupt();

	void assign(String arg0, String[] arg1) throws REngineException, InterruptedException;

	RConnection getConnection();

	void voidEval(String cmd) throws RserveException, InterruptedException;

}