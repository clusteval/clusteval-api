/**
 * 
 */
package de.clusteval.framework.repository;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 *
 */
public class ObjectVersionNotFoundException extends ObjectResolutionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1329844103735542669L;

	protected String objectName;
	protected ComparableVersion version;

	/**
	 * 
	 */
	public ObjectVersionNotFoundException(final String searchTerm, final ComparableVersion version) {
		super(searchTerm);
		this.objectName = searchTerm;
		this.version = version;
	}

	/**
	 * @return the searchTerm
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * @return the version
	 */
	public ComparableVersion getVersion() {
		return version;
	}
}
