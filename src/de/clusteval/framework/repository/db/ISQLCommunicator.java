/**
 * 
 */
package de.clusteval.framework.repository.db;

import java.sql.SQLException;
import java.util.Map;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IAnalysisRunResult;
import de.clusteval.run.runresult.IClusteringRunResult;
import de.clusteval.run.runresult.IDataAnalysisRunResult;
import de.clusteval.run.runresult.IExecutionRunResult;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.IRunAnalysisRunResult;
import de.clusteval.run.runresult.IRunDataAnalysisRunResult;
import de.clusteval.run.runresult.IRunResult;

/**
 * @author Christian Wiwie
 *
 */
public interface ISQLCommunicator {

	Map<IRepositoryObject, Integer> getObjectIds();

	// TODO
	boolean register(IRepositoryObject object, boolean updateOnly) throws RegisterException;

	// TODO
	boolean unregister(IRepositoryObject object);

	// TODO
	boolean register(Class<? extends IRepositoryObject> c);

	// TODO
	boolean unregister(Class<? extends IRepositoryObject> c);

	/**
	 * @param run
	 *            The run which changed its status.
	 * @param runStatus
	 *            The new run status.
	 * @return True, if the status of the run was updated successfully.
	 */
	boolean updateStatusOfRun(IRun run, String runStatus);

	/**
	 * Initializes the database: 1) establishes a connection 2) tells the
	 * database to delete this repository and all corresponding entries
	 * (cascading) and recreate a new and empty repository
	 * 
	 * @throws DatabaseConnectException
	 * @throws DatabaseInitializationException 
	 */
	void initDB() throws DatabaseConnectException, DatabaseInitializationException;

	/**
	 * 
	 */
	void commitDB();

	/**
	 * @param object
	 * @return True, if the runresult was registered successfully.
	 */
	int register(IRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	boolean register(IExecutionRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	int register(IClusteringRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	int register(IParameterOptimizationResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	boolean register(IAnalysisRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	int register(IRunAnalysisRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	int register(IRunDataAnalysisRunResult object);

	/**
	 * @param object
	 * @return True, if the object was registered successfully.
	 */
	int register(IDataAnalysisRunResult object);

	void setRepositoryParsingFinished();

	/**
	 * This method is only useful with postgreSQL, since mySQL does not support
	 * materialized views.
	 * 
	 * @return
	 */
	boolean refreshMaterializedViews();

	void deleteRunResult(String runResultId) throws SQLException;

	void deleteAllRunResults() throws SQLException;

	int getParameterOptimizationMethodId(final String name) throws SQLException;

	int getDataSetFormatId(final String dataSetFormatClassSimpleName) throws SQLException;

	int getRunAnalysisId(final int runId) throws SQLException;

	int getRunExecutionId(final int runId) throws SQLException;

	int getRunId(final IRun run) throws SQLException;

	int getRunResultExecutionId(final int runResultId) throws SQLException;

	int getRunResultFormatId(final String runResultFormatSimpleName) throws SQLException;

	int getRunResultId(final String uniqueRunIdentifier) throws SQLException;

	int getRunTypeId(final String name) throws SQLException;

	int getStatisticId(final String statisticsName) throws SQLException;

	int getDataSetTypeId(final String dataSetTypeClassSimpleName) throws SQLException;

}