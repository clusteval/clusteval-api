/**
 * 
 */
package de.clusteval.framework.repository.db;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class DatabaseException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3720589999554793641L;

	/**
	 * @param message
	 */
	public DatabaseException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DatabaseException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

}
