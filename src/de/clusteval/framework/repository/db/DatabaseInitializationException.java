/**
 * 
 */
package de.clusteval.framework.repository.db;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class DatabaseInitializationException extends DatabaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8032187426884192902L;

	/**
	 * @param message
	 */
	public DatabaseInitializationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DatabaseInitializationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

}
