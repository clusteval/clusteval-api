/**
 * 
 */
package de.clusteval.framework.repository.db;

/**
 * @author Christian Wiwie
 *
 */
public interface ISQLConfig {

	/**
	 * @author Christian Wiwie
	 *
	 */
	public enum DB_TYPE {
		NONE, MYSQL, POSTGRESQL
	}

	/**
	 * @return A boolean indicating, whether the repository that this sql
	 *         configuration corresponds to uses sql.
	 */
	boolean usesSql();

	/**
	 * @return
	 */
	DB_TYPE getDatabaseType();

	/**
	 * 
	 * @return The username used to connect to the sql database.
	 */
	String getUsername();

	boolean usesPassword();

	/**
	 * @return The password used to connect to the sql database.
	 */
	String getPassword();

	/**
	 * 
	 * @return The name of the database to connect to.
	 */
	String getDatabase();

	/**
	 * 
	 * @return The host address to connect to.
	 */
	String getHost();

}