/**
 * 
 */
package de.clusteval.framework.repository.db;

/**
 * @author Christian Wiwie
 *
 */
public class DatabaseConnectException extends DatabaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9096664638562065524L;

	/**
	 * @param message
	 */
	public DatabaseConnectException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DatabaseConnectException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseConnectException(String message, Throwable cause) {
		super(message, cause);
	}

}
