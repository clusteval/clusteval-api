/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptException;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormatParser;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.IDataStatisticCalculator;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.config.IRepositoryConfig;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.db.ISQLCommunicator;
import de.clusteval.framework.repository.maven.IMavenRepositoryResolver;
import de.clusteval.framework.threading.ISupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.IRunResultFormatParser;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunDataStatisticCalculator;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.IRunStatisticCalculator;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.INamedDoubleAttribute;
import de.clusteval.utils.INamedIntegerAttribute;
import de.clusteval.utils.INamedStringAttribute;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InternalAttributeException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.MissingClustEvalVersionException;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepository {

	/**
	 * @return the resolver
	 */
	IMavenRepositoryResolver getMavenResolver();

	/**
	 * @param e
	 *            The new exception to add.
	 * @return A boolean indicating, whether the exception was new.
	 */
	boolean addMissingRLibraryException(RLibraryNotLoadedException e);

	/**
	 * This method clears the existing exceptions for missing R libraries for
	 * the given class name.
	 * 
	 * @param className
	 *            The class name for which we want to clear the missing
	 *            libraries.
	 * @return The old exceptions that were present for this class.
	 */
	Set<RLibraryNotLoadedException> clearMissingRLibraries(String className);

	/**
	 * This method is a helper method for sql communication. The sql
	 * communicator usually does not commit after every change. Therefore we
	 * provide this method, to allow for commiting at certain points such that
	 * we can afterwards guarantee a certain state of the DB and operate on it.
	 */
	void commitDB();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/**
	 * This method evaluates all internal attribute placeholders contained in
	 * the passed string.
	 * 
	 * @param old
	 *            The string which might contain internal attribute
	 *            placeholders.
	 * @param dataConfig
	 *            The data configuration which might be needed to evaluate the
	 *            placeholders.
	 * @param programConfig
	 *            The program configuration which might be needed to evaluate
	 *            the placeholders.
	 * @return The parameter value with evaluated placeholders.
	 * @throws InternalAttributeException
	 */
	String evaluateInternalAttributes(String old, IDataConfig dataConfig,
			IProgramConfig programConfig) throws InternalAttributeException;

	/**
	 * This method is used to evaluate parameter values containing javascript
	 * arithmetic operations.
	 * 
	 * <p>
	 * A helper method of
	 * {@link IProgramParameter#evaluateDefaultValue(IDataConfig, IProgramConfig)},
	 * {@link IProgramParameter#evaluateMinValue(IDataConfig, IProgramConfig)}
	 * and
	 * {@link IProgramParameter#evaluateMaxValue(IDataConfig, IProgramConfig)}.
	 * 
	 * @param script
	 *            The parameter value containing javascript arithmetic
	 *            operations.
	 * @return The evaluated expression.
	 * @throws ScriptException
	 */
	String evaluateJavaScript(String script) throws ScriptException;

	/**
	 * @throws InterruptedException
	 */
	void terminateSupervisorThread() throws InterruptedException;

	void terminateSupervisorThread(boolean closeRengines)
			throws InterruptedException;

	/**
	 * @return The absolute path to the root of this repository.
	 */
	String getBasePath();

	String getBasePath(Class<? extends IRepositoryObject> c);

	<R extends IRepositoryObject> Collection<R> getCollectionStaticEntities(
			Class<R> c);

	<R extends IRepositoryObject> Map<String, Map<ComparableVersion, R>> getStaticEntitiesWithVersions(
			Class<R> c);

	/**
	 * A wrapper for
	 * {@link #getStaticObjectWithNameAndVersion(Class, String, boolean)} that
	 * enables maven resolution for objects in case they are not registered.
	 * 
	 * @param c
	 * @param name
	 * @return
	 * @throws ObjectNotRegisteredException
	 * @throws ObjectVersionNotRegisteredException
	 */
	<R extends IRepositoryObject> R getStaticObjectWithNameAndVersion(
			Class<R> c, String name) throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	<R extends IRepositoryObject> R getStaticObjectWithNameAndVersion(
			Class<R> c, String name, boolean resolve)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean isInitialized(Class<? extends IRepositoryObject> c);

	IRepositoryObject getRegisteredObject(IRepositoryObject object);

	IRepositoryObject getRegisteredObject(IRepositoryObject object,
			boolean ignoreChangeDate);

	IRepositoryObject getRegisteredObject(Class<? extends IRepositoryObject> c,
			IRepositoryObject object, boolean ignoreChangeDate);

	<T extends IRepositoryObject, S extends T> boolean unregister(S object);

	<S extends IRepositoryObject, T extends S, O extends T> boolean unregister(
			Class<T> c, O object);

	<T extends IRepositoryObject, S extends T> boolean register(S object)
			throws RegisterException;

	<S extends IRepositoryObject, T extends S, O extends T> boolean register(
			Class<T> c, O object) throws RegisterException;

	<T extends IRepositoryObject> void setInitialized(Class<T> c);

	<T extends IRepositoryObject> boolean isClassRegistered(Class<T> c)
			throws DynamicComponentMissingVersionException;

	<T extends IRepositoryObject> boolean isClassRegistered(
			String classFullName);

	<T extends IRepositoryObject> boolean isClassRegistered(
			String classFullName, ComparableVersion version);

	<T extends IRepositoryObject> boolean isClassRegistered(Class<T> base,
			String classSimpleName);

	<T extends IRepositoryObject> boolean isClassRegistered(Class<T> base,
			String classSimpleName, ComparableVersion version);

	<S extends IRepositoryObject, T extends S, SUB extends T> boolean registerClass(
			Class<T> base, Class<SUB> c)
			throws DynamicComponentMissingVersionException,
			DynamicComponentInitializationException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	<T extends IRepositoryObject> boolean unregisterClass(Class<T> c);

	<S extends IRepositoryObject, T extends S, SUB extends T> boolean unregisterClass(
			Class<T> base, Class<SUB> c);

	Class<? extends IRepositoryObject> getRegisteredClass(String fullClassName);

	<T extends IRepositoryObject> Class<? extends T> getRegisteredClass(
			Class<T> c, String className);

	<T extends IRepositoryObject> Class<? extends T> getRegisteredClass(
			Class<T> c, String className, ComparableVersion version);

	<T extends IRepositoryObject> Class<? extends T> resolveAndGetRegisteredClass(
			Class<T> baseClass, String simpleClassName, boolean tryMaven);

	// <T extends IRepositoryObject> T resolveAndGetRegisteredObject(Class<T>
	// baseClass, String name, boolean tryMaven);

	Collection<Class<? extends IRepositoryObjectDynamicComponent>> getClasses(
			Class<? extends IRepositoryObject> c);

	Map<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion> getClassesWithVersions(
			Class<? extends IRepositoryObject> c);

	boolean isClassAvailable(String fullClassName, ComparableVersion version);

	boolean isClassAvailable(String fullClassName);

	Class<? extends IRepositoryObject> getRegisteredClassWithFullName(
			String fullClassName, int version);

	Class<? extends IRepositoryObject> getRegisteredClassWithFullName(
			String fullClassName);

	String getAnalysisResultsBasePath();

	String getClusterResultsBasePath();

	String getClusterResultsQualityBasePath();

	boolean registerDataStatisticCalculator(
			Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> dataStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	boolean registerRunDataStatisticCalculator(
			Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> runDataStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	boolean registerRunStatisticCalculator(
			Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> runStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> getDataStatisticCalculator(
			String dataStatisticClassName, ComparableVersion version);

	Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> getRunDataStatisticCalculator(
			String runDataStatisticClassName, ComparableVersion version);

	Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> getRunStatisticCalculator(
			String runStatisticClassName, ComparableVersion version);

	/**
	 * This method checks whether the given string is a valid and internal
	 * double attribute by invoking {@link #isInternalAttribute(String)}. Then
	 * the internal double attribute is looked up and returned if it exists.
	 * 
	 * @param value
	 *            The name of the internal double attribute.
	 * @return The internal double attribute with the given name or null, if
	 *         there is no attribute with the given name
	 */
	INamedDoubleAttribute getInternalDoubleAttribute(String value);

	/**
	 * This method checks whether the given string is a valid and internal
	 * integer attribute by invoking {@link #isInternalAttribute(String)}. Then
	 * the internal integer attribute is looked up and returned if it exists.
	 * 
	 * @param value
	 *            The name of the internal integer attribute.
	 * @return The internal integer attribute with the given name or null, if
	 *         there is no attribute with the given name
	 */
	INamedIntegerAttribute getInternalIntegerAttribute(String value);

	/**
	 * This method checks whether the given string is a valid and internal
	 * string attribute by invoking {@link #isInternalAttribute(String)}. Then
	 * the internal string attribute is looked up and returned if it exists.
	 * 
	 * @param value
	 *            The name of the internal string attribute.
	 * @return The internal string attribute with the given name or null, if
	 *         there is no attribute with the given name
	 */
	INamedStringAttribute getInternalStringAttribute(String value);

	/**
	 * @return The absolute path to the directory, where for a certain runresult
	 *         (identified by its unique run identifier) all log files are
	 *         stored.
	 */
	String getLogBasePath();

	/**
	 * 
	 * @return The parent repository of this repository, or null if this
	 *         repository has no parent.
	 */
	IRepository getParent();

	/**
	 * This method looks up and returns (if it exists) the repository object
	 * that belongs to the passed absolute path.
	 * 
	 * @param absFilePath
	 *            The absolute path for which we want to find the repository
	 *            object.
	 * @return The repository object which has the given absolute path.
	 */
	IRepositoryObject getRegisteredObject(File absFilePath);

	/**
	 * This method checks, whether there is a named double attribute registered,
	 * that is equal to the passed object and returns it.
	 * 
	 * <p>
	 * Equality is checked in terms of
	 * <ul>
	 * <li><b>object.hashCode == other.hashCode</b></li>
	 * <li><b>object.equals(other)</b></li>
	 * </ul>
	 * since internally the repository uses hash datastructures.
	 * 
	 * <p>
	 * By default the {@link IRepositoryObject#equals(Object)} method is only
	 * based on the absolute path of the repository object and the repositories
	 * of the two objects, this means two repository objects are considered the
	 * same if they are stored in the same repository and they have the same
	 * absolute path.
	 * 
	 * @param object
	 *            The object for which we want to find an equal registered
	 *            object.
	 * @return The registered object equal to the passed object.
	 */
	INamedDoubleAttribute getRegisteredObject(INamedDoubleAttribute object);

	/**
	 * This method checks, whether there is a named integer attribute
	 * registered, that is equal to the passed object and returns it.
	 * 
	 * <p>
	 * Equality is checked in terms of
	 * <ul>
	 * <li><b>object.hashCode == other.hashCode</b></li>
	 * <li><b>object.equals(other)</b></li>
	 * </ul>
	 * since internally the repository uses hash datastructures.
	 * 
	 * <p>
	 * By default the {@link IRepositoryObject#equals(Object)} method is only
	 * based on the absolute path of the repository object and the repositories
	 * of the two objects, this means two repository objects are considered the
	 * same if they are stored in the same repository and they have the same
	 * absolute path.
	 * 
	 * @param object
	 *            The object for which we want to find an equal registered
	 *            object.
	 * @return The registered object equal to the passed object.
	 */
	INamedIntegerAttribute getRegisteredObject(INamedIntegerAttribute object);

	/**
	 * This method checks, whether there is a named string attribute registered,
	 * that is equal to the passed object and returns it.
	 * 
	 * <p>
	 * Equality is checked in terms of
	 * <ul>
	 * <li><b>object.hashCode == other.hashCode</b></li>
	 * <li><b>object.equals(other)</b></li>
	 * </ul>
	 * since internally the repository uses hash datastructures.
	 * 
	 * <p>
	 * By default the {@link IRepositoryObject#equals(Object)} method is only
	 * based on the absolute path of the repository object and the repositories
	 * of the two objects, this means two repository objects are considered the
	 * same if they are stored in the same repository and they have the same
	 * absolute path.
	 * 
	 * @param object
	 *            The object for which we want to find an equal registered
	 *            object.
	 * @return The registered object equal to the passed object.
	 */
	INamedStringAttribute getRegisteredObject(INamedStringAttribute object);

	/**
	 * This method looks up and returns (if it exists) the runresult with the
	 * given unique identifier.
	 * 
	 * @param runIdentifier
	 *            The identifier of the runresult.
	 * @return The runresult with the given identifier.
	 */
	IRunResult getRegisteredRunResult(String runIdentifier);

	/**
	 * @return The configuration of this repository.
	 */
	IRepositoryConfig getRepositoryConfig();

	/**
	 * @return A collection with the names of the parameter optimization run
	 *         results.
	 */
	Collection<String> getParameterOptimizationRunResultIdentifiers();

	/**
	 * @return A collection with the names of the clustering run results.
	 */
	Collection<String> getClusteringRunResultIdentifiers();

	/**
	 * @return A collection with the names of the data analysis run results.
	 */
	Collection<String> getDataAnalysisRunResultIdentifiers();

	/**
	 * @return A collection with the names of the run analysis run results.
	 */
	Collection<String> getRunAnalysisRunResultIdentifiers();

	/**
	 * @return A collection with the names of the run-data analysis run results.
	 */
	Collection<String> getRunDataAnalysisRunResultIdentifiers();

	/**
	 * @return A collection with the names of all run result directories
	 *         contained in the repository of this server. Those run result
	 *         directories can be resumed, if they were terminated before.
	 */
	Collection<String> getRunResumes();

	/**
	 * @return In case the backend is connected to a mysql database in the
	 *         frontend, this returns an sql communicator, which updates the
	 *         database after changes of repository objects (removal, addition),
	 *         otherwise it returns a stub sql communicator.
	 */
	ISQLCommunicator getSqlCommunicator();

	/**
	 * 
	 * @return The supervisor thread is responsible for starting and keeping
	 *         alive all threads that check the repository on the filesystem for
	 *         changes.
	 */
	ISupervisorThread getSupervisorThread();

	/**
	 * @return The absolute path to the directory within this repository, where
	 *         all supplementary materials are stored.
	 */
	String getSupplementaryBasePath();

	/**
	 * @return The absolute path to the directory within this repository, where
	 *         all supplementary materials related to clustering are stored.
	 */
	String getSupplementaryClusteringBasePath();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/**
	 * Initializes this repository by creating a supervisor thread
	 * {@link #createSupervisorThread()} and waiting until
	 * {@link #isInitialized()} returns true.
	 * 
	 * @throws InterruptedException
	 *             Is thrown, if the current thread is interrupted while waiting
	 *             for finishing the initialization process.
	 * @throws DatabaseException
	 */
	void initialize() throws InterruptedException, DatabaseException;

	/**
	 * This method checks, whether this repository has been initialized. A
	 * repository is initialized, if the following invocations return true:
	 * 
	 * <ul>
	 * <li><b>getDataSetFormatsInitialized()</b></li>
	 * <li><b>getDataSetTypesInitialized()</b></li>
	 * <li><b>getDataStatisticsInitialized()</b></li>
	 * <li><b>getRunStatisticsInitialized()</b></li>
	 * <li><b>getRunDataStatisticsInitialized()</b></li>
	 * <li><b>getRunResultFormatsInitialized()</b></li>
	 * <li><b>getClusteringQualityMeasuresInitialized()</b></li>
	 * <li><b>getParameterOptimizationMethodsInitialized()</b></li>
	 * <li><b>getRunsInitialized()</b></li>
	 * <li><b>getRProgramsInitialized()</b></li>
	 * <li><b>getDataSetConfigsInitialized()</b></li>
	 * <li><b>getGoldStandardConfigsInitialized()</b></li>
	 * <li><b>getDataConfigsInitialized()</b></li>
	 * <li><b>getProgramConfigsInitialized()</b></li>
	 * <li><b>getDataSetGeneratorsInitialized()</b></li>
	 * <li><b>getDistanceMeasuresInitialized()</b></li>
	 * </ul>
	 * 
	 * @return True, if this repository is initialized.
	 */
	boolean isInitialized();

	/**
	 * This method checks whether a parser has been registered for the given
	 * runresult format class.
	 * 
	 * @param runResultFormat
	 *            The class for which we want to know whether a parser has been
	 *            registered.
	 * @return True, if the parser has been registered.
	 */
	boolean isRegisteredForRunResultFormat(
			Class<? extends IRunResultFormat> runResultFormat);

	/**
	 * This method registers a new named double attribute. If an old object was
	 * already registered that equals the new object, the new object is not
	 * registered.
	 * 
	 * @param object
	 *            The new object to register.
	 * @return True, if the new object has been registered.
	 */
	boolean register(INamedDoubleAttribute object);

	/**
	 * This method registers a new named integer attribute. If an old object was
	 * already registered that equals the new object, the new object is not
	 * registered.
	 * 
	 * @param object
	 *            The new object to register.
	 * @return True, if the new object has been registered.
	 */
	boolean register(INamedIntegerAttribute object);

	/**
	 * This method registers a new named string attribute. If an old object was
	 * already registered that equals the new object, the new object is not
	 * registered.
	 * 
	 * @param object
	 *            The new object to register.
	 * @return True, if the new object has been registered.
	 */
	boolean register(INamedStringAttribute object);

	/**
	 * @return The IMyRengine object corresponding to the current thread.
	 * @throws RserveException
	 */
	IMyRengine getRengineForCurrentThread() throws RserveException;

	IMyRengine getRengine(Thread thread) throws RserveException;

	void clearRengineForCurrentThread();

	void clearRengine(Thread thread);

	/**
	 * This method registers a dataset format parser.
	 * 
	 * @param dsFormatParser
	 *            The dataset format parser to register.
	 * @return True, if the dataset format parser replaced an old object.
	 * @throws DynamicComponentMissingVersionException
	 * @throws IncompatibleClustEvalVersionException
	 * @throws MissingClustEvalVersionException
	 * @throws InvalidClustEvalVersionException
	 * @throws InvalidDependencyTargetException
	 */
	boolean registerDataSetFormatParser(
			Class<? extends IDataSetFormatParser> dsFormatParser)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	/**
	 * This method checks whether a parser has been registered for the given
	 * dataset format class.
	 * 
	 * @param dsFormat
	 *            The class for which we want to know whether a parser has been
	 *            registered.
	 * @return True, if the parser has been registered.
	 */
	boolean isRegisteredForDataSetFormat(
			Class<? extends IDataSetFormat> dsFormat);

	/**
	 * This method registers a new runresult format parser class.
	 * 
	 * @param runResultFormatParser
	 *            The new class to register.
	 * @return True, if the new class replaced an old one.
	 * @throws IncompatibleClustEvalVersionException
	 * @throws DynamicComponentMissingVersionException
	 * @throws MissingClustEvalVersionException
	 * @throws InvalidClustEvalVersionException
	 * @throws InvalidDependencyTargetException
	 */
	boolean registerRunResultFormatParser(
			Class<? extends IRunResultFormatParser> runResultFormatParser)
			throws IncompatibleClustEvalVersionException,
			DynamicComponentMissingVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException;

	/**
	 * @param comm
	 *            The new sql communicator.
	 */
	void setSQLCommunicator(ISQLCommunicator comm);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * This method unregisters the passed object.
	 * 
	 * @param object
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	boolean unregister(INamedDoubleAttribute object);

	/**
	 * This method unregisters the passed object.
	 * 
	 * @param object
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	boolean unregister(INamedIntegerAttribute object);

	/**
	 * This method unregisters the passed object.
	 * 
	 * @param object
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	boolean unregister(INamedStringAttribute object);

	/**
	 * This method unregisters the passed object.
	 * 
	 * @param object
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	boolean unregisterRunResultFormatParser(
			Class<? extends IRunResultFormatParser> object);

	/**
	 * This method is invoked by
	 * {@link IRun#setStatus(de.clusteval.run.RUN_STATUS)} and ensures that the
	 * new status is passed to the whole framework, e.g. the frontend database.
	 * 
	 * @param run
	 *            The run which changed its status.
	 * @param newStatus
	 *            The new status of the run.
	 * @return True, if the propagation of the new status was successful.
	 */
	boolean updateStatusOfRun(IRun run, String newStatus);

	/**
	 * @return The map containing all known finder exceptions.
	 */
	Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> getKnownFinderExceptions();

	/**
	 * @return The class loaders used by the finders to load classes
	 *         dynamically.
	 */
	Map<URL, URLClassLoader> getJARFinderClassLoaders();

	/**
	 * 
	 * @return A map containing dependencies between jar files that are loaded
	 *         dynamically.
	 */
	Map<String, List<File>> getJARFinderWaitingFiles();

	/**
	 * 
	 * @return The change dates of the jar files that were loaded dynamically by
	 *         jar finder instances.
	 */
	Map<String, Long> getFinderLoadedJarFileChangeDates();

	/**
	 * @return the fullClassNamesForPresentFiles
	 */
	Map<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>> getFullClassNamesAndVersionsForPresentFiles();

	void warn(final String message);

	void info(final String message);

	Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getLoadedClasses();

	Map<File, IRepositoryObject> getPathToRepositoryObject();

	boolean installRepositoryObjectFromJarFromMaven(
			final Class<? extends IRepositoryObject> type,
			final String objectName, final File jarFile,
			final ComparableVersion version);

	String getMavenGroupIDForRepositoryObject(
			final Class<? extends IRepositoryObject> c,
			final String objectName);

	String getMavenArtifactIDForRepositoryObject(
			final Class<? extends IRepositoryObject> c,
			final String objectName);

	public Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject>> getErrorObjects()
			throws ObjectNotFoundException, ObjectVersionNotFoundException;

	Collection<Throwable> getErrors(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final String version);

	boolean hasError(final Class<? extends IRepositoryObject> clazz,
			final String name, final String version);

	Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getClassesWithNameAndVersion(
			Class<? extends IRepositoryObject> c);

	String getRelativeFilePathToRepository(final File file);

	Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getAllRegisteredClasses();

	<T extends IRepositoryObject> Pair<String, ComparableVersion> getFullClassNameAndVersion(
			final Class<T> baseClass, final String simpleClassName,
			final boolean resolveToNewestIfVersionMissing);

	/**
	 * @param programConfig
	 *            The program configuration for which to calculate the expected
	 *            runtime when applied.
	 * @param numberSamplesInDataset
	 *            The size of the data set.
	 * @return The expected number of required miliseconds to cluster a data set
	 *         that contains the specified number of samples with the specified
	 *         program configuration and 1 CPU core. If not enough observations
	 *         are available to make predictions, -1l is returned.
	 */
	long getExpectedRuntimePerClustering(final IProgramConfig programConfig,
			final int numberSamplesInDataset);

	void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			IOException, URISyntaxException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, InterruptedException;

}