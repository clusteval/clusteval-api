/**
 * 
 */
package de.clusteval.framework.repository;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * @author Christian Wiwie
 *
 */
public interface IHasVersion {

	/**
	 * @return
	 */
	public ComparableVersion getVersion();
}
