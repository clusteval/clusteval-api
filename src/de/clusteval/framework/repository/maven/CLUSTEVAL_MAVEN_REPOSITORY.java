/**
 * 
 */
package de.clusteval.framework.repository.maven;

public enum CLUSTEVAL_MAVEN_REPOSITORY {
	/**
	 * The official ClustEval release maven repository.
	 */
	CLUSTEVAL_RELEASE("https://maven.compbio.sdu.dk/repository/internal/"),
	/**
	 * The official ClustEval development maven repository.
	 */
	CLUSTEVAL_SNAPSHOT("https://maven.compbio.sdu.dk/repository/snapshots/");

	private final String url;

	private CLUSTEVAL_MAVEN_REPOSITORY(final String url) {
		this.url = url;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
}