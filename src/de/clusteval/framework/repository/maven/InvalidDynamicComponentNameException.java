/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository.maven;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 * 
 */
public class InvalidDynamicComponentNameException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8915639211302644905L;

	/**
	 * @param simpleClassName
	 *            The simple class name of the component that we looked for.
	 */
	public InvalidDynamicComponentNameException(final String simpleClassName) {
		super(String.format("The component name is of an unknown type and cannot be resolved: \"%s\" ",
				simpleClassName));
	}
}
