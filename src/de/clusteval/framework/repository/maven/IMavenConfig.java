/**
 * 
 */
package de.clusteval.framework.repository.maven;

import java.util.Map;

import org.eclipse.aether.repository.ArtifactRepository;

/**
 * @author Christian Wiwie
 *
 */
public interface IMavenConfig {

	/**
	 * 
	 * @return Whether to use maven resolution.
	 */
	boolean useMaven();

	/**
	 * 
	 * @param useMaven
	 *            Whether to use maven resolution.
	 */
	void setUseMaven(final boolean useMaven);

	/**
	 * @param name
	 *            The name of the new maven repository
	 * @param url
	 *            The URL of the new maven repository
	 * @throws MavenRepositoryDuplicateException
	 */
	void addMavenRepository(String name, String url)
			throws MavenRepositoryDuplicateException;

	/**
	 * @return the mavenRepositories
	 */
	Map<String, ArtifactRepository> getMavenRepositories();

	void setResolveSnapshotVersions(boolean resolveSnapshotVersions);

	boolean resolveSnapshotVersions();

}