/**
 * 
 */
package de.clusteval.framework.repository.maven;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.VersionRangeRequest;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public interface IMavenRepositoryResolver {

	boolean isDynamicComponentAvailable(String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException;

	/**
	 * 
	 * @param simpleClassName
	 *            The simple class name of the component that we want to load.
	 * @param version
	 *            A version of the component. Can be null if the newest version
	 *            should be retrieved.
	 * @return True, if the component can be resolved.
	 * @throws InvalidDynamicComponentNameException
	 */
	boolean isDynamicComponentAvailable(String simpleClassName,
			ComparableVersion version)
			throws InvalidDynamicComponentNameException;

	/**
	 * 
	 * @param simpleClassName
	 *            The simple class name of the component that we want to load.
	 * @return A set of all available versions of this component.
	 * @throws InvalidDynamicComponentNameException
	 */
	Set<ComparableVersion> getAvailableVersionsForDynamicComponent(
			String simpleClassName) throws InvalidDynamicComponentNameException;

	boolean installDynamicComponentIntoRepository(
			String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException;

	/**
	 * 
	 * @param simpleClassName
	 *            The simple class name of the component that we want to load.
	 * @param version
	 *            A version of the component. Can be null.
	 * @return True, if the component was succsesfully installed.
	 * @throws InvalidDynamicComponentNameException
	 */
	boolean installDynamicComponentIntoRepository(String simpleClassName,
			ComparableVersion version)
			throws InvalidDynamicComponentNameException;

	void informOnClassRemoved(String simpleClassName,
			ComparableVersion version);

	boolean isRepositoryObjectAvailable(
			final Class<? extends IRepositoryObject> type,
			String objectNameWithVersion);

	boolean isRepositoryObjectAvailable(
			final Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version);

	Set<ComparableVersion> getAvailableVersionsForRepositoryObject(
			final Class<? extends IRepositoryObject> type, String objectName)
			throws InvalidDynamicComponentNameException;

	boolean installRepositoryObjectIntoRepository(
			final Class<? extends IRepositoryObject> type,
			String objectNameWithVersion);

	boolean installRepositoryObjectIntoRepository(
			final Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version);

	void informOnRepositoryObjectRemoved(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final ComparableVersion version);

	ComparableVersion checkForNewerVersionClustEvalClient(
			final ComparableVersion clientVersion,
			final boolean includeSnapshotVersions);

	ComparableVersion checkForNewerVersionClustEvalServer(
			final boolean includeSnapshotVersions);

	Set<ComparableVersion> getAvailableVersions(
			final VersionRangeRequest rangeRequest,
			final boolean includeSnapshotVersions);

	ComparableVersion checkForNewerVersionDynamicComponent(
			final String simpleClassName, final ComparableVersion version)
			throws InvalidDynamicComponentNameException;

	Map<String, Map<String, String>> checkForNewerVersionsComponents(
			final String clientId,
			final Map<String, Set<ComparableVersion>> registeredClassesNamesAndVersions,
			boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException;

	String getReleaseNotes(final String groupName, final String artifactName,
			final String artifactVersion) throws ArtifactResolutionException;

	String getReleaseTitle(final String groupName, final String artifactName,
			final String artifactVersion) throws ArtifactResolutionException;
}