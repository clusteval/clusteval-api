/**
 * 
 */
package de.clusteval.framework.repository.parse;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public class RepositoryObjectParseException extends ParseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3741978757095038100L;

	protected Class<? extends IRepositoryObject> clazz;
	protected String objectName, objectVersion;
	protected Throwable cause;

	public RepositoryObjectParseException(final Class<? extends IRepositoryObject> c, final String objectName,
			final String version, final Throwable cause) {
		super(String.format("The object '%s' of type '%s' could not be parsed: '%s'", objectName, c.getSimpleName(),
				cause.getMessage()));
		this.clazz = c;
		this.objectName = objectName;
		this.objectVersion = version;
		this.cause = cause;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return objectName;
	}

	/**
	 * @return the objectVersion
	 */
	public String getVersion() {
		return objectVersion;
	}

	/**
	 * @return the clazz
	 */
	public Class<? extends IRepositoryObject> getClazz() {
		return clazz;
	}

	/**
	 * @return the cause
	 */
	public Throwable getCause() {
		return cause;
	}
}
