/**
 * 
 */
package de.clusteval.framework.repository.parse;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ParseException extends ClustEvalException {

	public ParseException(final String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ParseException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6229131297440349595L;

}
