/**
 * 
 */
package de.clusteval.framework.repository;

import org.rosuda.REngine.REngineException;

/**
 * @author Christian Wiwie
 *
 */
public class RException extends REngineException {

	/**
	 * 
	 */
	public RException(final IMyRengine rEngine, final String message) {
		super(rEngine.getConnection(), message);
	}
}
