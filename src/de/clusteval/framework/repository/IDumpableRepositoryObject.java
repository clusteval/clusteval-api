/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.IDumpable;

/**
 * @author Christian Wiwie
 *
 */
public interface IDumpableRepositoryObject extends IDumpable {

	/**
	 * @throws RepositoryObjectDumpException
	 */
	@Override
	void dumpToFile() throws RepositoryObjectDumpException;
}