/**
 * 
 */
package de.clusteval.framework.repository.config;

import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.db.ISQLConfig;
import de.clusteval.framework.repository.maven.IMavenConfig;
import de.clusteval.utils.IDumpable;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepositoryConfig extends IDumpable {

	/**
	 * @return The mysql configuration of this repository.
	 */
	ISQLConfig getSQLConfig();

	/**
	 * Override the mysql configuration of this repository.
	 * 
	 * @param mysqlConfig
	 *            The new mysql configuration.
	 */
	void setMysqlConfig(ISQLConfig mysqlConfig);

	/**
	 * @return the mavenConfig
	 */
	IMavenConfig getMavenConfig();

	/**
	 * @param mavenConfig
	 *            the mavenConfig to set
	 */
	void setMavenConfig(IMavenConfig mavenConfig);

	/**
	 * @return The thread sleep times for the repository.
	 * @see #threadingSleepingTimes
	 */
	Map<String, Long> getThreadSleepTimes();

	void setVersion(ComparableVersion version);

	ComparableVersion getVersion();

}