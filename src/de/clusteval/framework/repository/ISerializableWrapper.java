/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableWrapper<R extends Object>
		extends
			Comparable<ISerializableWrapper<R>> {

	R deserialize(IRepository repository) throws DeserializationException;

	void setWrappedComponent(R wrappedComponent);

	File getFile();

	String getName();

	String getVersion();

	boolean equals(Object obj);

	int hashCode();

	String toString();

	int compareTo(ISerializableWrapper<R> o);

	/**
	 * @param repository
	 * @param force
	 * @return
	 * @throws DeserializationException
	 */
	R deserialize(IRepository repository, boolean force)
			throws DeserializationException;

}