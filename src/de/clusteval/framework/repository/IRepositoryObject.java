/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.Serializable;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.Logger;

import de.clusteval.utils.IObjectToBeSerialized;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepositoryObject
		extends
			IRepositoryListener,
			Serializable,
			IObjectToBeSerialized {

	/**
	 * @return A cloned deep copy of this object.
	 */
	IRepositoryObject clone();

	/**
	 * @return The repository this object is registered in.
	 */
	IRepository getRepository();

	/**
	 * @return The absolute path of this repository object.
	 */
	String getAbsolutePath();

	/**
	 * @return A file object pointing to this repository object
	 */
	File getFile();

	/**
	 * @param absFilePath
	 *            The new absolute file path.
	 */
	void setAbsolutePath(File absFilePath);

	/**
	 * Any subclass needs to implement this method. It will be responsible to
	 * register a new object of the subclass at the repository.
	 * 
	 * @return true, if successful
	 * @throws RegisterException
	 *             An exception is thrown if something goes wrong during the
	 *             registering process, that might be interesting to handle
	 *             individually.
	 */
	boolean register() throws RegisterException;

	/**
	 * Any subclass needs to implement this method. It will be responsible to
	 * unregister an object of the subclass from the repository.
	 * 
	 * @return true, if successful
	 */
	boolean unregister();

	/**
	 * @return The changedate of this repository object.
	 */
	long getChangeDate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	int hashCode();

	/**
	 * A convenience method for {@link #copyTo(File, boolean)}, with overwriting
	 * enabled.
	 * 
	 * @param copyDestination
	 *            The absolute path to the destination file.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyTo(File copyDestination);

	/**
	 * A convenience method for
	 * {@link #copyTo(File, boolean, boolean, boolean)}, with waiting for the
	 * operation to finish.
	 * 
	 * @param copyDestination
	 *            The absolute path to the destination file.
	 * @param overwrite
	 *            Whether the possibly already existing target file should be
	 *            overwritten.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyTo(File copyDestination, boolean overwrite);

	/**
	 * This method copies the file corresponding to this repository object to
	 * the destination.
	 * 
	 * <p>
	 * <b>Hint:</b> Use the wait parameter with caution: It might increase the
	 * ressource load of this method considerably. Also the wait operation might
	 * not terminate, if source and target filesystem use different encodings
	 * and the equality checks return false.
	 * 
	 * @param copyDestination
	 *            The absolute path to the destination file.
	 * @param overwrite
	 *            Whether the possibly already existing target file should be
	 *            overwritten.
	 * @param wait
	 *            Whether to wait for this operation to finish, were the
	 *            completion of the operation is determined by steadily
	 *            comparing source and target file for equality.
	 * @param ensureVersionsForAllComponents
	 *            Whether we should ensure that the copied version of this
	 *            object should contain versions for all dynamic components.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyTo(File copyDestination, boolean overwrite, boolean wait,
			boolean ensureVersionsForAllComponents);

	/**
	 * A convenience method for {@link #moveTo(File, boolean)}, with overwriting
	 * enabled.
	 * 
	 * @param moveDestination
	 *            The absolute path to the destination file.
	 * @return True, if the move operation was successful.
	 */
	boolean moveTo(File moveDestination);

	/**
	 * This method moves the file corresponding to this repository object to the
	 * destination.
	 * 
	 * <p>
	 * <b>Hint:</b> Use the wait parameter with caution: It might increase the
	 * ressource load of this method considerably. Also the wait operation might
	 * not terminate, if source and target filesystem use different encodings
	 * and the equality checks return false.
	 * 
	 * @param moveDest
	 *            The absolute path to the destination file.
	 * @param overwrite
	 *            Whether the possibly already existing target file should be
	 *            overwritten.
	 * @return True, if the move operation was successful.
	 */
	boolean moveTo(File moveDest, boolean overwrite);

	/**
	 * A convenience method for {@link #moveToFolder(File, boolean)}, with
	 * overwriting enabled.
	 * 
	 * @param moveFolderDestination
	 *            The folder into which this file should be move
	 * @return True, if the move operation was successful.
	 */
	boolean moveToFolder(File moveFolderDestination);

	/**
	 * This method moves the file corresponding to this repository object into
	 * the destination folder.
	 * 
	 * @param moveFolderDestination
	 *            The folder in which this file should be copied
	 * @param overwrite
	 *            Whether a possibly already existing target file within the
	 *            destination folder should be overwritten.
	 * @return True, if the copy operation was successful.
	 */
	boolean moveToFolder(File moveFolderDestination, boolean overwrite);

	/**
	 * A convenience method for {@link #copyToFolder(File, boolean, boolean)},
	 * with overwriting enabled.
	 * 
	 * @param copyFolderDestination
	 *            The folder in which this file should be copied
	 * @return True, if the copy operation was successful.
	 */
	boolean copyToFolder(File copyFolderDestination);

	/**
	 * A convenience method for {@link #copyToFolder(File, boolean, boolean)},
	 * with overwriting enabled.
	 * 
	 * @param copyFolderDestination
	 *            The folder in which this file should be copied
	 * @param overwrite
	 *            Whether a possibly already existing target file within the
	 *            destination folder should be overwritten.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyToFolder(File copyFolderDestination, boolean overwrite);

	/**
	 * This method copies the file corresponding to this repository object into
	 * the destination folder.
	 * 
	 * @param copyFolderDestination
	 *            The folder in which this file should be copied
	 * @param overwrite
	 *            Whether a possibly already existing target file within the
	 *            destination folder should be overwritten.
	 * @param ensureVersionsForAllComponents
	 *            Whether we should ensure that the copied version of this
	 *            object should contain versions for all dynamic components.
	 * @return True, if the copy operation was successful.
	 */
	boolean copyToFolder(File copyFolderDestination, boolean overwrite,
			boolean ensureVersionsForAllComponents);

	/**
	 * Add a listener to this objects listeners. Those are for example informed
	 * when this object is removed from the repository or replaced by another
	 * object.
	 * 
	 * @param listener
	 *            The new listener.
	 * @return True, if the listener was added successfully
	 */
	boolean addListener(IRepositoryListener listener);

	/**
	 * Remove a listener from this objects listener.
	 * 
	 * @param listener
	 *            The listener to remove.
	 * @return True, if the listener was removed successfully
	 */
	boolean removeListener(IRepositoryListener listener);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryListener#notify(utils.RepositoryEvent)
	 */
	@Override
	void notify(RepositoryEvent e) throws RegisterException;

	/**
	 * @return The logger of this object.
	 */
	Logger getLog();

	/**
	 * @return The name of this object.
	 */
	String getName();

	/**
	 * @return The version of this object
	 */
	ComparableVersion getVersion();

	/**
	 * @param versionSeparator
	 *            The separator between name and version.
	 * @return A string representation of this object.
	 */
	String toString(final String versionSeparator);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IObjectToBeSerialized#asSerializable()
	 */
	@Override
	ISerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializable()
			throws RepositoryObjectSerializationException;
}