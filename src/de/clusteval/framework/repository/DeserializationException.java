/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class DeserializationException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2344956785554521345L;

	protected String name;

	/**
	 * @param name
	 * @param cause
	 */
	public DeserializationException(final String name, Throwable cause) {
		super(cause);
		this.name = name;
	}

	/**
	 * @param name
	 * @param message
	 */
	public DeserializationException(final String name, final String message) {
		super(message);
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
