/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepositoryObjectDynamicComponent extends IRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableWrapperDynamicComponent<? extends IRepositoryObjectDynamicComponent> asSerializable()
			throws RepositoryObjectSerializationException;

	String getAlias();
}