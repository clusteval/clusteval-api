/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ObjectResolutionException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4314804274236202604L;

	/**
	 * 
	 */
	public ObjectResolutionException(final String searchTerm) {
		super(searchTerm);
	}
}
