/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 */
public interface IRepositoryObjectWithVersion extends IRepositoryObject, IHasVersion {

	@Override
	boolean equals(Object obj);

	@Override
	int hashCode();

	@Override
	String toString();
}
