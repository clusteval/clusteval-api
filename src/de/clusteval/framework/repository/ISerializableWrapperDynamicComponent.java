/**
 * 
 */
package de.clusteval.framework.repository;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableWrapperDynamicComponent<R extends IRepositoryObjectDynamicComponent>
		extends
			ISerializableWrapperRepositoryObject<R> {
	
	boolean hasAlias();

	String getAlias();

	String toString(final boolean readable);

}