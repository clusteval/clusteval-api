/**
 * 
 */
package de.clusteval.framework.repository;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ObjectVersionNotRegisteredException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4231836157010892544L;
	protected ComparableVersion version;
	protected Class<? extends IRepositoryObject> clazz;
	protected String name;

	public ObjectVersionNotRegisteredException(final Class<? extends IRepositoryObject> c, final String name,
			final ComparableVersion version) {
		super(String.format("Version 'v%s' of object '%s' of type '%s' is not registered.", version, name,
				c.getSimpleName()));
		this.version = version;
		this.clazz = c;
		this.name = name;
	}

	/**
	 * @return the clazz
	 */
	public Class<? extends IRepositoryObject> getClazz() {
		return clazz;
	}

	/**
	 * @return the name
	 */
	public String getObjectName() {
		return name;
	}

	/**
	 * @return the version
	 */
	public ComparableVersion getVersion() {
		return version;
	}
}
