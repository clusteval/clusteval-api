/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.run.runnable.IDataAnalysisRunRunnable;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataAnalysisRun
		extends
			IAnalysisRun<IDataStatistic, IDataAnalysisRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	IDataAnalysisRun clone();

	/**
	 * @return A list containing all data configurations to be assessed by this
	 *         run.
	 */
	List<IDataConfig> getDataConfigs();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	boolean terminate();

}