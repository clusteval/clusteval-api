/**
 * 
 */
package de.clusteval.run;

import java.util.List;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableRunAnalysisRun<R extends IRunAnalysisRun> extends ISerializableAnalysisRun<R> {

	List<String> getUniqueRunAnalysisRunIdentifiers();

}