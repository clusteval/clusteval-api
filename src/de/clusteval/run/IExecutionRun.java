/**
 * 
 */
package de.clusteval.run;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.IExecutionRunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IExecutionRun<RUNNABLE_TYPE extends IExecutionRunRunnable<?, ?>>
		extends
			IRun<RUNNABLE_TYPE> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IExecutionRun<RUNNABLE_TYPE> clone();

	boolean terminate();

	/**
	 * This method will perform this run, i.e. it combines every program with
	 * every dataset contained in this run's configuration. For every such
	 * combination a runnable is performed and the result of this will be added
	 * to the list of run results.
	 * 
	 * @throws RunRunnableInitializationException
	 * @throws RunInitializationException
	 */
	void perform(IRunSchedulerThread runScheduler) throws IOException,
			RunRunnableInitializationException, RunInitializationException;

	/**
	 * This method will resume this run, i.e. it combines every program with
	 * every dataset contained in this run's configuration. For every such
	 * combination a runnable is performed and the result of this will be added
	 * to the list of run results.
	 * 
	 * @throws RunRunnableInitializationException
	 * @throws RunInitializationException
	 */
	void resume(IRunSchedulerThread runScheduler, String runIdentString)
			throws MissingParameterValueException, IOException,
			NoRunResultFormatParserException,
			RunRunnableInitializationException, RunInitializationException;

	/**
	 * @return The list of runpairs consisting of program and data
	 *         configurations each.
	 */
	List<Pair<IProgramConfig, IDataConfig>> getClonedProgramAndDataConfigPairs();

	/**
	 * @return A list of maps containing parameter values. Every entry of the
	 *         list corresponds to one runpair.
	 */
	Map<IProgramConfig, Map<IProgramParameter<?>, String>> getFixedParameterValues();

	/**
	 * @return
	 */
	List<IRunResultPostprocessor> getPostProcessors();

	/**
	 * @return A list containing all program configurations of this run.
	 */
	List<IProgramConfig> getOriginalProgramConfigs();

	/**
	 * @return A list containing all data configurations of this run.
	 */
	List<IDataConfig> getOriginalDataConfigs();

	/**
	 * @return A list containing all clustering quality measures to be evaluated
	 *         during execution of this run.
	 */
	List<IClusteringQualityMeasure> getQualityMeasures();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	boolean hasMaxExecutionTime(IProgramConfig pc);

	int getMaxExecutionTime(IProgramConfig pc);

	ISerializableExecutionRun<? extends IExecutionRun> asSerializable()
			throws RepositoryObjectSerializationException;
}