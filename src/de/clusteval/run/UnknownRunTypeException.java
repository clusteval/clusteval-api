/**
 * 
 */
package de.clusteval.run;

import de.clusteval.utils.ClustEvalException;

public class UnknownRunTypeException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6103483126512531683L;

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownRunTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UnknownRunTypeException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UnknownRunTypeException(Throwable cause) {
		super(cause);
	}

}