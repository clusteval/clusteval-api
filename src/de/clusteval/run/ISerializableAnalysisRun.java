/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableAnalysisRun<R extends IAnalysisRun<? extends IStatistic, ?>>
		extends
			ISerializableRun<R> {

	/**
	 * @return the statistics
	 */
	List<ISerializableStatistic> getStatistics();

}