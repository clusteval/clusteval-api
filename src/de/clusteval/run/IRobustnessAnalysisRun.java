/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.runnable.IRobustnessAnalysisRunRunnable;

/**
 * @author Christian Wiwie
 *
 */
public interface IRobustnessAnalysisRun
		extends
			IClusteringRun<IRobustnessAnalysisRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#clone()
	 */
	IRobustnessAnalysisRun clone();

	void setOriginalDataConfigurations(List<IDataConfig> dataConfigs);

	List<String> getRunResultIdentifiers();

	IDataRandomizer getRandomizer();

	int getNumberOfDistortedDataSets();

	List<ParameterSet> getDistortionParams();

}