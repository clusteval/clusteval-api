/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableExecutionRun<R extends IExecutionRun<?>>
		extends
			ISerializableRun<R> {

	/**
	 * @return the dataConfigs
	 */
	List<ISerializableDataConfig> getDataConfigs();

	/**
	 * @return the programConfigs
	 */
	List<ISerializableProgramConfig<? extends IProgramConfig>> getProgramConfigs();

	/**
	 * @return the qualityMeasures
	 */
	List<ISerializableClusteringQualityMeasure> getQualityMeasures();

	/**
	 * @return the postProcessors
	 */
	List<ISerializableRunResultPostprocessor> getPostProcessors();

}