/**
 * 
 */
package de.clusteval.run;

/**
 * This enum holds a string representation for each of the available run types
 * as well as the corresponding class. The string representations are mostly
 * used in configuration files but also to distinguish between run types on
 * runtime.
 * 
 * @author Christian Wiwie
 *
 */
public enum RUN_TYPE {
	/**
	 * Clustering runs
	 */
	CLUSTERING("clustering", "Clustering", IClusteringRun.class),
	/**
	 * Parameter optimization runs
	 */
	PARAMETER_OPTIMIZATION("parameter_optimization", "Parameter Optimization",
			IParameterOptimizationRun.class),
	/**
	 * Internal parameter optimization runs
	 */
	INTERNAL_PARAMETER_OPTIMIZATION("internal_parameter_optimization",
			"Internal Parameter Optimization",
			IInternalParameterOptimizationRun.class),
	/**
	 * Robustness analysis runs
	 */
	ROBUSTNESS_ANALYSIS("robustnessAnalysis", "Robustness Analysis",
			IRobustnessAnalysisRun.class),
	/**
	 * Robustness analysis runs
	 */
	TOOL_RUNTIME_EVALUATION("toolRuntimeEvaluation", "Tool Runtime Evaluation",
			IToolRuntimeEvaluationRun.class),
	/**
	 * Data analysis runs
	 */
	DATA_ANALYSIS("dataAnalysis", "Data Analysis", IDataAnalysisRun.class),
	/**
	 * Run analysis runs
	 */
	RUN_ANALYSIS("runAnalysis", "Run Analysis", IRunAnalysisRun.class),
	/**
	 * Run data analysis runs
	 */
	RUN_DATA_ANALYSIS("runDataAnalysis", "Run-Data Analysis",
			IRunDataAnalysisRun.class);

	private String internalRepresentation, alias;
	private Class<? extends IRun> runClass;

	private RUN_TYPE(final String internal_representation, final String alias,
			final Class<? extends IRun> runClass) {
		this.internalRepresentation = internal_representation;
		this.alias = alias;
		this.runClass = runClass;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @return the internalRepresentation
	 */
	public String getInternalRepresentation() {
		return internalRepresentation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.internalRepresentation;
	}

	public String toString(final boolean readable) {
		if (!readable)
			return this.toString();
		return this.alias;
	}

	public static RUN_TYPE getForClass(final Class<? extends IRun> type)
			throws UnknownRunTypeException {
		if (IRobustnessAnalysisRun.class.isAssignableFrom(type))
			return ROBUSTNESS_ANALYSIS;
		else if (IToolRuntimeEvaluationRun.class.isAssignableFrom(type))
			return TOOL_RUNTIME_EVALUATION;
		else if (IClusteringRun.class.isAssignableFrom(type))
			return CLUSTERING;
		else if (IParameterOptimizationRun.class.isAssignableFrom(type))
			return PARAMETER_OPTIMIZATION;
		else if (IInternalParameterOptimizationRun.class.isAssignableFrom(type))
			return INTERNAL_PARAMETER_OPTIMIZATION;
		else if (IDataAnalysisRun.class.isAssignableFrom(type))
			return DATA_ANALYSIS;
		else if (IRunAnalysisRun.class.isAssignableFrom(type))
			return RUN_ANALYSIS;
		else if (IRunDataAnalysisRun.class.isAssignableFrom(type))
			return RUN_DATA_ANALYSIS;
		throw new UnknownRunTypeException(
				"There is no run type associated with class: "
						+ type.getSimpleName());
	}
}