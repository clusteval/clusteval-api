/**
 * 
 */
package de.clusteval.run;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableRobustnessAnalysisRun<R extends IRobustnessAnalysisRun>
		extends
			ISerializableClusteringRun<R> {

}