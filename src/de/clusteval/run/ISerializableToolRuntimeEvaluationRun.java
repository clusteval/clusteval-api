/**
 * 
 */
package de.clusteval.run;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 *
 * @since Nov 6, 2017
 */
public interface ISerializableToolRuntimeEvaluationRun<R extends IToolRuntimeEvaluationRun<?>>
		extends
			ISerializableClusteringRun<R> {

}