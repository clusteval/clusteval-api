/**
 * 
 */
package de.clusteval.run;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectWithVersion;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * @author Christian Wiwie
 *
 */
public interface IRun<RUNNABLE_TYPE extends IRunRunnable<?, ?, ?, ?>>
		extends
			IRepositoryObjectWithVersion,
			IDumpableRepositoryObject {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#clone()
	 */
	@Override
	IRun clone();

	/**
	 * This method creates instances of subclasses of IRunRunnable and passes
	 * them to the run scheduler. The run scheduler then executes the run
	 * runnables at some point in the future depending on the available physical
	 * ressources.
	 * 
	 * <p>
	 * The IRunRunnable objects represent atomic peaces of the overall run,
	 * which can be executed in parallel.
	 * 
	 * @param runScheduler
	 *            The run scheduler, this run should be executed by.
	 * @throws RunRunnableInitializationException
	 */
	void doPerform(IRunSchedulerThread runScheduler)
			throws RunRunnableInitializationException;

	/**
	 * This method will perform this run, i.e. it combines every program with
	 * every dataset contained in this run's configuration. For every such
	 * combination the result of this will be added to the list of run results
	 * 
	 * @param runScheduler
	 *            The run scheduler, this run should be executed by.
	 * @param runIdentString
	 *            The unique run identifier of the results directory,
	 *            corresponding to an execution of a run, that should by
	 *            resumed.
	 * @throws RunRunnableInitializationException
	 */
	void doResume(IRunSchedulerThread runScheduler, String runIdentString)
			throws RunRunnableInitializationException;

	/**
	 * @see #logFilePath
	 * @return The path to the log file when this run is executed.
	 */
	String getLogFilePath();

	/**
	 * The name of a run is used to uniquely identify runs.
	 * 
	 * <p>
	 * The name of a run is deduced from the filename of the run file. It is the
	 * filename without file extension.
	 * 
	 * @return the name
	 */
	String getName();

	/**
	 * Returns the current status of this run execution in terms of finished
	 * percentage. Depending on the current status of the run, this method
	 * returns different values:
	 * <p>
	 * If the run is scheduled, finished or terminated this method always
	 * returns 100%.
	 * <p>
	 * If the run is currently running, the percentage of the execution is
	 * returned.
	 * <p>
	 * Otherwise this method returns 0%.
	 * 
	 * @return The percent finished
	 */
	float getPercentFinished();

	float getPercentFinishedDuringCurrentExecution();

	long getEstimatedRemainingCPUTimeInMs();

	/**
	 * Gets the results.
	 * 
	 * @return Get the list of run results that are produced by the execution of
	 *         this run.
	 */
	List<IRunResult> getResults();

	/**
	 * @see #runIdentString
	 * @return The unique run identification string created when this run is
	 *         executed.
	 */
	String getRunIdentificationString();

	void setRunIdentificationString(String runIdentString);

	/**
	 * @return A list containing all run runnables this run created during its
	 *         execution.
	 */
	List<RUNNABLE_TYPE> getRunRunnables();

	/**
	 * @see #status
	 * @return the status
	 */
	RUN_STATUS getStatus();

	/**
	 * @return the startTime
	 */
	long getStartTime();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/**
	 * This method will perform this run.
	 * <p>
	 * First this method invokes {@link #beforePerform()} and does all necessary
	 * preoperations before the actual run is started. Afterwards in
	 * {@link #doPerform(IRunSchedulerThread)} the runnables are created and
	 * executed asynchronously by the run scheduler. Then invoking
	 * {@link #waitForRunnablesToFinish()} guarantees that the method waits
	 * until all calculations are finished. Last {@link #afterPerform()}
	 * performs additional operations with the results after all calculations
	 * are finished.
	 * 
	 * @param runScheduler
	 *            The run scheduler, this run should be executed by.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RunRunnableInitializationException
	 * @throws RunInitializationException
	 * @throws RegisterException
	 */
	void perform(IRunSchedulerThread runScheduler) throws IOException,
			RunRunnableInitializationException, RunInitializationException;

	/**
	 * This method will resume a previously started run. This method should only
	 * be invoked on a run, that was parsed from a runresult folder. Otherwise
	 * it will show unexpected behaviour.
	 * 
	 * @param runScheduler
	 *            The run scheduler, this run should be executed by.
	 * @param runIdentString
	 *            The unique run identifier of the results directory,
	 *            corresponding to an execution of a run, that should by
	 *            resumed.
	 * 
	 * @throws MissingParameterValueException
	 *             If a parameter required in the invocation line of some
	 *             program, is neither set in the program nor in the run
	 *             configuration, an exception will be thrown.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws NoRunResultFormatParserException
	 *             For every {@link RunResultFormat} there needs to be a parser,
	 *             that converts this format into the default format of the
	 *             framework for later analysis. If no such parser exists for
	 *             some format, this exception will be thrown.
	 * @throws RunRunnableInitializationException
	 * @throws RunInitializationException
	 */
	void resume(IRunSchedulerThread runScheduler, String runIdentString)
			throws MissingParameterValueException, IOException,
			NoRunResultFormatParserException,
			RunRunnableInitializationException, RunInitializationException;

	/**
	 * Sets the status of this run.
	 * 
	 * @param status
	 */
	void setStatus(RUN_STATUS status);

	/**
	 * This method terminates the execution of this run. It waits for the
	 * termination of all corresponding threads.
	 * 
	 * @return True if everything, including all corresponding threads, could be
	 *         terminated, false otherwise.
	 */
	boolean terminate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * @return A map with the optimization status of this run.
	 */
	Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus();

	/**
	 * 
	 * @return The context of this run.
	 */
	IContext getContext();

	/**
	 * 
	 * @return Get the thrown exceptions of all runnables during the last run
	 *         execution.
	 */
	Map<IRunRunnable, List<Exception>> getExceptions();

	/**
	 * Clears all exceptions of this run, i.e. of all the runnables.
	 * 
	 */
	void clearExceptions();

	/**
	 * 
	 * @return Get the less severe thrown exceptions of all runnables during the
	 *         last run execution.
	 */
	Map<IRunRunnable, List<Exception>> getWarningExceptions();

	/**
	 * Clears all warning exceptions of this run, i.e. of all the runnables.
	 * 
	 */
	void clearWarningExceptions();

	ProgressPrinter getProgress();

	ISerializableRun<? extends IRun> asSerializable()
			throws RepositoryObjectSerializationException;
}