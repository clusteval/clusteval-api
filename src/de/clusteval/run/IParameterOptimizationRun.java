/**
 * 
 */
package de.clusteval.run;

import java.util.List;
import java.util.Map;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.IParameterOptimizationRunRunnable;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IParameterOptimizationRun
		extends
			IExecutionRun<IParameterOptimizationRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	IParameterOptimizationRun clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#notify(framework.repository.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	/**
	 * @return A list of parameter lists for every program configuration, that
	 *         are to be optimized.
	 * @see #optimizationParameters
	 */
	Map<IProgramConfig, List<IProgramParameter<?>>> getOptimizationParameters();

	/**
	 * @return A list with optimization methods. One method for every program.
	 * @see #optimizationMethods
	 */
	List<IParameterOptimizationMethod> getOptimizationMethods();

	Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus();

	ISerializableParameterOptimizationRun<? extends IParameterOptimizationRun> asSerializable()
			throws RepositoryObjectSerializationException;

	/**
	 * @return The quality measure used as the optimization criterion (see
	 *         {@link #optimizationCriterion}).
	 */
	IClusteringQualityMeasure getOptimizationCriterion();

	int getDesiredTotalIterationCount();

	String getDefaultOptimizationMethod();

	Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> getOptimizationParameterOptionsOverrides();

	Map<IProgramConfig, Map<IProgramParameter<?>, String>> getOptimizationParameterMinValueOverrides();

	Map<IProgramConfig, Map<IProgramParameter<?>, String>> getOptimizationParameterMaxValueOverrides();
}