/**
 * 
 */
package de.clusteval.run.statistics;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunStatistic extends IStatistic {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IRunStatistic clone();

}