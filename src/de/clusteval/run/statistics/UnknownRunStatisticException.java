/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.statistics;

import de.clusteval.framework.repository.UnknownDynamicComponentException;

/**
 * @author Christian Wiwie
 * 
 */
public class UnknownRunStatisticException extends UnknownDynamicComponentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2173877271722904825L;

	/**
	 * @param name
	 * @param message
	 */
	public UnknownRunStatisticException(final String name, String message) {
		super(IRunStatistic.class, name, message);
	}

	/**
	 * @param name
	 * @param cause
	 */
	public UnknownRunStatisticException(final String name, Throwable cause) {
		super(IRunStatistic.class, name, cause);
	}
}
