/**
 * 
 */
package de.clusteval.run.statistics;

import de.clusteval.data.statistics.StatisticCalculateException;
import de.clusteval.utils.IStatisticCalculator;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IRunDataStatisticCalculator<T extends IRunDataStatistic> extends IStatisticCalculator<T> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.StatisticCalculator#clone()
	 */
	IRunDataStatisticCalculator<T> clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.StatisticCalculator#calculate()
	 */
	T calculate() throws StatisticCalculateException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.StatisticCalculator#getStatistic()
	 */
	T getStatistic();

}