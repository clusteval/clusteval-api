/**
 * 
 */
package de.clusteval.run;

import java.util.List;
import java.util.Map;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.run.runnable.IRunRunnable;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableRun<R extends IRun> extends ISerializableWrapperRepositoryObject<R> {

	/**
	 * @return the context
	 */
	ISerializableWrapperRepositoryObject<IContext> getContext();

	long getEstimatedRemainingCPUTimeInMs();

	String getLogFilePath();

	Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus();

	float getPercentFinished();

	String getRunIdentificationString();

	long getStartTime();

	RUN_STATUS getStatus();

	Map<IRunRunnable, List<Exception>> getWarningExceptions();

	List<IRunRunnable> getRunRunnables();

	/**
	 * @return the progress
	 */
	ProgressPrinter getProgress();

}