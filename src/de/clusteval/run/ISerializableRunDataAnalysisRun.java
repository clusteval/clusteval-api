/**
 * 
 */
package de.clusteval.run;

import java.util.List;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableRunDataAnalysisRun<R extends IRunDataAnalysisRun> extends ISerializableAnalysisRun<R> {

	List<String> getUniqueDataAnalysisRunIdentifiers();

	List<String> getUniqueRunAnalysisRunIdentifiers();

}