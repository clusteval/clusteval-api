/**
 * 
 */
package de.clusteval.run;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableClusteringRun<R extends IClusteringRun<?>>
		extends
			ISerializableExecutionRun<R> {

}