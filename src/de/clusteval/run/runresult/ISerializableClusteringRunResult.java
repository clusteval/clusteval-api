/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableClusteringRunResult<R extends IClusteringRunResult>
		extends
			ISerializableExecutionRunResult<R> {

	/**
	 * @return the clustering
	 */
	ISerializableClustering getClustering();

	/**
	 * @return the resultFormat
	 */
	ISerializableRunResultFormat getResultFormat();

	ParameterSet getParameterSet();

}