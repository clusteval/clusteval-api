/**
 * 
 */
package de.clusteval.run.runresult.format;

import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunResultFormat extends IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	IRunResultFormat clone();

	@Override
	ISerializableRunResultFormat asSerializable()
			throws RepositoryObjectSerializationException;

	Class<? extends IRunResultFormatParser> getRunResultFormatParser();
}