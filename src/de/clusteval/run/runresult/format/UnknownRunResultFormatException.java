/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult.format;

import de.clusteval.framework.repository.UnknownDynamicComponentException;

/**
 * The Class UnknownRunResultFormatException.
 * 
 * @author Christian Wiwie
 */
public class UnknownRunResultFormatException extends UnknownDynamicComponentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4703586012634484344L;

	/**
	 * @param name
	 * @param message
	 */
	public UnknownRunResultFormatException(final String name, String message) {
		super(IRunResultFormat.class, name, message);
	}

	/**
	 * @param name
	 * @param cause
	 */
	public UnknownRunResultFormatException(final String name, Throwable cause) {
		super(IRunResultFormat.class, name, cause);
	}

}
