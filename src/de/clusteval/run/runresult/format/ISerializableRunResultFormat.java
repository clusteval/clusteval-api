/**
 * 
 */
package de.clusteval.run.runresult.format;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableRunResultFormat extends ISerializableWrapperDynamicComponent<IRunResultFormat> {

}