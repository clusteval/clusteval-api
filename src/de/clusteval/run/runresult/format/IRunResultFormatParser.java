/**
 * 
 */
package de.clusteval.run.runresult.format;

import java.io.IOException;

import de.clusteval.run.runresult.IClusteringRunResult;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunResultFormatParser {

	/**
	 * Gets the run result.
	 * 
	 * @return the run result
	 */
	IClusteringRunResult getRunResult();

	/**
	 * Convert to standard format.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	void convertToStandardFormat() throws IOException;

}