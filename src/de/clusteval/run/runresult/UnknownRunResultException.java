/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class UnknownRunResultException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 588601811711531678L;

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownRunResultException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UnknownRunResultException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UnknownRunResultException(Throwable cause) {
		super(cause);
	}

}
