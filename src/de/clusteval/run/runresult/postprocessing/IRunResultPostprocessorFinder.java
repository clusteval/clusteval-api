/**
 * 
 */
package de.clusteval.run.runresult.postprocessing;

import de.clusteval.utils.IJARFinder;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunResultPostprocessorFinder extends IJARFinder<IRunResultPostprocessor> {

}