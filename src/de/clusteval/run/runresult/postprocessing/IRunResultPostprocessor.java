/**
 * 
 */
package de.clusteval.run.runresult.postprocessing;

import de.clusteval.cluster.IClustering;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.r.RLibraryInferior;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunResultPostprocessor
		extends
			RLibraryInferior,
			IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	IRunResultPostprocessor clone();

	/**
	 * This method is reponsible for postprocessing the passed clustering and
	 * creating a new clustering object corresponding to the newly created
	 * postprocessed clustering.
	 * 
	 * @param clustering
	 *            The clustering to be postprocessed.
	 * @return The postprocessed clustering.
	 */
	IClustering postprocess(IClustering clustering);

	@Override
	ISerializableRunResultPostprocessor asSerializable()
			throws RepositoryObjectSerializationException;
}