/**
 * 
 */
package de.clusteval.run.runresult.postprocessing;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableRunResultPostprocessor
		extends
			ISerializableWrapperDynamicComponent<IRunResultPostprocessor> {

}