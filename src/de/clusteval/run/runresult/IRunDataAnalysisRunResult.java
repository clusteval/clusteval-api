/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.run.IRunDataAnalysisRun;
import de.clusteval.run.statistics.IRunDataStatistic;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunDataAnalysisRunResult
		extends
			IAnalysisRunResult<Pair<List<String>, List<String>>, IRunDataStatistic> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#clone()
	 */
	IRunDataAnalysisRunResult clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	IRunDataAnalysisRun getRun();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	void loadIntoMemory() throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	void unloadFromMemory();

	/**
	 * @return A set with all pairs of identifiers of run analysis runresults
	 *         and data analysis runresults.
	 */
	Set<Pair<List<String>, List<String>>> getUniqueIdentifierPairs();

	/**
	 * @param uniqueRunIdentifierPair
	 *            A pair with identifier of run analysis runresult and data
	 *            analysis runresult for which we want to know which run-data
	 *            statistics were evaluated.
	 * @return A list with all run-data statistics that were evaluated for the
	 *         given pair.
	 */
	Map<IRunDataStatistic, String> getRunDataStatisticValues(
			Pair<List<String>, List<String>> uniqueRunIdentifierPair);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

}