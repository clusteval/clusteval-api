/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 */
public interface IParameterOptimizationResult extends IExecutionRunResult {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	IParameterOptimizationResult clone();

	/**
	 * This method adds the given qualities for the given parameter set and
	 * resulting clustering.
	 * 
	 * @param iterationNumber
	 *            The number of the iteration.
	 * 
	 * @param last
	 *            The parameter set for which we want to add clustering
	 *            qualities.
	 * @param qualities
	 *            The qualities which we want to add for the parameter set.
	 * @param clustering
	 *            The clustering resulting the given parameter set.
	 * @return The old value, if this operation replaced an old mapping,
	 */
	IClusteringQualitySet put(long iterationNumber, IClusteringQualitySet qualities, IClustering clustering);

	/**
	 * @return The parameter set which led to the highest clustering quality of
	 *         the optimization criterion.
	 */
	ParameterSet getOptimalParameterSet();

	/**
	 * @return A map with the optimal parameter sets for every clustering
	 *         quality measure.
	 */
	Map<IClusteringQualityMeasure, Long> getOptimalIterations();

	/**
	 * @return The optimal quality value achieved for the optimization
	 *         criterion.
	 */
	IClusteringQualitySet getOptimalCriterionValue();

	/**
	 * @return The clustering corresponding to the highest achieved quality
	 *         value for the optimization criterion (see
	 *         {@link #getOptimalCriterionValue()}).
	 */
	IClustering getOptimalClustering();

	/**
	 * @return A map with all clustering quality measures together with the
	 *         clusterings which achieved the highest quality values for each of
	 *         those.
	 */
	Map<IClusteringQualityMeasure, IClustering> getOptimalClusterings();

	/**
	 * @return A list of pairs containing all parameter sets evaluated during
	 *         the optimization process together with the optimal resulting
	 *         quality sets.
	 */
	Map<Long, IClusteringQualitySet> getOptimizationQualities();

	void addParameterSet(long iterationNumber, ParameterSet paramSet);

	/**
	 * @return A list with all evaluated parameter sets of this optimization
	 *         process.
	 */
	Map<Long, ParameterSet> getParameterSets();

	/**
	 * @return A list with all evaluated parameter sets of this optimization
	 *         process.
	 */
	List<ParameterSet> getParameterSetsList();

	/**
	 * @return the parameterSetsWithoutQuality
	 */
	Set<Long> getIterationsWithoutQuality();

	/**
	 * @return A list with all evaluated parameter sets of this optimization
	 *         process.
	 */
	List<Long> getIterationNumbers();

	Set<Long> getIterationNumberForParameterSet(ParameterSet parameterSet);

	/**
	 * @return A list of pairs containing all parameter sets evaluated during
	 *         the optimization process together with the optimal resulting
	 *         clusterings.
	 */
	List<Pair<ParameterSet, IClustering>> getOptimizationClusterings();

	/**
	 * @return The parameter optimization method which created this result.
	 */
	IParameterOptimizationMethod getMethod();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	IParameterOptimizationRun getRun();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	void loadIntoMemory() throws RunResultParseException;

	void loadIntoMemory(boolean onlyOptimalResults) throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#register()
	 */
	boolean register() throws RegisterException;

	/**
	 * This method clears all internal attributes that do not store the optimal
	 * results (those might be needed afterwards). This includes
	 * {@link #iterationToParameterSets}, {@link #iterationToClustering} and
	 * {@link #iterationToQualities}.
	 */
	void unloadFromMemory();

	/**
	 * @param paramSet
	 *            The parameter set for which we want the resulting clustering
	 *            quality set.
	 * @return The clustering quality set resulting from the given parameter
	 *         set.
	 */
	IClusteringQualitySet get(long iterationNumber);

	/**
	 * 
	 * @param paramSet
	 *            The parameter set for which we want to know the resulting
	 *            clustering.
	 * @return The clustering resulting from the given parameter set.
	 */
	IClustering getClustering(long iteration);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	Iterator<Pair<ParameterSet, IClusteringQualitySet>> iterator();

	Map<ParameterSet, Set<Long>> getParameterSetToIterationNumber();

	Map<Long, IClusteringQualitySet> getIterationToQualities();

	Map<Long, ParameterSet> getIterationToParameterSets();

	Map<Long, IClustering> getIterationToClustering();

	boolean isStoreClusterings();

	boolean isParseClusterings();

	void setStoreClusterings(boolean storeClusterings);

	void setParseClusterings(boolean parseClusterings);

}