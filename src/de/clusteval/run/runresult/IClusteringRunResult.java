/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.cluster.IClustering;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusteringRunResult extends IExecutionRunResult {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	IClusteringRunResult clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/**
	 * @return The clustering corresponding to this clustering run result.
	 */
	IClustering getClustering();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	void loadIntoMemory() throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	void unloadFromMemory();

	@Override
	ISerializableClusteringRunResult<? extends IClusteringRunResult> asSerializable()
			throws RepositoryObjectSerializationException;

	ParameterSet getParameterSet();

	void setFormat(IRunResultFormat resultFormat);

	IRunResultFormat getFormat();
}