/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.run.IRunAnalysisRun;
import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunAnalysisRunResult
		extends
			IAnalysisRunResult<String, IRunStatistic> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#clone()
	 */
	IRunAnalysisRunResult clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	IRunAnalysisRun getRun();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	void loadIntoMemory() throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	void unloadFromMemory();

	/**
	 * @return A set with all runresult identifiers, which the run analysed
	 *         which produced this runresult.
	 */
	Set<String> getUniqueRunIdentifier();

	/**
	 * @param uniqueRunIdentifier
	 *            The runresult identifier for which we want to know the
	 *            assessed run statistics.
	 * @return A list with all run statistics assessed for the given runresult
	 *         identifier.
	 */
	Map<IRunStatistic, String> getRunStatisticValues(
			String uniqueRunIdentifier);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

}