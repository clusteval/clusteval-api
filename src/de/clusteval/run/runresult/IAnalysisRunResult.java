/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.Map;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <S>
 * @param <T>
 */
public interface IAnalysisRunResult<S extends Object, T extends IStatistic>
		extends
			IRunResult {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	IAnalysisRunResult<S, T> clone();

	/**
	 * @param object
	 * @param statisticValues
	 */
	void put(S object, Map<T, String> statisticValues);

	Map<S, Map<T, String>> getStatisticValues();

}