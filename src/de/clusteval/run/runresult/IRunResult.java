/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.IRun;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunResult extends IRepositoryObject {

	/**
	 * @return The unique identifier of this runresult, equal to the name of the
	 *         runresult folder.
	 */
	String getIdentifier();

	/**
	 * @return The run this runresult belongs to.
	 */
	IRun getRun();

	/**
	 * Checks, whether this run result is currently held in memory.
	 * 
	 * @return True, if this run result is currently held in memory. False
	 *         otherwise.
	 */
	boolean isInMemory();

	/**
	 * This method loads the contents of this run result into the memory by
	 * parsing the files on the filesystem.
	 * 
	 * <p>
	 * The run result might consume a lot of memory afterwards. Only invoke this
	 * method, if you really need access to the run results contents and
	 * afterwards free the contents by invoking {@link #unloadFromMemory()}.
	 * 
	 * @throws RunResultParseException
	 */
	void loadIntoMemory() throws RunResultParseException;

	/**
	 * This method unloads the contents of this run result from the memory and
	 * releases the reserved memory. This can be helpful especially for large
	 * parameter optimization run results.
	 */
	void unloadFromMemory();

	boolean hasChangedSinceLastRegister();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	boolean register() throws RegisterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#asSerializable()
	 */
	@Override
	ISerializableRunResult<? extends IRunResult> asSerializable()
			throws RepositoryObjectSerializationException;
}