/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableRunResult<R extends IRunResult>
		extends
			ISerializableWrapperRepositoryObject<R> {

	/**
	 * @return the run
	 */
	ISerializableRun<? extends IRun> getRun();

	/**
	 * @return the runIdentString
	 */
	String getIdentifier();

}