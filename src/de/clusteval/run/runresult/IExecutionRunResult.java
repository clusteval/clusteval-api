/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;

/**
 * @author Christian Wiwie
 *
 */
public interface IExecutionRunResult extends IRunResult {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	IExecutionRunResult clone();

	/**
	 * @return The program configuration wrapping the program that produced this
	 *         runresult.
	 */
	IProgramConfig getProgramConfig();

	/**
	 * @return The data configuration wrapping the dataset on which this
	 *         runresult was produced.
	 */
	IDataConfig getDataConfig();

	@Override
	ISerializableExecutionRunResult<? extends IExecutionRunResult> asSerializable()
			throws RepositoryObjectSerializationException;

	Map<Long, Map<String, String>> getMetaDataOfAllIterations();

	Map<String, String> getMetaDataOfIteration(final long iteration);

	void setMetaDataForIteration(final long iteration,
			final Map<String, String> metaData);
}