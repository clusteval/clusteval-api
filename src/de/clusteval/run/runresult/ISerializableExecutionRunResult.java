/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableExecutionRunResult<R extends IExecutionRunResult>
		extends
			ISerializableRunResult<R> {

	/**
	 * @return the dataConfig
	 */
	ISerializableDataConfig getDataConfig();

	/**
	 * @return the programConfig
	 */
	ISerializableProgramConfig<? extends IProgramConfig> getProgramConfig();

}