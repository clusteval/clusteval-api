/**
 * 
 */
package de.clusteval.run.runresult;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableDataAnalysisRunResult
		extends
			ISerializableAnalysisRunResult<IDataConfig, IDataStatistic, IDataAnalysisRunResult> {

}