/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.Map;

import de.clusteval.utils.IObjectToBeSerialized;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <O>
 * @param <S>
 * @param <R>
 */
public interface ISerializableAnalysisRunResult<O extends IObjectToBeSerialized, S extends IStatistic, R extends IAnalysisRunResult<O, S>>
		extends
			ISerializableRunResult<R> {

	/**
	 * @return the statistics
	 */
	Map<O, Map<ISerializableStatistic<S>, String>> getStatistics();

}