/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.Map;
import java.util.Set;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.run.IDataAnalysisRun;

/**
 * @author Christian Wiwie
 *
 */
public interface IDataAnalysisRunResult
		extends
			IAnalysisRunResult<IDataConfig, IDataStatistic> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#clone()
	 */
	IDataAnalysisRunResult clone();

	/**
	 * @return The data configurations encapsulating the datasets that were
	 *         analysed.
	 */
	Set<IDataConfig> getDataConfigs();

	/**
	 * @param dataConfig
	 *            The data configuration for which we want to know which data
	 *            statistics were evaluated.
	 * @return The data statistics that were assessed for the given data
	 *         configuration.
	 */
	Map<IDataStatistic, String> getDataStatisticValues(IDataConfig dataConfig);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	void loadIntoMemory() throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	void unloadFromMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	IDataAnalysisRun getRun();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	String toString();

}