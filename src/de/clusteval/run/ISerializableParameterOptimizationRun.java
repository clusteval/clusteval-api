/**
 * 
 */
package de.clusteval.run;

import java.util.List;
import java.util.Map;

import de.clusteval.cluster.paramOptimization.ISerializableParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableParameterOptimizationRun<R extends IParameterOptimizationRun>
		extends
			ISerializableExecutionRun<R> {

	/**
	 * @return the optimizationMethods
	 */
	List<ISerializableParameterOptimizationMethod> getOptimizationMethods();

	/**
	 * @return the optimizationCriterion
	 */
	ISerializableWrapperRepositoryObject<IClusteringQualityMeasure> getOptimizationCriterion();

	/**
	 * @return the totalIterationCount
	 */
	int getTotalIterationCount();

	Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String[]>> getOptimizationParameterOptionsOverrides();

	Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> getOptimizationParameterMinValueOverrides();

	Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> getOptimizationParameterMaxValueOverrides();

	Map<ISerializableProgramConfig<? extends IProgramConfig>, List<IProgramParameter<?>>> getOptimizationParameters();

}