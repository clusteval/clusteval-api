/**
 * 
 */
package de.clusteval.run;

import de.clusteval.run.runnable.IInternalParameterOptimizationRunRunnable;

/**
 * @author Christian Wiwie
 *
 */
public interface IInternalParameterOptimizationRun
		extends
			IExecutionRun<IInternalParameterOptimizationRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#clone()
	 */
	IInternalParameterOptimizationRun clone();

}