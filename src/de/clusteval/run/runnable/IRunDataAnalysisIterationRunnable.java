/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunDataStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunDataAnalysisIterationRunnable
		extends
			IAnalysisIterationRunnable<IRunDataStatistic, IRunDataAnalysisIterationWrapper, IRunDataAnalysisRunRunnable> {

	String getRunIdentifier();

	String getDataAnalysisIdentifier();

}