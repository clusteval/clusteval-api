/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunDataStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunDataAnalysisIterationWrapper
		extends
			IAnalysisIterationWrapper<IRunDataStatistic> {

	String getUniqueDataAnalysisRunIdentifier();

	void setUniqueDataAnalysisRunIdentifier(
			String uniqueDataAnalysisRunIdentifier);

	String getRunIdentifier();

	void setRunIdentifier(String runIdentifier);

}