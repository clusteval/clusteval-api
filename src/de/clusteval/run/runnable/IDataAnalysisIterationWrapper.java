/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IDataAnalysisIterationWrapper
		extends
			IAnalysisIterationWrapper<IDataStatistic> {

	IDataConfig getDataConfig();

	void setDataConfig(IDataConfig dataConfig);

}