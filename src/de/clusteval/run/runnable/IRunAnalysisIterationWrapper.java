/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunAnalysisIterationWrapper
		extends
			IAnalysisIterationWrapper<IRunStatistic> {

	String getRunIdentifier();

	void setRunIdentifier(String runIdentifier);

}