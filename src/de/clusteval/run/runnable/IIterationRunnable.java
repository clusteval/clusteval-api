/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.IOException;
import java.util.List;

import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.run.IRun;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 *
 * @param <IW>
 */
public interface IIterationRunnable<IW extends IIterationWrapper, PARENT_TYPE extends IRunRunnable>
		extends
			Runnable {

	/**
	 * @return the rNotAvailableException
	 */
	RNotAvailableException getrNotAvailableException();

	/**
	 * @return the ioException
	 */
	IOException getIoException();

	/**
	 * @return the rLibraryException
	 */
	RLibraryNotLoadedException getrLibraryException();

	InterruptedException getInterruptedException();

	/**
	 * @return the exceptions
	 */
	List<Exception> getExceptions();

	/**
	 * less severe exceptions
	 * 
	 * @return the warningExceptions
	 */
	List<Exception> getWarningExceptions();

	PARENT_TYPE getParentRunnable();

	IRun getRun();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	void run();

	long getStartTime();

	long getRunningTime();

	/**
	 * @param isRemote
	 *            the isRemote to set
	 */
	void setRemote(boolean isRemote);

	/**
	 * @return the isRemote
	 */
	boolean isRemote();

	String getCurrentState();

}