/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 *
 */
public class IterationRunnableStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -589702740688839304L;
	protected long startTime;
	protected String currentRunnableState, parentRunnableName, status;

	/**
	 * @param startTime
	 * @param currentRunnableState
	 * @param name
	 * @param status
	 */
	public IterationRunnableStatus(long startTime, String currentRunnableState,
			String name, String status) {
		super();
		this.startTime = startTime;
		this.currentRunnableState = currentRunnableState;
		this.parentRunnableName = name;
		this.status = status;
	}

	/**
	 * @return the currentRunnableState
	 */
	public String getCurrentRunnableState() {
		return currentRunnableState;
	}

	/**
	 * @return the name
	 */
	public String getParentRunnableName() {
		return parentRunnableName;
	}

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
}
