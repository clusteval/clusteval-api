/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IRunDataAnalysisRun;
import de.clusteval.run.runresult.IRunDataAnalysisRunResult;
import de.clusteval.run.statistics.IRunDataStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunDataAnalysisRunRunnable
		extends
			IAnalysisRunRunnable<IRunDataAnalysisRun, IRunDataStatistic, IRunDataAnalysisRunResult, IRunDataAnalysisIterationWrapper, IRunDataAnalysisIterationRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	IRunDataAnalysisRunResult getResult();

}