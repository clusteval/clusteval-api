/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class SkipIterationException extends ClustEvalException {

	/**
	 * @param cause
	 */
	public SkipIterationException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1901779859411655908L;

}
