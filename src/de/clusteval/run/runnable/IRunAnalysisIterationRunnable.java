/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunAnalysisIterationRunnable
		extends
			IAnalysisIterationRunnable<IRunStatistic, IRunAnalysisIterationWrapper, IRunAnalysisRunRunnable> {

	String getRunIdentifier();

}