/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.program.ParameterSet;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.runresult.NoRunResultFormatParserException;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IClusteringIterationRunnable<PARENT_TYPE extends IExecutionRunRunnable>
		extends
			IIterationRunnable<IExecutionIterationWrapper, PARENT_TYPE> {

	/**
	 * @return the noRunResultException
	 */
	NoRunResultFormatParserException getNoRunResultException();

	int getIterationNumber();

	ParameterSet getParameterSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#getRun()
	 */
	IExecutionRun getRun();

	Process getProcess();
}