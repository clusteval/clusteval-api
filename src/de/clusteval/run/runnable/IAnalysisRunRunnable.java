/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.framework.repository.RegisterException;
import de.clusteval.run.IAnalysisRun;
import de.clusteval.run.runresult.IAnalysisRunResult;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <S>
 * @param <R>
 * @param <IW>
 * @param <IR>
 *
 * @since Nov 4, 2017
 */
public interface IAnalysisRunRunnable<RUN_TYPE extends IAnalysisRun<?, ?>, S extends IStatistic, R extends IAnalysisRunResult<?, ?>, IW extends IAnalysisIterationWrapper<S>, IR extends IAnalysisIterationRunnable<?, ?, ?>>
		extends
			IRunRunnable<RUN_TYPE, IR, IW, R> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.RunRunnable#beforeRun()
	 */
	void beforeRun() throws RunRunnableBeforeExecutionException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.RunRunnable#afterRun()
	 */
	void afterRun() throws InterruptedException, RegisterException;

}