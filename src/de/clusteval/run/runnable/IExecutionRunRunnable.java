/**
 * 
 */
package de.clusteval.run.runnable;

import java.util.List;
import java.util.Map;

import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.runresult.IExecutionRunResult;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.format.RunResultNotFoundException;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IExecutionRunRunnable<RUN_TYPE extends IExecutionRun<?>, RESULT extends IExecutionRunResult>
		extends
			IRunRunnable<RUN_TYPE, IClusteringIterationRunnable<?>, IExecutionIterationWrapper, RESULT> {

	/**
	 * @return The program configuration of this runnable.
	 */
	IProgramConfig getProgramConfig();

	/**
	 * @return The data configuration of this runnable.
	 */
	IDataConfig getDataConfig();

	String getCompleteQualityOutput();

	IClusteringQualitySet assessQualities(
			final IFileBackedClustering convertedResult,
			final Map<String, String> internalParams)
			throws RunResultNotFoundException;

	IFileBackedClustering convertResult(final IFileBackedClustering result,
			final Map<String, String> effectiveParams,
			final Map<String, String> internalParams)
			throws NoRunResultFormatParserException, RunResultNotFoundException,
			RunResultConversionException;

	void writeQualitiesToFile(
			List<Triple<ParameterSet, IClusteringQualitySet, Long>> qualities);

	void writeIterationMetaDataToFile(int optId,
			Map<String, String> iterationMetaData);

	void handleMissingRunResult(
			final IExecutionIterationWrapper iterationWrapper);
}