/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.runresult.IToolRuntimeEvaluationRunResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public interface IToolRuntimeEvaluationRunRunnable
		extends
			IClusteringRunRunnable<IToolRuntimeEvaluationRun<?>, IToolRuntimeEvaluationRunResult> {

}