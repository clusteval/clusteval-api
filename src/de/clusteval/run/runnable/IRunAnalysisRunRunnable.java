/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IRunAnalysisRun;
import de.clusteval.run.runresult.IRunAnalysisRunResult;
import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IRunAnalysisRunRunnable
		extends
			IAnalysisRunRunnable<IRunAnalysisRun, IRunStatistic, IRunAnalysisRunResult, IRunAnalysisIterationWrapper, IRunAnalysisIterationRunnable> {

	String getRunIdentifier();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	IRunAnalysisRunResult getResult();

}