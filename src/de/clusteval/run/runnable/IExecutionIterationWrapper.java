/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.util.Map;

import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ParameterSet;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IExecutionIterationWrapper extends IIterationWrapper {

	String[] getInvocation();

	void setInvocation(String[] invocation);

	ParameterSet getParameterSet();

	IProgramConfig getProgramConfig();

	int getOptId();

	Map<String, String> getEffectiveParams();

	File getClusteringResultFile();

	Map<String, String> getInternalParams();

	IFileBackedClustering getClusteringRunResult();

	void setConvertedClusteringRunResult(
			IFileBackedClustering clusteringRunResult);

	IFileBackedClustering getConvertedClusteringRunResult();

	void setParameterSet(ParameterSet parameterSet);

	void setProgramConfig(IProgramConfig programConfig);

	void setClusteringRunResult(IFileBackedClustering clusteringRunResult);

	void setOptId(int optId);

	void setResultQualityFile(File resultQualityFile);

	File getResultQualityFile();

	void setClusteringResultFile(File clusteringResultFile);
}