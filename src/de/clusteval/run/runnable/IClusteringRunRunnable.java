/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IExecutionRun;
import de.clusteval.run.runresult.IClusteringRunResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IClusteringRunRunnable<R extends IExecutionRun<?>, RESULT extends IClusteringRunResult>
		extends
			IExecutionRunRunnable<R, RESULT> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#handleMissingRunResult()
	 */
	void handleMissingRunResult(IExecutionIterationWrapper iterationWrapper);
}