/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <S>
 * @param <IW>
 * @param <PARENT_TYPE>
 *
 * @since Nov 4, 2017
 */
public interface IAnalysisIterationRunnable<S extends IStatistic, IW extends IAnalysisIterationWrapper<S>, PARENT_TYPE extends IAnalysisRunRunnable>
		extends
			IIterationRunnable<IW, PARENT_TYPE> {

	S getStatistic();

	void setStatistic(S statistic);

	void doRun() throws InterruptedException;
}