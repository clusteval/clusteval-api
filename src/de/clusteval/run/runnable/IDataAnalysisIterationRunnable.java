/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IDataAnalysisIterationRunnable
		extends
			IAnalysisIterationRunnable<IDataStatistic, IDataAnalysisIterationWrapper, IDataAnalysisRunRunnable> {

	IDataConfig getDataConfig();

	ProgressPrinter getProgressPrinter();

}