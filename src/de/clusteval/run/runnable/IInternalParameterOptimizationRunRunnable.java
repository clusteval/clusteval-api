/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IInternalParameterOptimizationRun;
import de.clusteval.run.runresult.IParameterOptimizationResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 5, 2017
 */
public interface IInternalParameterOptimizationRunRunnable
		extends
			IExecutionRunRunnable<IInternalParameterOptimizationRun, IParameterOptimizationResult> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#handleMissingRunResult()
	 */
	void handleMissingRunResult(IExecutionIterationWrapper iterationWrapper);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	IParameterOptimizationResult getResult();

}