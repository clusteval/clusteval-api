/**
 * 
 */
package de.clusteval.run.runnable;

import java.util.List;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 5, 2017
 */
public interface IParameterOptimizationRunRunnable
		extends
			IExecutionRunRunnable<IParameterOptimizationRun, IParameterOptimizationResult> {

	/**
	 * @return Get the optimization method of this parameter optimization run
	 *         runnable.
	 * @see #optimizationMethod
	 */
	IParameterOptimizationMethod getOptimizationMethod();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#handleMissingRunResult()
	 */
	void handleMissingRunResult(IExecutionIterationWrapper iterationWrapper);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.ExecutionRunRunnable#afterQualityAssessment(java.util.List)
	 */

	// 04.04.2013: adding iteration number into complete file
	void writeQualitiesToFile(
			List<Triple<ParameterSet, IClusteringQualitySet, Long>> qualities);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	@Override
	IParameterOptimizationResult getResult();

}