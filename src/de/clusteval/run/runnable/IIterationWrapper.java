/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;

import de.clusteval.data.IDataConfig;

/**
 * @author Christian Wiwie
 *
 */
public interface IIterationWrapper {

	boolean isResume();

	IRunRunnable getRunnable();

	IDataConfig getDataConfig();

	File getLogfile();

	void setRunnable(IRunRunnable runnable);

	void setDataConfig(IDataConfig dataConfig);

	void setResume(boolean isResume);

	void setLogfile(File logfile);

}