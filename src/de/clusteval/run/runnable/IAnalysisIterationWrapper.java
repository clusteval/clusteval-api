/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <S>
 *
 * @since Nov 4, 2017
 */
public interface IAnalysisIterationWrapper<S extends IStatistic>
		extends
			IIterationWrapper {

	String getAnalysesFolder();

	void setAnalysesFolder(String analysesFolder);

	S getStatistic();

	void setStatistic(S statistic);

}