/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.runresult.IClusteringRunResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 5, 2017
 */
public interface IRobustnessAnalysisRunRunnable
		extends
			IClusteringRunRunnable<IRobustnessAnalysisRun, IClusteringRunResult> {

}