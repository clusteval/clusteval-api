/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.IInMemoryListener;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * @author Christian Wiwie
 *
 * @param <IR>
 * @param <IW>
 */
public interface IRunRunnable<RUN_TYPE extends IRun<?>, IR extends IIterationRunnable<?, ?>, IW extends IIterationWrapper, RESULT extends IRunResult>
		extends
			Runnable,
			Serializable,
			IInMemoryListener

{

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	void run();

	void terminate();

	/**
	 * This method causes the caller to wait for this runnable's thread to
	 * finish its execution.
	 * 
	 * <p>
	 * This method also waits in case this runnable has not yet been started
	 * (the future object has not yet been initialized).
	 * 
	 * @throws InterruptedException
	 * @throws CancellationException
	 * @throws ExecutionException
	 */
	void waitFor() throws InterruptedException, ExecutionException;

	/**
	 * @return True, if this runnable's thread has been paused.
	 */
	boolean isPaused();

	/**
	 * @return True, if this runnable's thread has been cancelled.
	 */
	boolean isCancelled();

	/**
	 * @return True, if this runnable's thread has finished its execution.
	 */
	boolean isDone();

	/**
	 * This method pauses this runnable's thread until {@link #resume()} is
	 * invoked. If it was paused before this invocation will be ignored.
	 */
	void pause();

	/**
	 * This method resumes this runnable's thread, after it has been paused. If
	 * it wasn't paused before this invocation will be ignored.
	 */
	void resume();

	/**
	 * The future object of a runnable is only initialized, when it has been
	 * started.
	 * 
	 * @return The future object of this runnable.
	 * @see #future
	 */
	Future<?> getFuture();

	/**
	 * @return A list with all exceptions thrown during execution of this
	 *         runnable.
	 * @see #exceptions
	 */
	List<Throwable> getExceptions();

	void clearExceptions();

	/**
	 * @return A list of less severe exceptions thrown during exuection of this
	 *         runnable.
	 */
	List<Throwable> getWarningExceptions();

	void clearWarningExceptions();

	/**
	 * @return The progress printer of this runnable.
	 * @see #progress
	 */
	ProgressPrinter getProgressPrinter();

	/**
	 * @return The progress printer of this runnable.
	 * @see #progress
	 */
	ProgressPrinter getProgressPrinterBeforeExecution();

	/**
	 * @return the lastStartTime
	 */
	long getStartTime();

	float getPercentFinished();

	float getPercentFinishedDuringCurrentExecution();

	/**
	 * @return the iterationRunnableRunningTimeSum
	 */
	long getIterationRunnableRunningTimeSum();

	/**
	 * 
	 * @return
	 */
	long getEstimatedRemainingCPUTimeInMs();

	/**
	 * @return The run this runnable belongs to.
	 */
	RUN_TYPE getRun();

	void reportIterationRunnableRunningTime(long runningTime);

	RESULT getResult();

	boolean isInterrupted();

	boolean checkForInterrupted();
}