/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.runresult.IDataAnalysisRunResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public interface IDataAnalysisRunRunnable
		extends
			IAnalysisRunRunnable<IDataAnalysisRun, IDataStatistic, IDataAnalysisRunResult, IDataAnalysisIterationWrapper, IDataAnalysisIterationRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisRunRunnable#beforeRun()
	 */
	void beforeRun() throws RunRunnableBeforeExecutionException;

	IDataConfig getDataConfig();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	IDataAnalysisRunResult getResult();

}