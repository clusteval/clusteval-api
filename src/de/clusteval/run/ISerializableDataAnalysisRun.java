/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.data.ISerializableDataConfig;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableDataAnalysisRun<R extends IDataAnalysisRun> extends ISerializableAnalysisRun<R> {

	/**
	 * @return the dataConfigs
	 */
	List<ISerializableDataConfig> getDataConfigs();

}