/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runnable.IAnalysisRunRunnable;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <S>
 */
public interface IAnalysisRun<S extends IStatistic, RUNNABLE_TYPE extends IAnalysisRunRunnable<?, ?, ?, ?, ?>>
		extends
			IRun<RUNNABLE_TYPE> {

	/**
	 * @return A list with all statistics that belong to this run.
	 * @see #statistics
	 */
	List<S> getStatistics();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#notify(framework.repository.RepositoryEvent)
	 */
	void notify(RepositoryEvent e) throws RegisterException;

	ISerializableAnalysisRun<? extends IAnalysisRun<? extends IStatistic, ?>> asSerializable()
			throws RepositoryObjectSerializationException;
}