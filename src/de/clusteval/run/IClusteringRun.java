/**
 * 
 */
package de.clusteval.run;

import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runnable.IClusteringRunRunnable;

/**
 * @author Christian Wiwie
 *
 */
public interface IClusteringRun<R extends IClusteringRunRunnable<?, ?>>
		extends
			IExecutionRun<R> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#clone()
	 */
	IClusteringRun<R> clone();

	@Override
	ISerializableClusteringRun<?> asSerializable()
			throws RepositoryObjectSerializationException;
}