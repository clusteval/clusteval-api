/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.run.runnable.IRunDataAnalysisRunRunnable;
import de.clusteval.run.statistics.IRunDataStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunDataAnalysisRun
		extends
			IAnalysisRun<IRunDataStatistic, IRunDataAnalysisRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.RunAnalysisRun#clone()
	 */
	IRunDataAnalysisRun clone();

	/**
	 * @return the run identifiers
	 */
	List<String> getUniqueRunAnalysisRunIdentifiers();

	/**
	 * @return the dataConfigs
	 */
	List<String> getUniqueDataAnalysisRunIdentifiers();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	boolean terminate();

}