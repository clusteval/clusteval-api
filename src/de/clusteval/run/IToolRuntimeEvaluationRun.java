/**
 * 
 */
package de.clusteval.run;

import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runnable.IToolRuntimeEvaluationRunRunnable;

/**
 * @author Christian Wiwie
 *
 */
public interface IToolRuntimeEvaluationRun<RUNNABLE_TYPE extends IToolRuntimeEvaluationRunRunnable>
		extends
			IClusteringRun<RUNNABLE_TYPE> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#clone()
	 */
	IToolRuntimeEvaluationRun clone();

	ISerializableToolRuntimeEvaluationRun<? extends IToolRuntimeEvaluationRun<RUNNABLE_TYPE>> asSerializable()
			throws RepositoryObjectSerializationException;
}