/**
 * 
 */
package de.clusteval.run;

import java.util.List;

import de.clusteval.run.runnable.IRunAnalysisRunRunnable;
import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 */
public interface IRunAnalysisRun
		extends
			IAnalysisRun<IRunStatistic, IRunAnalysisRunRunnable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	IRunAnalysisRun clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	boolean terminate();

	/**
	 * @return A list with all unique run identifiers of this run.
	 */
	List<String> getUniqueRunAnalysisRunIdentifiers();

}