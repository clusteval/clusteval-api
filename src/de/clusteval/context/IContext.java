/**
 * 
 */
package de.clusteval.context;

import java.util.Set;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * @author Christian Wiwie
 *
 */
public interface IContext extends IRepositoryObjectDynamicComponent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#equals(java.lang.
	 * Object )
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#hashCode()
	 */
	int hashCode();

	IRepositoryObject clone();

	/**
	 * Contexts have a unique name.
	 * 
	 * @return The name of this context
	 */
	String getName();

	/**
	 * @return A set with all simple names of classes this context requires.
	 */
	Set<String> getRequiredJavaClassFullNames();

	/**
	 * 
	 * @return The standard input format connected to this context. Every
	 *         context has its own standard format, which is used during
	 *         execution of runs.
	 */
	IDataSetFormat getStandardInputFormat();

	/**
	 * 
	 * @return The standard output format connected to this context. Every
	 *         context has its own standard format, which is used during
	 *         execution of runs.
	 */
	IRunResultFormat getStandardOutputFormat();

	String toString();

	@Override
	ISerializableWrapperDynamicComponent<IContext> asSerializable()
			throws RepositoryObjectSerializationException;
}