/**
 * 
 */
package de.clusteval.context;

import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableContext extends ISerializableWrapperDynamicComponent<IContext> {

}